//
//
//  ecTool      EtherCAT Master mit graphischer Benutzeroberfläche als allgemeines Inbetriebnahmetool für BESTEC Projekte
//              verwendet SOEM und QT4
//
//              In diesem Modul ist der GUI-Kram drin und das TCP/IP-Interface
//
//  Autor:      Christian Grundel
//
//
//  06.11.2019  Neu aufgesetzt, vom aktuellen ecControl geforkt
//  06.11.19    Beliebiger Offset für Rotationsachsen eingefügt, so geht auch -1 bis 361 Grad. Dies ist Version 1.0
//  17.12.19    Bug beseitigt: refreshMoveFinished ändert nicht mehr die Zielposition einer anderen Achse,
//              positionUnits läuft bei Achsen ohne Encoder A oder B synthetisch mit, dadurch ist simulierte Bewegung mit Borys Server möglich (Axis GetStatus)
//  18.12.19    Beim Erreichen der Limits wird PositionTarget auf aktuellen Wert gesetzt, closedloop Marker gelöscht.
//  24.12.19    "set_cpu_dma_latency" und "setcap CAP_DAC_OVERRIDE" ergänzt, dies setzt den CPU Takt auf Maximum
//              und ist der Schlüssel für sehr gute Latenzen. Dies ist Version ecTool 1.01.
//  25.12.19    Jetzt neu auch Protokoll der nanosleep latency, GUI entsprechend aufgeräumt, ist 1.02
//  28.12.19    Aus mlockall den Parameter MCL_FUTURE rausgenommen, ist 1.021
//  26.02.20    ECM-702 mit Tonic geht, Referenz-Marken-Reset kann ein- und ausgeschaltet werden
//  02.03.20    Fahren über die Referenz Marke wird erkannt und signalisiert
//  03.03.20    Alarm vom Tonic geht, EncoderStatus gründlich überarbeitet
//  11.03.20    OTF Funktion für die Tonics ist vorbereitet, eine Achse fungiert als Führungsachse, die anderen werden alle mit protokolliert
//  20.03.20    Erste Funktionalität für OTF mit ECM-702 und Tonic
//  06.04.20    ECM-713 neu in die Liste der EtherCAT-Module aufgenommen
//  17.04.20    Erste Funktionalität mit ECM-713, wird korrekt enumeriert
//  30.06.20    PDO für ECM-713 aktualisiert, Limits werden korrekt ausgelesen
//  03.07.20    Config-Axis und Config-Encoder für ECM-713 erweitert, Fahren in Steps und die Einstellung der microSteps funktionieren
//  04.07.20    Zerlegung des Quelltextes in Module
//  13.07.20    Korrektur der Indizierung der ECM-713 Encoder
//  16.07.20    Lauf- und Stopstrom werden einwandfrei gesteuert
//  19.07.20    Kleinen Fehler beim Indizieren der ECM-702 beseitigt, Position im GUI auf 6 Nachkommastellen    - Dies ist Revision 1.105
//  20.07.20    Ein paar Hints hinzugefügt
//  28.07.20    Logging der Encoder und MoveSequence neu eingefügt, das Logging funktioniert, das automatisierte Bewegen noch nicht  - Dies ist Revision 1.106
//  29.07.20    Die Filenames der log-Dateien Encoder_Counts_xx:yy:zz.dat und Moves_xx:yy:zz.log auf hh:mm:ss auf fest zwei Stellen mit leading zero korrigiert, .dat mit Dezimal-Punkt und Horizontal Tab als Trenner im Korpus der Datei
//  30.07.20    Move Sequence (automatisierte Hin- und Herbewegung einer Achse) funktioniert (noch ohne Subdivisions)   - Dies ist Revision 1.108
//  30.07.20    Auswahl der zu loggenden Encoder unabhängig vom Namen
//  31.07.20    Wait Time in Minuten bis zum ersten automatischen Move Sequence ergänzt, funktioniert.
//  01.08.20    Mit dem Dialog für die Parallel-Kinematik Hexapod angefangen
//  02.08.20    Die Berechnung der Achswerte aus der Pose geht grundsätzlich, habe einfache analytische Lösung gefunden
//  03.08.20    Die default-Logik umgestellt: In Default.cfg ist jetzt nur noch ein Datei-Namens-Index, der auf die aktive Konfiguration verweist.
//              Default.cfg wird bei "Save/Load Config" automatisch aktualisiert            - Dies ist Revision 1.110
//  06.08.20    Vorzugsdrehrichtung über das Vorzeichen von steps_per_unit neu eingebaut, simuliert die Einstellung an der Endstufe
//  07.08.20    positionStepsOffset aufgeräumt, die Steps im GUI und der internen Logik beleiben bei Restart ECat unverändert, die Offsets werden für einen reibungslosen Anschluss aktualisiert
//  08.08.20    Alle drei Drehungen in hexapod, über signal connect() fahren die Achsen     - Dies ist Revision 1.111
//  09.08.20    Parameter Delta_ZPP ergänzt, ist die Distanz zwischen dem Pivot und dem Ursprung der Platform. Reihenfolge der Drehungen jetzt Roll-Pitch-Yaw
//  10.08.20    Ansicht von oben auf das Layout des Hexapod funktioniert!   - Dies ist Revision 1.112
//  11.08.20    MoveSequence geht jetzt auch für DOFs als virtuelle Achse
//  12.08.20    Die Möglichkeit simulierter Achsen eingefügt    - Dies ist Revision 1.114
//  13.08.20    Die Einheit rad auf mrad geändert
//  14.08.20    chooselist hat jetzt überschriebenes exec(), mylastrowglobal ist entfernt
//  17.08.20    Gleitender Mittelwert als zweite Spur für das Logging - Dies ist Revision 1.115
//  14.09.20    Motorstrom in 0,1 A Schritten einstellbar, MoveSequence geht jetzt mit Subdivisions, "quicklog" - Dies ist Revision 1.116
//  16.09.20    Die Aux Limits werden in Gelb und invers angezeigt, ZeroStatic löscht ZEROLOST
//  21.09.20    Vorzugsdrehrichtung für die Motortreiber ergänzt, den Vorzeichenkram von steps-per-unit aufgeräumt - Dies ist Revision 1.118
//  22.09.20    Initialisierung der Achsen gründlich überarbeitet
//  23.09.20    OffsetSteps überarbeitet, shadow entfernt, Referenzfahrt mit anschließendem Nullmarkensuchen geht, auch nach Restart
//              CL Wait Step auf ms umgebaut    - Dies ist Revision 1.119
//  25.09.20    Indexdatei zu "ecToolDefault.cfg" umbenannt, um Konflikt mit ecControl zu vermeiden
//  26.09.20    Init Prozedur um Step 3 erweitert, das Referencing der Tonic wird jetzt korrekt automatisch aktiviert und deaktiviert - Referenzfahrten sind jetzt voll funktional!   - Dies ist Revision 1.120
//  29.09.20    Collsion Control (Software Ersatz für die Collision Box) eingefügt - Dies ist Revision 1.121
//  30.09.20    GUI aufgeräumt, keine Timing Info mehr - "Axis GetStatus"  liefert jetzt als weitere zwei Parameter die AUX1 und AUX2 Bits.- Dies ist Revision 1.122
//  05.10.20    Subtilen Fehler beseitigt: Unsigned weg, aCounts, bCounts auf quint64 verändert.
//  06.10.20    Per Doppelklick auf die Zeile mit dem Encoder Status können die Tonics von "Zero Lost" auf "Ok" gesetzt werden.
//              Im GUI ui->lineEdit_Position auf acht Nachkommastellen erweitert, das Logging ist fernsteuerbar, offsetCounts kann negativ sein  - Dies ist Revision 1.123
//  07.10.20    Quickfix für Borys bei Abfrage des Achsentyps: 1,2 wird auf 2,3 verschoben
//              Konstante FUENFMILLE fürs Init eingeführt, Die AUX-Bits für Abfrage von Borys über TCP/IP invertiert - Dies ist Revision 1.125
//  08.10.20    ecTool zu zwei Quelldateien konsolidiert, eine fürs GUI, die andere für Motion/Hardware. Ist übersichtlicher und kompiliert deutlich schneller.
//              Alles auf Dezimalpunkte geändert, Achtung auch das .cfg-File !!
//  12.10.20    Achsen und Encoder haben den neuen Parameter isUsed, "NU" gibts nicht mehr, der Name kann beliebig sein, auch bei nicht genutzten Limit Schaltern - Dies ist Revision 1.127
//  13.10.20    Passwortschutz hinzugefügt. Funktioniert über einen MD5 Hash in "/Config_Motion/users.tool".  - Dies ist Revision 1.128
//  19.10.20    OTF
//  26.10.20    get_ECM702_CaptureCounts hinzugefügt
//  03.11.20    Konzept "Führungsachse" = "OTF Axis"
//
//  12.01.21    GUI Oberfläche Aufräumen - Dies ist Revision 1.130 für P494

//  27.01.21    Großes Aufräumen, Hauptfenster jetzt mit "Extend"
//  27.01.21    MoveSequence geht wieder
//  28.01.21    Verzahnung von MoveSequence mit Hexapod (DOFs aufgeräumt)
//  01.02.21    OTF Fenster aufgeräumt
//  02.02.21    Elektrische Tests, OTF geht! - Dies ist Revision 1.150
//  09.02.21    OTF von Counts auf Units der Achsen umgestellt, feedbacktype abgeschafft
//  10.02.21    Fehler mit indexEncoder gefunden und beseitigt, OTF mit Units geht!
//  11.02.21    Test mit fahrender Leitachse und Dauertrigger geht! Die TCP/IP-Befehle für OtF angefangen.
//  12.02.21    OtF-Befehle fertig gestellt - Dies ist Revision 1.152
//  24.03.21    Biss-C CRC Check für die Renishaw Resolute funktioniert jetzt, ausfühliche Status-Anzeige im Encoder Dialog. - Dies ist Revision 1.153
//  31.05.21    Dezimal-Trenner für "Save last Move" ist konfigurierbar, ob . oder , gewünscht
//  23.06.21    Die ECM-713 (Firmware 0.9) werden ab jetzt aus ecTool mit TBLANK, TDECAY und TOFF versorgt. Achtung, auf korrekte [MOTOR_DRIVER_CONFIGURATION] achten! - Dies ist Revision 1.155
//
//  23.08.21    Dialog MoveSequence optisch aufgeräumt, jetzt mit Angabe der Einheit der Zeit (min oder sec), keine funktionale Änderung
//  15.12.21    Das Modul A760-01-B (Fast Shutter und 4Q-Dioden für P483 Laser-Plasma-Quelle) wird jetzt korrekt erkannt
//  15.03.22    Shutter Steurung geht
//  28.03.22    Mit setWindowFlags(Qt::Dialog) werden die untergeordneten Dialoge nicht mehr dauernd vom Hauptfenster verdeckt
//  06.04.22    Die Counts von der 4-Q Diode werden korrekt angezeigt, Target Korrektur über einzel anwählbare Quadranten gesteuert. - Dies ist Revision 1.3
//  30.05.22    Umstellung auf Start/Stop fertig gestellt. - Dies ist Revision 1.4
//  08.06.22    Anzahl gleitender Mittelwert konfigurierbar, Offsets für die Quadranten, Fehler TT behoben, Befehl für Abfrage Quadranten. - Dies ist Revision 1.5
//  25.07.22    Neuer Fehler-Code ENCSTATE_NO_CONNECT für die Resolute eingefügt, es wird zwischen Bit-Daten Fehler und komplettem Kabelbruch differenziert - 1.51
//  03.08.22    readClientData() aufgeräumt, die neuen Befehle "Process Start", "Process Stop" und "GetQuads" funktionieren - Dies ist Revision 1.6

//
//              Im Modul maindialog.cpp sind alle GUI-Funktionen enthalten
//

#define MAIN_WINDOW_TITLE   "ecTool 1.6"   // Programmname mit Revisionsnummer

#define SERVERPORT 1234         // der Port über den der Motion-Server gesteuert wird

#define USERLEVELVIEWER     0
#define USERLEVELEXPERT     1
#define USERTEXTVIEWER      "  Level Viewer"
#define USERTEXTEXPERT      "  Level Expert"

#define USERHASHPATH        "/Config_Motion/users.tool"


// die Bezeichnung der Einheiten, ist nur zur Information des Nutzers, hat sonst keine Funktion im Programm
#define UNIT_1  "mm"
#define UNIT_2  "mrad"
#define UNIT_3  "grad"
#define UNITk_1 "(mm)"
#define UNITk_2 "(mrad)"
#define UNITk_3 "(grad)"

//  für die verschiedenen Auswahllisten
#define CLIST_UNIT               1
#define CLIST_ROTATIONTYPE       2
#define CLIST_AXISTYPE           3
#define CLIST_YESNO              4
#define CLIST_ENCODER            5
#define CLIST_FEEDBACK           6
#define CLIST_GANTRY             7
#define CLIST_INITDIRECTION      8
#define CLIST_MICROSTEPS         9
#define CLIST_AXISCURRENT       10
#define CLIST_REFERENCING       11
#define CLIST_MOTORDIRECTION    12


#include "common.h"                 // Alles was ecTool und maindialog gemeinsam nutzen, insbesonders die globalen Variablen

#include <sys/file.h>               // enthält flock()
#include <unistd.h>                 // enthält getpid()
#include <math.h>

#include "alldialogs.h"             // die ganzen GUI Objekte für Qt

#include "ui_ecMainDialog.h"        // erzeugt der moc aus den Formulardateien
#include "ui_ecEncoderDialog.h"
#include "ui_ecBeckhoffDialog.h"
#include "ui_ecConfigurationDialog.h"
#include "ui_ecMoveSequenceDialog.h"
#include "ui_ecHexapodDialog.h"
#include "ui_ecAxisDialog.h"
#include "ui_ecChooseList.h"
#include "ui_ecOtfTonicDialog.h"
#include "ui_ecSourceControlDialog.h"


//
//      Zugriff auf die in ecTool.cpp definierten globalen gemeinsamen Variablen
//

extern slavesInfo_t         foundSlaves;            // extern = Verweis auf Speicher in ecTool
extern myRelais_t           myRelais;
extern myInputs_t           myInputs;
extern myAxes_t             myAxes;
extern myEncoderChannels_t  myEncoderChannels;
extern ecatState_t          ecatState;
extern flags_t              flags;
extern flagsDebug_t         flagsDebug;
extern collisionControl_t   collisionControl;
extern motorDriver_t        motorDriverConfig;

extern sourceControl_t      sourceControl;



//
//          lokale Typdefinitionen für maindialog
//

// der Teil der Achsenbeschreibung nur für den Dialog
typedef struct
{
    QString     comment = "";                           // Kommentar über die gesamte Konfiguration
    QString     defaultFileName = "Default_Project";    // Der Name der Datei, die zum Speichern der Konfiguration verwendet wird
    QString     axisName[MAXAXISNUMBER];                // alle meine Achsen
} myAxesDialog_t;


// der Teil der Encoderbeschreibung nur für den Dialog
typedef struct
{
    //QString     comment = "";                             // Kommentar über die gesamte Konfiguration
    //QString     defaultFileName = "Default_Project";    // Der Name der Datei, die zum Speichern der Konfiguration verwendet wird
    QString     encName[MAXENCODERNUMBER];                 // alle meine Encoder
    //QString     encoderTypeName[MAXENCODERNUMBER];
} myEncoderDialog_t;

typedef struct
{
    // Zustand der Dialoge, ob offen oder geschlossen
    bool    isOpen_Axis             = false;            // beim Start sind alle Dialoge bis auf das Hauptfenster geschlossen
    bool    isOpen_Encoder          = false;
    bool    isOpen_Configuration    = false;
    //bool    isOpenSensors = false;
    bool    isOpen_IO_Beckhoff      = false;
    bool    isOpen_MoveSequence     = false;
    bool    isOpen_Hexapod          = false;
    bool    isOpen_OTF_Tonic        = false;
    bool    isOpen_SourceControl    = false;

    //bool    isOpenSensors = false;

    // Die Dialoge, welche beim Klick auf den Button "Extend" geöffnet werden
    bool    extOpen_IO_Beckhoff     = false;    // wird aus der config gelesen
    bool    extOpen_MoveSequence    = false;
    bool    extOpen_Hexapod         = false;
    bool    extOpen_OTF_Tonic       = false;
    bool    extOpen_SourceControl   = false;


} myDialogs_t;

typedef struct
{
    QString enName = "eth0";    // Intranet
    QString enPort = "1234";    // Port Number für den Server
    QString ecName = "eth1";    // EtherCAT
    QString enIP = "";          // wenn nicht leer, zB "192.168.18.22", dann werden nur Befehle von dieser Client Adresse angenommen
    bool    isLocalLoopBack = true;
    bool    enConnected = false;    // true wenn mit einem Client verbunden
    // QString enName = "enp1s0";
    // QString ecName = "enp0s31f6";
} myNetwork_t;

typedef struct
{
    int         index;                      // laufende Nr
    bool        first;
    int         Offset[MAXENCODERNUMBER];   // hohe Offsets von den Counts automatisch abziehen
} quick_t;


typedef struct
{
    double          startPoint = 0.0;
    double          endPoint = 1.0;
    int             subDivisions = 1;           // Zahl der Unterteilungen zwischen den Endpunkten
    int             iterations = 1;             // Anzahl der Wiederholungen der Bewegungssequenz
    int             iterationCounter = 0;       // downcounter
    int             subdivisionCounter = 0;     // downcounter
    int             timePerStep = 1;            // Zeit des Verharrens auf einem Punkt bis zum nächsten Move in Sekunden
    int             nextTime;
    int             axis = 0;                   // der Index der Achse, welche fährt
    bool            reverse = false;            // fährt zurück nach Start
    int             waitTime = 0;               // Wartezeit bis Beginn der Bewegungs-Sequenz
    int             waitTimeCounter = 0;        // Count Down der Wartezeit in Sekunden!
    bool            firstMove = false;
} moveSequence_t;


typedef struct
{
    // Parameter aus der Konfiguration:

    double      LegBaseLocal_x[6];      // lokale Koordinaten Basis
    double      LegBaseLocal_y[6];      // ist konstruktiv fest, wird eingemessen, z implizit Null
    // unten wird immer z=0 angenommen, das steckt implizit in den Offsets für die Encoder-Counts

    double      LegUpperLocal_x[6];     // lokale Koordinaten Platform
    double      LegUpperLocal_y[6];     // ist konstruktiv fest, wird eingemessen, z implizit Null
    double      LegUpperLocal_z[6];     // hier sind aufgrund der Fertigungstoleranz Abweichungen von Null in z möglich

    double      LegLenght[6];           // die Beinlängen einschließlich zwei Kugelradien, wird eingemessen

    double      Offset_Z0 = 100;        // Höhendifferenz zwischen globalem Koordinatensystem und der Nullpose dof_Z = 0, dies ist zugleich der Pivot
    double      Delta_ZPP = 10;         // Differenz zwischen Platform und Pivot = Nullpose dof_Z = 0

    int         Axis_Number[6];         // die zu Hx zugehörige Achse

    // Interne Variablen:

    // die oberen Punkte der Füße im globalen Koordinatensystem
    double      LegUpper_x[6];
    double      LegUpper_y[6];
    double      LegUpper_z[6];

    // die Pose der Plattform, Definition der sechs DOFs
    double      dof_X, dof_Y, dof_Z;    // der Mittelpunkt der Platform in globalen Koordinaten = (0,0,Z0) in der Nullpose
    double      dof_Yaw;                // Drehung um senkrechte Achse
    double      dof_Pitch;              // Drehung um waagerechte X
    double      dof_Roll;               // Drehung um waagerechte Y, liegt in Strahlrichtung

    // die Position der sechs Achsen, Ausgabewerte für die Motoren:
    double      AxisH[6];
} hexa_t;


//
//      lokale globale Variablen für maindialog
//


myAxesDialog_t      myAxesDialog;
myEncoderDialog_t   myEncoderDialog;
myDialogs_t         myDialogs;
myNetwork_t         myNetwork;

slavesInfo_t        configuredSlaves;       // lokal nur in maindialog
moveSequence_t      moveSequence;
quick_t             quick;
hexa_t              hexa;

static char     cstring[100];           // Ausrotten ?

QFile           encoderlogfile;         // Logging der Counts für Inbetriebnahme Stabilitätstest, wird nur hier in maindialog lokal verwendet, da myTimer Takt,fuer Igor
QFile           quicklogfile;           // Logging vereibfacht fuer Quickplot
QFile           protofile;              // für laufendes Protokoll des EtherCAT Status
QFile           mlogfile;               // für Protokoll der ausgefühten moves
QFile           lockfile;               // Marker für single Instance
QFile           otfdatafile;            // die Capture Counts

qint64          logCounter = 0;

QBrush          gantryBrush(QColor(87, 244, 48));    // grelles Grün für Achse mit Gantry-Funktion
//QBrush brushOk(Qt::green);
QBrush          brushOk(QColor(12, 160, 32));        // gedämpftes Grün
QBrush          brushErr(Qt::red);
QPalette        palOk;
QPalette        palErr;

//QLocale     myLocale = QLocale::German;

QLocale         myLocale = QLocale::English;        // alles in dieser App mit Dezimalpunkt


//
//      Die Deklaration der lokalen Funktionen von maindialog.cpp, welche nicht GUI Objekte und daher in maindialog.h deklariert sind
//

void writeConfiguration(QFile* myFile);
void readConfiguration(QFile* myFile);
void writeDefaultConfig(QFile* myFile);
void readDefaultConfig(QFile* myFile);
void writeRotationState();
void readRotationState();
void writeAxesState();
void writeOtfData();
bool checkConfiguration();

bool isSingleInstanceAndLock();
void unlockSingleInstanceMarker();

void DOF_to_Axes();


//
//      Initialisierung der Applikation
//

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);     // dieses Objekt muss existieren, damit QMessageBox geht.

    if(isSingleInstanceAndLock())
    {
        ecMainDialog w;
        w.show();
        int status = a.exec();
        unlockSingleInstanceMarker();
        return status;
    }
    else
    {
        fprintf(stderr, "An Instance of ecControl is already running. Will stop.\n");
        QMessageBox::critical(NULL, "Abort", "Another ecControl is already running!",  QMessageBox::Ok);
        return 0;
    }
}

// prüft ob schon eine Instanz läuft, wenn nicht wird der Marker für diese Instanz gesetzt
bool isSingleInstanceAndLock()
{
    QString path = QDir::homePath() + "/Data_Motion/ecControl.lock";
    lockfile.setFileName(path);

    if (lockfile.open(QIODevice::ReadWrite | QIODevice::Text))      // try to open the lock-file
    {
        if( flock(lockfile.handle(), LOCK_EX | LOCK_NB) == 0 )      // try to lock it
        {
            // alle dürfen schreiben und lesen
            lockfile.setPermissions(QFileDevice::ReadUser  | QFileDevice::WriteUser  |
                                    QFileDevice::ReadGroup | QFileDevice::WriteGroup |
                                    QFileDevice::ReadOther | QFileDevice::WriteOther );
            // fine, we have the exclusive lock to the file, write our PID into it
            pid_t m_pidLocker = getpid();
            QTextStream out(&lockfile);
            out << QString("%1").arg( (int)m_pidLocker );
            if( !lockfile.flush() )    // Puffer in Datei schreiben
            {
                fprintf(stderr, "Error:  Writing of PID to lock-file failed.\n");
            }
            return true;        // Marker erfolgreich gelockt, wir sind die Single Instance
        }
        else        // already locked, another instance is running
        {
            fprintf(stderr, "Lock-File is already locked.\n");
            return false;   // Es läuft bereits eine andere Instanz
        }
    }
    else
    {
        fprintf(stderr, "Error: Opening of Lock-File failed.\n");
        return true;   // Trotzdem starten, um eine Art DOS zu vermeiden
    }
}

// gibt Marker für diese Instanz wieder frei
void unlockSingleInstanceMarker()
{
    int status = flock(lockfile.handle(), LOCK_UN);
    lockfile.close();
    lockfile.remove();
}


//
//          Konstruktor des Hauptfensters
//

ecMainDialog::ecMainDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecMainDialog)
{
    myLocale.setNumberOptions(QLocale::OmitGroupSeparator);     // alle Zahlen mit englischem Dezimalpunkt, aber ohne group separator
    QLocale::setDefault(myLocale);                              // dies muss im Konstruktor des Hauptfensters stehen!!

    palOk.setBrush(QPalette::Text, brushOk);                    // globale Variablen für die Textfarben initialisieren
    palErr.setBrush(QPalette::Text, brushErr);

    // 16-Bit CRC Checksum of this Program
    check = "";
    QFile thisProgramFile(QCoreApplication::applicationFilePath());
    if (thisProgramFile.open(QIODevice::ReadOnly))
    {
        //QByteArray thisProgram;
        QByteArray thisProgram = thisProgramFile.readAll();
        quint16 crc1 = qChecksum(thisProgram.data(), thisProgram.length());
        thisProgram.clear();        // Speicher wieder freigeben
        check = QString("    (%1)").arg(crc1, 0, 16);     // hexadezimal
        check = check.toUpper();
        qDebug() << "CRC" << check;
    }
    else
    {
        qDebug() << "Can't open Program File for calculating CRC.";
    }

    ui->setupUi(this);
    //this->setWindowTitle(MAIN_WINDOW_TITLE + check + USERTEXTVIEWER);     // dies macht schon setUserLevel
    userLevel = USERLEVELVIEWER;
    setUserLevel(userLevel);

    fprintf(stderr, "\n...Starting ecTool... \n");
    myAxes.initFlag = false;

    // Das EtherCAT Zustandsprotokoll initialisieren:
    QString path = QDir::homePath() + "/Data_Motion/eCatStatus.log";
    //QFile protofile(line);
    protofile.setFileName(path);
    //protofile = QFile.(path);
    if (protofile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&protofile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        out << "ecTool started on " << QDateTime::currentDateTime().toString() << "\n\n";
        out << "Time            ms Sleep mSleep Jitter mJitter Loop mLoop wkState errCounter\n";
        protofile.close();
    }

    // Das Logfile für die Moves initialisieren:
    QTime myTime = QTime::currentTime();
    // 2 Stellen zur Basis 10, nach links mit '0' aufgefüllt:
    path = QDir::homePath() + QString("/Data_Motion/Moves_%1:%2:%3.log").arg(myTime.hour(), 2, 10, QChar('0')).arg(myTime.minute(), 2, 10,  QChar('0')).arg(myTime.second(), 2, 10, QChar('0'));
    mlogfile.setFileName(path);
    if (mlogfile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&mlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        out << "ecTool started on " << QDateTime::currentDateTime().toString() << "\n\n";
        out << "Time               Start Delta Stop\n";
        mlogfile.close();
    }


    //QShortcut *shortcut = new QShortcut(this);
    //shortcut->setKey('s');   // Taste für Bewegungs-Stop
    //shortcut->connect()
    //shortcut->setEnabled(true);
    //connect(shortcut, SIGNAL(activated()), this, SLOT(on_key_s_clicked()));

    QString fileName = QDir::homePath() + "/Config_Motion/ecToolDefault.cfg";     // die default Einstellungen laden
    QFile myFile(fileName);
    if (myFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        fprintf(stderr, "Found Default Configuration Index \"ecToolDefault.cfg\" - reading Filename: ");
        readDefaultConfig(&myFile);     // die Index-Datei auf die aktive Konfiguration lesen, setzt implizit myAxes.defaultFileName
        myFile.close();

        //ui->label_defaultFileName->setText(myAxesDialog.defaultFileName);
        ui->textBrowser_DefaultFileName->setText(myAxesDialog.defaultFileName);
        qDebug() << myAxesDialog.defaultFileName << "\n";

        fileName = QDir::homePath() + "/Config_Motion/" + myAxesDialog.defaultFileName;     // die aktive Konfiguration
        myFile.setFileName(fileName);
        if (myFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            qDebug() << "Found Configuration File " << fileName << " - reading Params in:\n";
            readConfiguration(&myFile);
            myFile.close();
        }
        else
            fprintf(stderr, "No Configuration File found, using hardwired parameters as default.\n");

    }
    else
        fprintf(stderr, "No Configuration Index File found, using hardwired parameters as default.\n");



    qDebug() << "EtherCAT If=" << myNetwork.ecName << " Ethernet If=" << myNetwork.enName << " Server Port=" << myNetwork.enPort << " Client IP=" << myNetwork.enIP << "\n";

    readRotationState();   // den Status der 361 Grad Ringe laden

    // Daten fest im RAM, hilft das wirklich?
    lock_AllMemory();

    // CG 24.12.19
    // Latency trick - if the file /dev/cpu_dma_latency exists, open it and write a zero into it.
    // Dies deaktiviert das Power Management und ist der Schlüssel zu niedrigen Latenzen!
    set_cpu_dma_latency();

    //usleep(500000);     // 0,2 s warten bis mallocall fertig ist, wird nicht gebraucht!

    // create the RT thread, der läuft gleich los aber wartet auf allSlavesOperational==true
    create_RT();

    // and give it higher priority
    elevate_RT();


    cpu_set_t   my_set;         /* Define your cpu_set bit mask. */
    CPU_ZERO(&my_set);          /* Initialize it all to 0, i.e. no CPUs selected. */
    CPU_SET(0, &my_set);        /* set the bit that represents core 1. */
    //iret1 = sched_setaffinity(0, sizeof(cpu_set_t), &my_set);     /* Set affinity of this process to */
    //qDebug() << "setaffinity " << iret1 << "\n";

    //movePlan.status = 0;
    //axisParams.tRamp = 1;   // 1 Sekunde
    //axisParams.vCruise = 8000;   // 8000 Steps = 2 Umdrehungen pro Sekunde

    //char ifname[100];
    //ifname = myNetwork.ecName.toLatin1().data();

    Startup_Ecat(myNetwork.ecName.toLatin1().data());       // Initialisierung, setzt alle Slaves auf operational wenn möglich

    if(configuredSlaves.simulateECM)
    {
        myAxes.numAxis = 8;
        myEncoderChannels.numEncoder = 8;
    }

    // bei Programmstart einmalig nichtmodale Dialog-Objekte erzeugen, verwendet in Startup_Ecat erzeugte globale Werte (myAxis.numAxis):
    // diese speichern erfreulicherweise alle Einstellungen, selbst wenn sie temporär geschlossen werden
    axisDialog = new ecAxisDialog;
    encoderDialog = new ecEncoderDialog;
    configurationDialog = new ecConfigurationDialog;
    beckhoffDialog = new ecBeckhoffDialog;
    hexapodDialog = new ecHexapodDialog;
    movesequenceDialog = new ecMoveSequenceDialog;
    otfTonicDialog = new ecOtfTonicDialog;
    sourceControlDialog = new ecSourceControlDialog;

    connect(axisDialog, &ecAxisDialog::axisParameterChanged, this, &ecMainDialog::InitAxisListMainDialog);     // Doppelklick in den Achsparametern an das Hauptfenster signalisieren
    connect(hexapodDialog, &ecHexapodDialog::hexaMoveToUnits, this, &ecMainDialog::MoveToUnits);                    // damit aus dem Hexapod Dialog heraus Achsen fahren können

    InitConfigMainDialog();                 // alle Elemente in der Gruppe Configuration des Hauptdialogs initialisieren
    InitAxisListMainDialog();               // die Achsentabelle im Hauptdialog initialisieren

    // Diese Elemente werden nur einmal beim Programmstart initialisiert:

    ui->lineEdit_DeltaSteps->setText(QString::number(myAxes.Axis[0].jogDeltaSteps));
    ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionStepsTarget));
    ui->lineEdit_DeltaPosition->setText(myLocale.toString(myAxes.Axis[0].jogDeltaPosition, 'f', 1));

    //sprintf(cstring, "%d", myAxis.Axis[myAxis.currentAxis].positionCountsTarget);
    //ui->lineEdit_Counts_Aim->setText(cstring);


    // Die aktuelle Position der Achsen als Zielwerte übernehmen, wenn eCat Hardware vorhanden:
    if(ecatState.allSlavesOperational)
    {
        while(!myAxes.initFlag);        // implizit auf Ethercat warten, ECM712 Slaves mindestens einmal ausgelesen
        usleep(600000);                 // 0.6 Sekunden warten für die Mittelung der Positionen
        for(int axis = 0; axis < myAxes.numAxis; axis++)
        {
            myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnitsAverage;

        }
    }
    ui->lineEdit_Position_Aim->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionUnitsTarget, 0, 'f', 8));

    // die Serverfunktion starten, auf Port xxx auf Client lauschen:
    // muss für Autostart verzögert zum Schluss erfolgen
    //usleep(1000000);        // 1 Sekunde warten, damit Ethernet sicher up ist bei Autostart
    if(myNetwork.enName != "")
    {
        Initialize_TCPIP_Interface();
    }
    else {
        ui->label_IP->setText("Server deaktivated.");
        fprintf(stderr, "Ethernet Eintrag leer: Intranet deaktiviert!\n");
    }

    // QTimer ganz zum Schluss starten
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(myqTimerTask()));
    timer->start(100);  // Zylus 100ms für irgendwas langsames im GUI


}


// Bewegung einer Achse im open loop Modus starten, mit absoluten Steps als Zielvorgabe
void ecMainDialog::MoveToSteps(int axis, qint64 absSteps, double relVel)
{
    flags.lastMoveType = flags.STEPS;
    myAxes.Axis[axis].positionStepsTarget = absSteps;
    ui->lineEdit_Steps_Aim->setText(QString::number(absSteps));
    MovePlan(axis, absSteps, relVel);
}

// Bewegung einer Achse im open loop Modus starten, mit relativen Steps
void ecMainDialog::MoveDeltaSteps(int axis, qint64 deltaSteps, double relVel)
{
    flags.lastMoveType = flags.STEPS;
    myAxes.Axis[axis].positionStepsTarget += deltaSteps;
    ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[axis].positionStepsTarget));
    MovePlan(axis, myAxes.Axis[axis].positionStepsTarget, relVel);
}

// Bewegung einer Achse mit Units als Zielvorgabe starten, geht mit Typ 1 = normal (mit Encoder, oder einfach nach Steps gefakte Units), mit Typ 2 machts keinen Sinn, wird hier aber nicht abgefangen
void ecMainDialog::MoveToUnits(int axis, double units, double relVel)
{
    if(!myAxes.errorEncoderGlobal)  // diesen Befehl bei Encoder Fehler ignorieren, kein closed loop möglich
    {
        flags.lastMoveType = flags.UNITS;
        myAxes.Axis[axis].positionUnitsTarget = units;
        ui->lineEdit_Position_Aim->setText(QString("%L1").arg(units, 0, 'f', 8));

        if( (myAxes.Axis[axis].indexEncoder_A == 0) && (myAxes.Axis[axis].indexEncoder_B == 0) )     // open loop wenn keine Encoder vorhanden
        {
            qint64 absSteps =(qint64)(myAxes.Axis[axis].steps_per_unit * units);
            myAxes.Axis[axis].positionStepsTarget = absSteps;
            ui->lineEdit_Steps_Aim->setText(QString::number(absSteps));
            MovePlan(axis, absSteps, relVel);
        }
        else
        {
            // closed loop
            myAxes.Axis[axis].CL_WaitMoveDowncount = myAxes.Axis[axis].CL_WaitMove;     // Countdown Wait scharf machen
            myAxes.Axis[axis].CL_downcountIterations = MAXITERATIONS - 1;               // Sicherheitszähler zur Begrenzung der Iterationen

            myAxes.puaBlock = true;    // schützt positionUnitsAverage vor Update, macht dadurch folgende Zuweisung atomar
            double delta = myAxes.Axis[axis].positionUnitsTarget - myAxes.Axis[axis].positionUnitsAverage;
            myAxes.puaBlock = false;

            // Logfile für die Moves:
            if(mlogfile.open(QIODevice::Append | QIODevice::Text))
            {
                QTextStream out(&mlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben
                QString line = QString("%L1 U %L2 %L3  %L4 %L5 %L6").arg(QTime::currentTime().toString())
                        .arg(axis+1).arg(myAxesDialog.axisName[axis])
                        .arg(myAxes.Axis[axis].positionUnitsAverage).arg(delta).arg(myAxes.Axis[axis].positionUnitsTarget);
                if(myAxes.puaSplit)
                {
                    line += " puaSplit";        // Diagnostik
                    myAxes.puaSplit = false;    // Flag rücksetzen
                }
                line += "\n";
                out << line;
                mlogfile.close();
            }
            myAxes.Axis[axis].isMovingClosedLoop = true;    // closed loop für diesen einen Move aktivieren
            MovePlan(axis, myAxes.Axis[axis].positionSteps + (int)(delta * myAxes.Axis[axis].CL_Approach * myAxes.Axis[axis].steps_per_unit), relVel); // setzt status auf 2 hoch
        }
    }
}


// Bewegung einer Achse im closed loop Modus starten, mit Units als Zielvorgabe implizit in "positionUnitsTarget"
void ecMainDialog::MoveDeltaUnits(int axis, double deltaUnits, double relVel)
{
    if(!myAxes.errorEncoderGlobal)  // diesen Befehl bei Encoder Fehler ignorieren
    {
        flags.lastMoveType = flags.UNITS;
        myAxes.Axis[axis].positionUnitsTarget += deltaUnits;
        ui->lineEdit_Position_Aim->setText(QString("%L1").arg(myAxes.Axis[axis].positionUnitsTarget, 0, 'f', 8));

        if( (myAxes.Axis[axis].indexEncoder_A == 0) & (myAxes.Axis[axis].indexEncoder_B == 0) )     // open loop wenn keine Encoder vorhanden
        {
            qint64 absSteps =(qint64)(myAxes.Axis[axis].steps_per_unit * myAxes.Axis[axis].positionUnitsTarget);
            myAxes.Axis[axis].positionStepsTarget = absSteps;
            ui->lineEdit_Steps_Aim->setText(QString::number(absSteps));
            MovePlan(axis, absSteps, relVel);
        }
        else
        {
            myAxes.Axis[axis].CL_WaitMoveDowncount = myAxes.Axis[axis].CL_WaitMove;     // Countdown Wait scharf machen
            myAxes.Axis[axis].CL_downcountIterations = MAXITERATIONS - 1;               // Sicherheitszähler zur Begrenzung der Iterationen

            myAxes.puaBlock = true;    // schützt positionUnitsAverage vor Update, macht dadurch folgende Zuweisung atomar
            double delta = myAxes.Axis[axis].positionUnitsTarget - myAxes.Axis[axis].positionUnitsAverage;
            myAxes.puaBlock = false;

            // Logfile für die Moves:
            if(mlogfile.open(QIODevice::Append | QIODevice::Text))
            {
                QTextStream out(&mlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben
                QString line = QString("%L1 U %L2 %L3  %L4 %L5 %L6").arg(QTime::currentTime().toString())
                        .arg(axis+1).arg(myAxesDialog.axisName[axis])
                        .arg(myAxes.Axis[axis].positionUnitsAverage).arg(delta).arg(myAxes.Axis[axis].positionUnitsTarget);
                if(myAxes.puaSplit)
                {
                    line += " puaSplit";        // Diagnostik
                    myAxes.puaSplit = false;    // Flag rücksetzen
                }
                line += "\n";
                out << line;
                mlogfile.close();
            }
            myAxes.Axis[axis].isMovingClosedLoop = true;    // closed loop für diesen einen Move aktivieren
            MovePlan(axis, myAxes.Axis[axis].positionSteps + (int)(delta * myAxes.Axis[axis].CL_Approach * myAxes.Axis[axis].steps_per_unit), relVel); // setzt status auf 2 hoch
        }
    }
}

// Bewegung einer Achse im closed loop Modus starten, mit Encoder Counts als Vorgabe, noch nicht fertig!!
void ecMainDialog::MoveToCounts(int axis, qint64 counts, double relVel)
{

    /*
       flags.lastMoveType = flags.COUNTS;
        myAxis.Axis[axis].CL_WaitMoveDowncount = myAxis.Axis[axis].CL_WaitMove;     // Countdown Wait scharf machen
        myAxis.Axis[axis].CL_downcountIterations = MAXITERATIONS - 1;               // Sicherheitszähler zur Begrenzung der Iterationen

        //double delta = myAxis.Axis[axis].positionUnitsTarget - myAxis.Axis[axis].positionUnitsAverage;    ????

        //MovePlan(axis, myAxis.Axis[axis].positionSteps + (int)(delta * myAxis.Axis[axis].CL_Approach * myAxis.Axis[axis].steps_per_unit)); // setzt status auf 2 hoch
    }
    else
        if(myAxis.Axis[axis].doesStabMoving)    // Stabilisierung unterbrechen und trotzdem fahren
        {
            StopAxis(axis);
            flags.lastMoveType = flags.COUNTS;
            myAxis.Axis[axis].CL_WaitMoveDowncount = myAxis.Axis[axis].CL_WaitMove;     // Countdown Wait scharf machen
            myAxis.Axis[axis].CL_downcountIterations = MAXITERATIONS - 1;               // Sicherheitszähler zur Begrenzung der Iterationen
        }
        */
}

void ecMainDialog::MoveDeltaCounts(int axis, qint64 deltaCounts, double relVel)
{

}

//
//  übernimmt implizit myNetwork.enName als Parameter
void ecMainDialog::Initialize_TCPIP_Interface()
{
    // die Serverfunktion starten, auf Port xxx auf Client lauschen:
    // muss für Autostart verzögert zum Schluss erfolgen
    QNetworkInterface myIf = QNetworkInterface::interfaceFromName(myNetwork.enName);
    //qDebug() << myIf;     // vollständige Netzwerkinfo, nur für Debug
    if(myIf.isValid())
    {
        QHostAddress myIp;
        if(myIf.addressEntries().isEmpty())
        {
            // Mit EtherCAT verwechselt, oder NIC auf DHCP und kein Netzwerkkabel angesteckt?
            // Keine IP Adresse verfügbar, localhost verwenden:
            qDebug() << "Warning: Server NIC" << myNetwork.enName << "has no configured IP Address, using localhost.";
            //ui->label_IPandPort->setText("Error: Server NIC has no configured IP Address.");
            myIp.setAddress("127.0.0.1");
        }
        else
        {
            if(myNetwork.isLocalLoopBack)   // Dann immer lokal
            {
                myIp.setAddress("127.0.0.1");
            }
            else
            {
                // die TCP-IP Schnittstelle für die externe Steuerung durch den Prozess-Server auf einem anderen Rechner des Intranets.
                myIp = myIf.addressEntries().first().ip();
            }
        }
        // qDebug() << myIp;
        myServer = new QTcpServer(this);
        quint16 serverport = myNetwork.enPort.toInt();
        if(!myServer->listen(myIp, serverport)) // aktiviert den Server für ankommende Verbindungsversuche auf SERVERPORT
        {
            fprintf(stderr, "Error: TCP/IP Server could not start.\n");
            ui->label_IP->setText("Server could not start");
        }
        else
        {
            qDebug() << "Ok: TCP/IP Server on" << myIp.toString() << "Port" << myNetwork.enPort << "started successfully.";

            ui->label_IP->setText("Listening on " + myIp.toString());
            ui->label_Port->setText("Port " + QString("%L1").arg(serverport));

            connect(myServer, &QTcpServer::newConnection, this, &ecMainDialog::connectClient);  // Signal mit Funktion verbinden
        }

    }
    else
    {
        ui->label_IP->setText("Error: Network Adapter Name invalid.");
        fprintf(stderr, "Error: No NIC for TCP/IP found. Wrong name?\n");
    }
}


void ecMainDialog::MoveAxisOrDof(int axis, double position)
{
    if(axis < myAxes.numAxis)
        MoveToUnits(axis, position, 1.0);   // gewöhnliche Achse fahren
    else
    {
        axis -= myAxes.numAxis;             // die DOFs als PseudoAchse
        switch(axis)
        {
        case 0:
            hexa.dof_X = position;
            break;
        case 1:
            hexa.dof_Y = position;
            break;
        case 2:
            hexa.dof_Z = position;
            break;
        case 3:
            hexa.dof_Yaw = position;
            break;
        case 4:
            hexa.dof_Pitch = position;
            break;
        case 5:
            hexa.dof_Roll = position;
            break;
        }

        DOF_to_Axes();                  // Position der Motorachsen berechnen, alle Parameter bis auf die PseudoAchse sind im global hexa schon drin
        hexapodDialog->SetContent();

        double maxDelta, delta[6], velFactor;
        // den längsten Fahrweg suchen
        maxDelta = 0;
        for(int i = 0; i < 6; ++i)
        {
            delta[i] = fabs(hexa.AxisH[i] - myAxes.Axis[hexa.Axis_Number[i]-1].positionUnits);
            if(delta[i] > maxDelta)
                maxDelta = delta[i];
        }

        for(int i = 0; i < 6; ++i)          // für einen DOF müssen immer alle sechs Hexapod-Achsen fahren
        {
            if(maxDelta > 0)
                velFactor = delta[i] / maxDelta;
            else
                velFactor = 1.0;
            MoveToUnits(hexa.Axis_Number[i]-1, hexa.AxisH[i], velFactor);
        }
    }
}


// zyklische QTimer-Schleife, nicht RT, läuft mit 10 Hz
// aktualisiert GUI mit laufenden Werten, macht die Targetkorrektur und speichert automatisch Parameter ab
void ecMainDialog::myqTimerTask()
{
    double point ;

    if(myDialogs.isOpen_MoveSequence)
    {
        if(moveSequence.firstMove)
        {
            ecMainDialog::MoveAxisOrDof(moveSequence.axis, moveSequence.startPoint);  // schon mal sofort zum Startpunkt
            moveSequence.firstMove = false;
        }

        if(moveSequence.iterationCounter > 0)
        {
            if(moveSequence.waitTimeCounter > 0)
            {
                moveSequence.waitTimeCounter -= 1;
                movesequenceDialog->UpdateContent();
                if(moveSequence.waitTimeCounter == 1)                       // billiger Trick, startet das Timing der Moves zuverlässig
                    moveSequence.nextTime = ecatState.msCounter / 1000 + 1;     // wird hier am Ende der Wartezeit initialisiert
            }
            else
            {
                if(1000 * moveSequence.nextTime < ecatState.msCounter)
                {
                    moveSequence.nextTime += moveSequence.timePerStep;
                    if(moveSequence.reverse)
                    {

                        point = (moveSequence.endPoint - moveSequence.startPoint) * (double)moveSequence.subdivisionCounter / (double)moveSequence.subDivisions  + moveSequence.startPoint;
                        MoveAxisOrDof(moveSequence.axis, point);
                        moveSequence.subdivisionCounter -= 1;
                        if( moveSequence.subdivisionCounter <  0)
                        {
                            moveSequence.reverse = false;
                            moveSequence.subdivisionCounter = 1;
                            moveSequence.iterationCounter -= 1;
                            movesequenceDialog->UpdateContent();
                        }
                    }
                    else    // Wartezeit ist abgelaufen, nächster Move:
                    {
                        point = (moveSequence.endPoint - moveSequence.startPoint) * (double)moveSequence.subdivisionCounter / (double)moveSequence.subDivisions  + moveSequence.startPoint;
                        MoveAxisOrDof(moveSequence.axis, point);
                        moveSequence.subdivisionCounter += 1;
                        if( moveSequence.subdivisionCounter >  moveSequence.subDivisions)
                        {
                            moveSequence.reverse = true;
                            moveSequence.subdivisionCounter = moveSequence.subDivisions-1;
                        }
                    }
                }
            }
        }
    }

    // wenn Bewegung regulär abgeschlossen oder abgebrochen (Limit, Notstop), einmalig die GUI Anzeige aktualisieren, hier aber keine Werte ändern
    if(flags.refreshMoveFinished)                   // wenn Bewegung regulär abgeschlossen oder abgebrochen (Limit, Notstop)
    {
        flags.refreshMoveFinished = false;          // einmalig Anzeige aktualisieren

        ui->lineEdit_Steps_Aim->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionStepsTarget));

        if(myAxes.Axis[myAxes.currentAxis].axisType == AXISTYPE_DELTASTEPS)
            ui->lineEdit_Position_Aim->setText("none");
        else
            ui->lineEdit_Position_Aim->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionUnitsTarget, 0, 'f', 8));

        ui->checkBox_GlobalStab->setChecked(myAxes.isStabilizedGlobal);
        writeAxesState();

    }

    switch(ecatState.workingCounterState)
    {
    case WKC_INIT:
        ui->lineEdit_Status->setText("Initializing...");
        break;
    case WKC_OK:
        ui->lineEdit_Status->setText("EtherCAT Ok");
        ui->lineEdit_Status->setPalette(palOk);
        break;
    case WKC_ERR:
        ui->lineEdit_Status->setText("EtherCAT Error");
        ui->lineEdit_Status->setPalette(palErr);
        break;
    }

    //if(ecatState.allSlavesOperational) ui->lineEdit_Status_3->setText("Operating...");
    ui->lineEdit_Status_3->setText("ErrorCount = " + QString("%L1").arg(ecatState.numErrorCycles));

    //ui->lineEdit_eCycle->setText(QString("cyCount = %1").arg(ecatState.eCycleCounter));
    ui->lineEdit_msCount->setText(QString("msCount = %1").arg(ecatState.msCounter));


    // nur für debug auf die Konsole gehen:
    //fprintf(stderr, "Jitter = %4d %4d %4d %4d\n", ecatState.deltaJitter, ecatState.deltaLoop, ecatState.deltaJitterMax, ecatState.deltaLoopMax);


    //sprintf(string, "TrigCount= %d", diag_StepsIst);    // nur zu Testzwecken
    //diag_StepsIst = get_ECM712_StepsIst(1, 1);
    //encoderDialog->ui->tableWidget_Encoder->setItem(4, column, new QTableWidgetItem(QString("%L1").arg(qint64_Wert)));


    //
    //      In Zukunft wird aller graphischer Inhalt immer in der Timer Schleife aktualisiert, ob nötig oder nicht.
    //      Das macht die Programm-Logik viel einfacher und übersichtlicher
    //

    MainDialogUpdateContent();        // Das Hauptfenster ist immer offen

    // Ganz wichtig, um ein übles Memory Leak zu verhindern! Nur in die Zellen schreiben, wenn das Fenster offen ist und
    // damit auch das tableWidget_Encoder Objekt initialisiert ist. Dann übernimmt es die mit new erzeugten Items und löscht die alten.
    if(myDialogs.isOpen_Axis)
        axisDialog->UpdateContent();

    if(myDialogs.isOpen_Encoder)
        encoderDialog->UpdateContent();

    if(myDialogs.isOpen_IO_Beckhoff)
        beckhoffDialog->UpdateContent();

    if(myDialogs.isOpen_SourceControl)
        sourceControlDialog->UpdateContent();


    QString status;

    switch(myAxes.statusMotionGlobal)
    {
    case GSTATUS_ISMOVING:
        status = "Moving ..";   // mindestesn eine Achse fährt
        break;
    case GSTATUS_IDLE:
        status = "Idle ..";     // Grundzustand, warte auf Bewegungsbefehle
        break;
    case GSTATUS_ELH:
        status = QString("Global Stop: Encoder %1 above Limit High").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_ELL:
        status = QString("Global Stop: Encoder %1 below Limit Low").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_ENCODER_ZEROLOST:
        status = QString("Global Stop: Encoder %1 lost Reference").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_ENCODER_ERROR:
        status = QString("Global Stop: Encoder %1 not functional").arg(myAxes.statusMotionArg); // genutzter Encoder nicht angeschlossen oder gestört
        break;
    case GSTATUS_FLIMIT:
        status = QString("Axis %1 reached Forward Limit Switch").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_RLIMIT:
        status = QString("Axis %1 reached Reverse Limit Switch").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_FOLLOWING_ERROR:
        status = QString("Global Stop: Axis %1 has Following Error").arg(myAxes.statusMotionArg);
        break;
    case GSTATUS_TOPOLOGY_ERROR:
        status = "Global Stop: Topology Error";
        break;
    case GSTATUS_ECAT_ERROR:
        status = "Global Stop: EtherCAT Error";
        break;

    }

    if(flags.doWriteRotationState)
    {
        flags.doWriteRotationState = false;   // Flag wieder löschen
        writeRotationState();
    }

    ui->label_statusMotion->setText(status);


    // Den Zustand des Programms alle 60 Sekunden abspeichern:
    if(ecatState.msCounter > ecatState.protoCounter)
    {
        ecatState.protoCounter += 60000;
        if(protofile.open(QIODevice::Append | QIODevice::Text))
        {
            QTextStream out(&protofile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben
            QString line = QString("%L1 %L2 %L3 %L4 %L5 %L6 %L7 %L8 %L9 %L10\n").arg(QTime::currentTime().toString()).arg(ecatState.msCounter, 9)
                    .arg(ecatState.deltaSleep, 6).arg(ecatState.deltaSleepMax, 6).arg(ecatState.deltaJitter, 6).arg(ecatState.deltaJitterMax, 6).arg(ecatState.deltaLoop, 6).arg(ecatState.deltaLoopMax, 6)
                    .arg(ecatState.workingCounterState).arg(ecatState.numErrorCycles);
            out << line;
            protofile.close();
        }
    }


    // Wenn aktiv die Encoder-Counts zehnmal die Sekunde loggen:
    if(flags.doLoggingEncoder)
    {
        if(ecatState.msCounter > logCounter)
        {
            logCounter += 100;      // alle 100 ms einen Wert schreiben

            if(encoderlogfile.open(QIODevice::Append | QIODevice::Text))
            {
                QTextStream out(&encoderlogfile);                                   // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

                double timeStamp = (double)QDateTime::currentMSecsSinceEpoch();     // die Millisekunden seit 1970 UTC
                timeStamp *= 0.001;
                out << QString("%1").arg(timeStamp, 0, 'f', 2);                     // Zeitstempel mit Dezimalpunkt und zwei Nachkommastellen für Igor Pro
                for(int i=0; i < myEncoderChannels.numEncoder; ++i)
                {
                    if(myEncoderChannels.Encoder[i].doLogging)
                        out << "\t" << myEncoderChannels.Encoder[i].counts << "\t" << myEncoderChannels.Encoder[i].counts_mean;         // \t = horizontaler Tabulator
                }
                out << "\n";
                encoderlogfile.close();              
            }

            if(quicklogfile.open(QIODevice::Append | QIODevice::Text))
            {
                QTextStream out(&quicklogfile);                             // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben
                if(quick.first)
                {
                    for(int i=0; i < myEncoderChannels.numEncoder; ++i)
                    {
                        quick.Offset[i] = myEncoderChannels.Encoder[i].counts_mean;     // einmalig merken
                    }
                    quick.first = false;
                }

                out << QString("%1").arg(quick.index);                        // laufende Nummer für Quickplot
                quick.index += 1;
                for(int i=0; i < myEncoderChannels.numEncoder; ++i)
                {
                    if(myEncoderChannels.Encoder[i].doLogging)
                        out << " " << myEncoderChannels.Encoder[i].counts_mean - quick.Offset[i];         // nur die gleitenden Mittelwerte
                }
                out << "\n";
                quicklogfile.close();
            }
        }
    }

    // wenn OtF capture abgeschlossen, die Werte in eine Datei schreiben
    if(myAxes.otfReady)
    {
        myAxes.otfReady = false;
        writeOtfData();                         // hier werden die Rohdaten counts zu Units und den Achsen zugeordnet

        //if(clientConnection->isOpen())
        if(myAxes.otfIsRemote)                  // Messung wurde von Borys über TCP/IP bestellt
        {
            myAxes.otfIsRemote = false;         // remote Flag löschen
            if(myNetwork.enConnected)
                clientConnection->write("OtF Finished\r\n");   // Signal an Server, er kann sich jetzt die Rohdaten holen
        }
    }
}


void ecMainDialog::setUnitText(int unitType)
{
    switch(unitType)
    {
    case 0:
        ui->label_unit->setText(UNITk_1);
        break;
    case 1:
        ui->label_unit->setText(UNITk_2);
        break;
    case 2:
        ui->label_unit->setText(UNITk_3);
        break;
    default:
        ui->label_unit->setText("invalid");
        break;
    }
}

void ecMainDialog::setFeedbackText(int axis)
{
    // Nur Encoder A genutzt
    if((myAxes.Axis[axis].indexEncoder_A > 0) && (myAxes.Axis[axis].indexEncoder_B == 0))
        ui->label_feedback->setText("from Encoder A");

    // Nur Encoder B genutzt
    if((myAxes.Axis[axis].indexEncoder_B > 0) && (myAxes.Axis[axis].indexEncoder_A == 0))
        ui->label_feedback->setText("from Encoder B");

    // Beide Encoder A und B genutzt, dies aktiviert implizit Mittelwertbildung, feedbackType wird hier nicht mehr genutzt
    if((myAxes.Axis[axis].indexEncoder_A > 0) && (myAxes.Axis[axis].indexEncoder_B > 0))
        ui->label_feedback->setText("from Mean of A,B");

    // Kein Encoder genutzt
    if( (myAxes.Axis[axis].indexEncoder_A == 0) && (myAxes.Axis[axis].indexEncoder_B == 0) )    // keine Encoder vorhanden
        ui->label_feedback->setText("none");
}



void ecMainDialog::on_pushButton_SaveConfiguration_clicked()
{
    QString path;
    QFile myFile;

    if(myAxes.numAxis != myAxes.numAxisConfig)
    {
        int ret = QMessageBox::critical(NULL, "Attention", "Number of actual Axes differs from configured ones! Do you want to overwrite the Configuration?",  QMessageBox::Yes | QMessageBox::No);
        if(ret == QMessageBox::No) return;
    }

    path = QDir::homePath() + "/Config_Motion/" + myAxesDialog.defaultFileName;     // home/user/Config_Motion für die Einstellungen

    QString fileName = QFileDialog::getSaveFileName(this,  "Save Configuration to File", path, "Config File (*.cfg)");
    if (!fileName.isEmpty())
    {
        // die Konfiguration speichern
        myFile.setFileName(fileName);
        if (myFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            writeConfiguration(&myFile);
            myFile.close();
        }

        // den Namen der aktiven Konfiguration speichern, nur die Datei ohne Pfad
        myAxesDialog.defaultFileName = fileName.mid(fileName.lastIndexOf("/") + 1);
        ui->textBrowser_DefaultFileName->setText(myAxesDialog.defaultFileName);

        fileName = QDir::homePath() + "/Config_Motion/ecToolDefault.cfg";     // home/user/Config_Motion/ecToolDefault.cfg als feste Indexdatei für die Einstellungen
        myFile.setFileName(fileName);
        if (myFile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            writeDefaultConfig(&myFile);
            myFile.close();
        }


    }
}

void ecMainDialog::on_pushButton_LoadConfiguration_clicked()
{
    QString myPath;

    myPath = QDir::homePath() + "/Config_Motion/" + myAxesDialog.defaultFileName;     // home/user/Configuration für die Einstellungen
    //myPath = QDir::currentPath();

    QString fileName = QFileDialog::getOpenFileName(this,  tr("Open Configuration"), myPath, tr("Config File (*.cfg)"));
    if (!fileName.isEmpty())
    {
        QFile myFile(fileName);
        if (myFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            readConfiguration(&myFile);
            myFile.close();

            // den Namen der aktiven Konfiguration speichern, nur die Datei ohne Pfad
            myAxesDialog.defaultFileName = fileName.mid(fileName.lastIndexOf("/") + 1);
            ui->textBrowser_DefaultFileName->setText(myAxesDialog.defaultFileName);

            fileName = QDir::homePath() + "/Config_Motion/ecToolDefault.cfg";     // home/user/Config_Motion/Default.cfg als feste Indexdatei für die aktive Konfiguration
            myFile.setFileName(fileName);
            if (myFile.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                writeDefaultConfig(&myFile);
                myFile.close();
            }

            ui->lineEdit_Status->setText("Restarting...");
            ui->lineEdit_Status->repaint();

            Restart_Ecat(myNetwork.ecName.toLatin1().data());       // EtherCAT mit den geladenen Parametern neu starten
            InitConfigMainDialog();
            InitAxisListMainDialog();
            ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionStepsTarget));  // wird in Restart_Ecat() auf 0 gesetzt

            axisDialog->InitContent();
            encoderDialog->InitContent();
            configurationDialog->InitContent();
            otfTonicDialog->InitContent();      // die Führungsachse auswählen
            sourceControlDialog->InitContent();
            //hexapodDialog->SetContent();

            //movesequenceDialog->InitContent();    hier nötig??

            // die Serverfunktion starten, auf Port xxx auf Client lauschen:
            if(myNetwork.enName != "")
            {
                if(myServer != NULL)
                {
                    myServer->close();
                    myServer = NULL;
                }
                Initialize_TCPIP_Interface();
            }
            else {
                ui->label_IP->setText("Intranet Server deaktivated.");
                fprintf(stderr, "Ethernet Eintrag leer: Intranet deaktiviert!\n");
            }

        }
    }
}


void ecMainDialog::on_pushButton_SaveLastMove_clicked()
{
    QString line;

    line = QDir::homePath() + "/Data_Motion/lastMove.csv";

    QFile file(line);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&file); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        int axis = myAxes.currentAxis;
        for(int i = 0; i < myAxes.Axis[axis].indexPath-1; i++)
        {
            line = QString("%L1").arg(i);      // die Millisekunden
            if(flagsDebug.steps)
            {
                line += QString(" %L1 %L2").arg(myAxes.Axis[axis].lastMoveSteps[i]).arg(myAxes.Axis[axis].lastMoveStepsIst[i]);
            }

            line += QString(" %L1 %L2").arg(myAxes.Axis[axis].lastMovePosition[i], 0, 'f', 7).arg(myAxes.Axis[axis].lastMovePositionAverage[i], 0, 'f', 7);

            if(flagsDebug.acc)
            {
                line += QString(" %L1").arg(myAxes.Axis[axis].lastMoveSteps[i+1]-myAxes.Axis[axis].lastMoveSteps[i]);   // das Differential der Sollkurve
                //line += QString(" %L1").arg(myAxis.Axis[axis].lastMoveStepsIst[i+1]-myAxis.Axis[axis].lastMoveStepsIst[i]);    // das Differential der Istwerte einschließlich eCat Jitter
            }
            if(flagsDebug.res)
            {
                line += QString(" %L1").arg(myAxes.Axis[axis].lastMoveDeltaFollow[i]);                  // das Residual der Schleppfehlerüberwachung
            }
            if(flagsDebug.enc > 0)
            {
                line += QString(" %L1").arg(myEncoderChannels.lastMoveDebugEnc[i], 0, 'f', 7);          // zusätzlicher Encoder für Diagnostik
            }
            if(flagsDebug.encStatus > 0)
            {
                line += QString(" %L1").arg(myEncoderChannels.lastMoveDebugEncStatus[i]);               // zusätzlicher Encoder Status für Diagnostik
            }
            line += "\n";

            if(flagsDebug.decimal == comma)
                line.replace(".", ",");
            else
                line.replace(",", ".");

            out << line;
        }
        out << "END_OF_FILE\n";
        file.close();
    }

}


void ecMainDialog::on_pushButton_ecRestart_clicked()
{
    ui->lineEdit_Status->setText("Restarting...");
    ui->lineEdit_Status->repaint();
    Restart_Ecat(myNetwork.ecName.toLatin1().data());   // EtherCAT neu starten und enumerieren

    InitConfigMainDialog();                                       // die GroupBox "EtherCAT" neu aufbauen
    InitAxisListMainDialog();                         // die Liste aller Achsen in der GroupBox "Motion" des Hauptdialogs neu aufbauen

    ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionStepsTarget)); // wird in Startup_Ecat in Restart_Ecat() auf 0 gesetzt

    //axisDialog->InitContent(); ?
    //encoderDialog->InitContent(); ?
}

void ecMainDialog::MainDialogUpdateContent()      // wird von QTimer aufgerufen
{
    // die Bewegung:
    ui->lineEdit_Steps->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionSteps));

    //sprintf(cstring, "%d", myAxis.Axis[myAxis.currentAxis].positionCounts);

    if( (myAxes.Axis[myAxes.currentAxis].indexEncoder_A == 0) & (myAxes.Axis[myAxes.currentAxis].indexEncoder_B == 0) )     // keine Encoder vorhanden
    {
        if(myAxes.Axis[myAxes.currentAxis].axisType == AXISTYPE_DELTASTEPS)
            ui->lineEdit_Position->setText("none");     // bei Typ 2 keine Position anzeigen, nicht sinnvoll
        else
            ui->lineEdit_Position->setText(QString("%L1").arg((double)myAxes.Axis[myAxes.currentAxis].positionSteps / myAxes.Axis[myAxes.currentAxis].steps_per_unit, 0, 'f', 6));    // aus den Steps berechnet
    }
    else
    {
        ui->lineEdit_Position->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionUnitsAverage, 0, 'f', 8));
    }

    if(myAxes.Axis[myAxes.currentAxis].indexEncoder_A > 0)
    {
        //sprintf(cstring, "%d", myEncoders.Encoder[myAxis.Axis[myAxis.currentAxis].indexEncoder_A -1].counts);    // das Encoder[] array fängt bei 0 an
        ui->lineEdit_Counts_A->setText(QString("%L1").arg(myEncoderChannels.Encoder[myAxes.Axis[myAxes.currentAxis].indexEncoder_A -1].counts));  // die Encoder Counts immer unsigned
        sprintf(cstring, "%f", myEncoderChannels.Encoder[myAxes.Axis[myAxes.currentAxis].indexEncoder_A -1].positionUnits);
        ui->lineEdit_Position_A->setText(cstring);
    }
    else
    {
        ui->lineEdit_Counts_A->setText("none");
        ui->lineEdit_Position_A->setText(" - ");
    }

    if(myAxes.Axis[myAxes.currentAxis].indexEncoder_B > 0)
    {
        //sprintf(cstring, "%d", myEncoders.Encoder[myAxis.Axis[myAxis.currentAxis].indexEncoder_B -1].counts);
        ui->lineEdit_Counts_B->setText(QString("%L1").arg(myEncoderChannels.Encoder[myAxes.Axis[myAxes.currentAxis].indexEncoder_B -1].counts));  // die Encoder Counts immer unsigned
        sprintf(cstring, "%f", myEncoderChannels.Encoder[myAxes.Axis[myAxes.currentAxis].indexEncoder_B -1].positionUnits);
        ui->lineEdit_Position_B->setText(cstring);
    }
    else
    {
        ui->lineEdit_Counts_B->setText("none");
        ui->lineEdit_Position_B->setText(" - ");
    }


    if(myAxes.isMovingGlobal)
        ui->radioButton_MoveFlag->setChecked(true);
    else
        ui->radioButton_MoveFlag->setChecked(false);

    if(myAxes.Axis[myAxes.currentAxis].limitForward)
        ui->radioButton_Limit_Forward->setChecked(true);
    else
        ui->radioButton_Limit_Forward->setChecked(false);

    if(myAxes.Axis[myAxes.currentAxis].limitReverse)
        ui->radioButton_Limit_Reverse->setChecked(true);
    else
        ui->radioButton_Limit_Reverse->setChecked(false);


    // die Hilfseingänge in Gelb und invertiert:
    if(myAxes.Axis[myAxes.currentAxis].limitAux1)
        ui->radioButton_Limit_Aux1->setChecked(false);
    else
        ui->radioButton_Limit_Aux1->setChecked(true);

    if(myAxes.Axis[myAxes.currentAxis].limitAux2)
        ui->radioButton_Limit_Aux2->setChecked(false);
    else
        ui->radioButton_Limit_Aux2->setChecked(true);


    if(myAxes.statusMotionGlobal == GSTATUS_FOLLOWING_ERROR)
        ui->radioButton_FollowingError->setChecked(true);
    else
        ui->radioButton_FollowingError->setChecked(false);

    if(myAxes.errorEncoderGlobal)
        ui->radioButton_Error_Encoder->setChecked(true);
    else
        ui->radioButton_Error_Encoder->setChecked(false);

    if( myAxes.ignoreLimits == false && ui->checkBox_IgnoreLimits->isChecked() )   // nach einen Move ignore zurücksetzen
        ui->checkBox_IgnoreLimits->setChecked(false);
}

// Die Liste der verfügbaren Achsen im Hauptfenster unter "Motion"
void ecMainDialog::InitAxisListMainDialog()      // wird bei Programmstart, Wechsel der aktuellen Achse und Doppelklich im Achsendialog, aber nicht bei QTimer aufgerufen
{
    if(myAxes.Axis[myAxes.currentAxis].isStabilized)
        ui->checkBox_Stabilize->setChecked(true);
    else
        ui->checkBox_Stabilize->setChecked(false);

    switch(myAxes.Axis[myAxes.currentAxis].unitType)
    {
    case 0:
        ui->label_unit->setText("(mm)");
        break;
    case 1:
        ui->label_unit->setText("(mrad)");
        break;
    case 2:
        ui->label_unit->setText("(grad)");
        break;
    default:
        ui->label_unit->setText("invalid");
        break;
    }

    setFeedbackText(myAxes.currentAxis);

    ui->listWidget_Axis->clear();   // komplett löschen, die Liste wird jedesmal neu aufgebaut
    for(int i=0; i< myAxes.numAxis; i++)
    {
        if(myAxes.Axis[i].isGantry == 0)
            ui->listWidget_Axis->addItem(QString("Axis %1 T%2  %3").arg(i+1, 2).arg(myAxes.Axis[i].axisType).arg(myAxesDialog.axisName[i]));
        else
        {
            ui->listWidget_Axis->addItem(QString("Axis %1 G%2  G%3").arg(i+1, 2).arg(myAxes.Axis[i].isGantry).arg(myAxesDialog.axisName[i]));
            ui->listWidget_Axis->item(i)->setForeground(gantryBrush);
            //ui->listWidget_Axis->item(i)->setHidden(true);
        }
    }
    ui->listWidget_Axis->setCurrentRow(myAxes.currentAxis);  
}

void ecMainDialog::InitConfigMainDialog()   // wird nur bei Programmstart, Restart EtherCAT und Load Configuration aufgerufen
{

    if(checkConfiguration()) {
        ui->lineEdit_StatusEcat->setPalette(palOk);
        ui->lineEdit_StatusEcat->setText("Topology Ok");
        if(myAxes.statusMotionGlobal == GSTATUS_TOPOLOGY_ERROR)
            myAxes.statusMotionGlobal = GSTATUS_IDLE;       // Fehler löschen, wenn es die Topologie war
    }
    else
    {
        ui->lineEdit_StatusEcat->setPalette(palErr);
        ui->lineEdit_StatusEcat->setText("Topology Mismatch");
        myAxes.statusMotionGlobal = GSTATUS_TOPOLOGY_ERROR;
    }

    ui->textBrowser_Comment->setText(myAxesDialog.comment);  // Kommentar aus der Konfigurations-Datei anzeigen, mehrzeilig mit Wrap

}


void ecMainDialog::on_checkBox_IgnoreLimits_clicked()
{
    myAxes.ignoreLimits = ui->checkBox_IgnoreLimits->isChecked();
}

void ecMainDialog::on_lineEdit_Tail_editingFinished()
{
    myAxes.lastMoveTail = ui->lineEdit_Tail->text().toInt();
    ui->lineEdit_Tail->setText(QString::number(myAxes.lastMoveTail));

}


void ecMainDialog::on_lineEdit_DeltaSteps_editingFinished()
{
    myAxes.Axis[myAxes.currentAxis].jogDeltaSteps = ui->lineEdit_DeltaSteps->text().toInt();
    ui->lineEdit_DeltaSteps->setText(QString::number(myAxes.Axis[myAxes.currentAxis].jogDeltaSteps));
}

void ecMainDialog::on_lineEdit_DeltaPosition_editingFinished()
{
    myAxes.Axis[myAxes.currentAxis].jogDeltaPosition = myLocale.toDouble(ui->lineEdit_DeltaPosition->text());
    ui->lineEdit_DeltaPosition->setText(myLocale.toString(myAxes.Axis[myAxes.currentAxis].jogDeltaPosition));
}

void ecMainDialog::on_lineEdit_Steps_Aim_editingFinished()
{
    myAxes.Axis[myAxes.currentAxis].positionStepsTarget = ui->lineEdit_Steps_Aim->text().toInt();
    ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionStepsTarget));
}


void ecMainDialog::on_lineEdit_Position_Aim_editingFinished()
{
    myAxes.Axis[myAxes.currentAxis].positionUnitsTarget = myLocale.toDouble(ui->lineEdit_Position_Aim->text());
    //ui->lineEdit_Position_Aim->setText(deLocale.toString(myAxis.Axis[myAxis.currentAxis].positionUnitsTarget));
    ui->lineEdit_Position->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionUnitsTarget, 0, 'f', 8));
}



void ecMainDialog::on_pushButton_JogPlus_Steps_clicked()    // die aktuelle Achse vorwärts joggen
{
    MoveDeltaSteps(myAxes.currentAxis, myAxes.Axis[myAxes.currentAxis].jogDeltaSteps, 1.0);
}

void ecMainDialog::on_pushButton_JogMinus_Steps_clicked()
{
    MoveDeltaSteps(myAxes.currentAxis, -myAxes.Axis[myAxes.currentAxis].jogDeltaSteps, 1.0);
}

void ecMainDialog::on_pushButton_MoveTo_Steps_clicked()
{
    MoveToSteps(myAxes.currentAxis, myAxes.Axis[myAxes.currentAxis].positionStepsTarget, 1.0);   // ???
}

void ecMainDialog::on_pushButton_JogMinus_Position_clicked()
{
    MoveDeltaUnits(myAxes.currentAxis, -myAxes.Axis[myAxes.currentAxis].jogDeltaPosition, 1.0);
}

void ecMainDialog::on_pushButton_JogPlus_Position_clicked()
{
    MoveDeltaUnits(myAxes.currentAxis, myAxes.Axis[myAxes.currentAxis].jogDeltaPosition, 1.0);
}

void ecMainDialog::on_pushButton_MoveTo_Position_clicked()
{
    MoveToUnits(myAxes.currentAxis, myAxes.Axis[myAxes.currentAxis].positionUnitsTarget, 1.0);
}


void ecMainDialog::on_pushButton_Stop_clicked() // sofortiger Stop aller Achsen
{
    moveSequence.iterationCounter = 0;  // auch automatische Moves abbrechen !
    StopAllAxis();
    //movesequenceDialog->RefreshContent();
}


void ecMainDialog::on_checkBox_Stabilize_clicked()
{
    myAxes.Axis[myAxes.currentAxis].isStabilized = ui->checkBox_Stabilize->isChecked();
    axisDialog->InitContent();
}


// Wechsel der aktuellen Achse im Fokus
void ecMainDialog::on_listWidget_Axis_itemClicked(QListWidgetItem *item)
{
    myAxes.currentAxis = ui->listWidget_Axis->currentRow() + 0;

    setUnitText(myAxes.Axis[myAxes.currentAxis].unitType);
    setFeedbackText(myAxes.currentAxis);

    ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[myAxes.currentAxis].positionStepsTarget));
    if(myAxes.Axis[myAxes.currentAxis].axisType == AXISTYPE_DELTASTEPS)
        ui->lineEdit_Position_Aim->setText("none");
    else
        ui->lineEdit_Position_Aim->setText(QString("%L1").arg(myAxes.Axis[myAxes.currentAxis].positionUnitsTarget, 0, 'f', 8));

    ui->lineEdit_DeltaSteps->setText(QString::number(myAxes.Axis[myAxes.currentAxis].jogDeltaSteps));
    ui->lineEdit_DeltaPosition->setText(QString::number(myAxes.Axis[myAxes.currentAxis].jogDeltaPosition));

    ui->checkBox_Stabilize->setChecked(myAxes.Axis[myAxes.currentAxis].isStabilized);

    if(myAxes.Axis[myAxes.currentAxis].isGantry)
    {
        //ui->checkBox_ClosedLoop->setChecked(false);
        ui->pushButton_JogPlus_Steps->setEnabled(false);
        ui->pushButton_JogMinus_Steps->setEnabled(false);
        ui->pushButton_MoveTo_Steps->setEnabled(false);
        ui->pushButton_JogPlus_Position->setEnabled(false);
        ui->pushButton_JogMinus_Position->setEnabled(false);
        ui->pushButton_MoveTo_Position->setEnabled(false);
    }
    else
    {
        //ui->checkBox_ClosedLoop->setChecked(false);
        if(userLevel == USERLEVELEXPERT)
        {
        ui->pushButton_JogPlus_Steps->setEnabled(true);
        ui->pushButton_JogMinus_Steps->setEnabled(true);
        ui->pushButton_MoveTo_Steps->setEnabled(true);
        ui->pushButton_JogPlus_Position->setEnabled(true);
        ui->pushButton_JogMinus_Position->setEnabled(true);
        ui->pushButton_MoveTo_Position->setEnabled(true);
        }
    }

}


// wird bei Verbindungsaufnahme mit dem Client aufgerufen
void ecMainDialog::connectClient()
{
    clientConnection = myServer->nextPendingConnection();

    quint16         peerPort = clientConnection->peerPort();
    QHostAddress 	peerAddress = clientConnection->peerAddress();

    QString         peerAddrString = peerAddress.toString();

    if(myNetwork.enIP == "" || myNetwork.enIP ==  peerAddrString)   // wenn Filter, dann nur gültige Client IP verbinden
    {

        connect(clientConnection, &QAbstractSocket::disconnected, clientConnection, &QObject::deleteLater); // Aufräumen, löschen des Socket Objekts nach Ende der Verbindung
        connect(clientConnection, &QAbstractSocket::disconnected, this, &ecMainDialog::disconnectClient);   //
        connect(clientConnection, &QIODevice::readyRead, this, &ecMainDialog::readClientData);
        ui->label_NetworkStatus->setText("Connected to "+ peerAddress.toString() + "  Port " + QString("%L1").arg(peerPort));
        myNetwork.enConnected = true;
    }
    else
    {
        clientConnection->disconnectFromHost(); // Verbindungsversuch zurückweisen
    }
}

// wird bei Verbindungsende aufgerufen
void ecMainDialog::disconnectClient()
{
    ui->label_NetworkStatus->setText("No Client");
    myNetwork.enConnected = false;
}


// Putty macht aus einer Zeile mit \r \n am Ende genau ein Datagramm, Borys Client hingegen sendet auch mehrere Zeilen in einem Paket
// Daher kann folgende Funktion eine beliebige Anzahl von Befehls-Zeilen pro Paket verarbeiten
void ecMainDialog::readClientData()
{
    QByteArray packet;
    QString  packetString, cmd;                 // die Kommandozeile
    QString  answer;                            // Rückmeldung

    // die Achsen-Befehle:
    QString  axisSpace       = "axis ";
    QString  movestepsdelta  = "movestepsdelta:axis-";     // relative Position = jog
    QString  movestepsabs    = "movestepsabs:axis-";       // absolute Position = move to
    QString  moveunitsabs    = "moveunitsabs:axis-";
    //QString  moveunitsdelta  = "moveunitsdelta:axis-";
    QString  stopaxis        = "stop:axis-";
    QString  initaxis        = "init:axis-";                // encoderlose Achse initialisieren

    QString  getstatus       = "getstatus:axis-";
    QString  getparams       = "getparams:axis-";
    QString  getglobalstatus = "getglobalstatus";          // alle Achsen fertig, Fehler ?
    QString  stopallaxis     = "stopallaxis";              // sofortiger Nothalt aller Achsen, für Zeiss "SystemStop"

    QString  setstabglobal   = "setstabglobal:";           // Stabilisierung global für alle Achsen ein- oder ausschalten, 0=aus, 1= ein

    // Positionsabfragen Encoder, auch solche ohne Achse:
    QString  getposenc       = "sens getpose:";
    //QString  getposild       = "sens getposl:";

    // Digital IO - je ein Bit für Relais, Lichtschranken, etc
    QString  diogetbit       = "dio getbit:";                  // Zustand eines Eingangs abfragen
    QString  diosetbit       = "dio setbit:";                  // Zustand eines Ausgangs setzen

    // für OTF Position Capture mit den ECM-702:
    QString otf_startscan       = "otf startscan:";             // N Sekunden gemeinsame Bewegung von GR und MR für On-the-Fly
    QString otf_getlastscan     = "otf getlastscan";            // Die pro externem Trigger-Puls gesampleten Positionen abfragen
    //QString meas_abort         = "otf abort";                // laufende Messung abbrechen, der Fast Shutter geht zu, aber der Laser (Trigger) läuft weiter
    QString otf_getnmin         = "otf getnmin:";               // Mindestzahl Sekunden für OTF-Scan abfragen, hängt von der maximal möglichen Geschwindigkeit der Achsen ab


    QString restartecat         = "ecat restart";               // damit der ProcessServer reinitialisieren kann
    QString clearfollowingerror = "clear followingerror";       // Schleppfehler löschen

    QString loggingstart        = "logging start";
    QString loggingstop         = "logging stop";

    QString process_start       = "process start";              // Plasmaprozess starten, Target läuft, Shutter geht auf
    QString process_stop        = "process stop";               // Prozess beenden

    QString getquads            = "getquads";                   // Die Quadranten der Lochkamera abfragen

    QString help                = "help";                       // Alle verfügbaren Befehle anzeigen

    int axis, enc, steps, index_rn;
    double units, relVel;

    answer = "";
    packet = clientConnection->readAll();
    packetString = QString::fromLatin1( packet );

    // Paket nach \r\n durchsuchen und in einzelne Befehle zerlegen
    index_rn = packetString.indexOf("\r\n");    // sucht jeweils nächsten Trenner von links

    while(index_rn > -1)                        // Trenner gefunden
    {
        bool validCmd = false;
        cmd =   packetString.left(index_rn);
        packetString.remove(0, index_rn + 2);       // Befehl plus \r\n abtrennen
        index_rn = packetString.indexOf("\r\n");    // sucht jeweils nächsten Trenner von links, hier schon für den nächsten Durchlauf

        ui->label_Command->setText(cmd);
        cmd = cmd.simplified(); // macht jeweils exakt ein space als Trenner
        cmd = cmd.toLower();

        if(cmd.startsWith(axisSpace))
        {
            cmd = cmd.right(cmd.size() - axisSpace.size()); // fixen Befehltyp "axis " abschneiden
            QStringList items = cmd.split(" "); // Zwei oder drei Items, Befehl mit Achsennummer und ein oder zwei Parameter

            if(cmd.startsWith(stopallaxis))
            {
                validCmd = true;
                StopAllAxis();  // sofortiger Stop aller Achsen
                answer += "Axis StopAllAxis:y\r\n";
            }

            if(cmd.startsWith(stopaxis))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - stopaxis.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis <= myAxes.numAxis && axis > 0)
                {
                    // Befehl ausführen, einzelne Achse anhalten:
                    StopAxis(axis-1);
                    answer += QString("Axis Stop:%1 y\r\n").arg(axis);
                }
                else
                {
                    // Fehlermeldung senden
                    answer += QString("Axis Stop:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
            }

            if(cmd.startsWith(initaxis))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - stopaxis.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis <= myAxes.numAxis && axis > 0)
                {
                    if(myAxes.Axis[axis-1].axisType == AXISTYPE_NORMAL)         // ist so OK, warum nicht auch ein Init auf einer Resolute Achse
                    {
                        // Befehl ausführen, einzelne Achse initialisieren:
                        if(myAxes.Axis[axis-1].statusMove == 0) // Achse fährt nicht
                        {
                            if(myAxes.Axis[axis-1].indexEncoder_A > 0)
                                myEncoderChannels.Encoder[myAxes.Axis[axis-1].indexEncoder_A-1].referencing = 1;    // Zero on Mark aktivieren
                            if(myAxes.Axis[axis-1].indexEncoder_B > 0)
                                myEncoderChannels.Encoder[myAxes.Axis[axis-1].indexEncoder_B-1].referencing = 1;    // Zero on Mark aktivieren

                            myAxes.Axis[axis-1].isIniting = 3;    // Flag für Initialisierung der Achse setzen
                            MoveDeltaSteps(axis-1, FUENFMILLE * myAxes.Axis[axis-1].initDirection, 1.0);    //  500 Millionen Steps sollten immer reichen um in den Reverse Limt zu fahren
                            answer += QString("Axis Init:%1 y\r\n").arg(axis);
                        }
                        else
                            answer += QString("Axis Init:n - Axis Number:%1 is already moving.\r\n").arg(axis);
                    }
                    else
                        answer += QString("Axis Init:n - Axis Number:%1 has wrong Axis Type.\r\n").arg(axis);
                }
                else
                {
                    // Fehlermeldung senden
                    answer += QString("Axis Init:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
            }

            if(cmd.startsWith(movestepsabs))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - movestepsabs.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis <= myAxes.numAxis && axis > 0)
                {
                    if(items.count()==2)    // Zweiter Parameter Steps vorhanden?
                    {
                        steps = items[1].toInt();

                        myAxes.currentAxis = axis-1;    // Array beginnt mit 0
                        //myAxis.Axis[myAxis.currentAxis].positionStepsTarget = steps;

                        //sprintf(cstring, "%d", myAxis.Axis[myAxis.currentAxis].positionStepsTarget);
                        //ui->lineEdit_Steps_Aim->setText(cstring);

                        MoveToSteps(myAxes.currentAxis, steps, 1.0);
                        answer += QString("Axis MoveStepsAbs:%1 y\r\n").arg(axis);
                    }
                    else
                    {
                        answer += "Axis MoveStepsAbs:n - Missing Argument Steps\r\n";
                    }
                }
                else
                {
                    // Fehlermeldung sendenAxis-%1
                    answer += QString("Axis MoveStepsAbs:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
            }

            if(cmd.startsWith(movestepsdelta))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - movestepsdelta.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis <= myAxes.numAxis && axis > 0)
                {
                    if(items.count()==2)    // Zweiter Parameter Delta Steps vorhanden?
                    {
                        steps = items[1].toInt();

                        myAxes.currentAxis = axis-1;    // Array beginnt mit 0
                        //myAxis.Axis[myAxis.currentAxis].positionStepsTarget += steps;

                        MoveDeltaSteps(myAxes.currentAxis, steps, 1.0);
                        answer += QString("Axis MoveStepsDelta:%1 y\r\n").arg(axis);
                    }
                    else
                    {
                        answer += "Axis MoveStepsDelta:n - Missing Argument Steps\r\n";
                    }

                }
                else
                {
                    answer += QString("Axis MoveStepsDelta:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
            }

            if(cmd.startsWith(moveunitsabs))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - moveunitsabs.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis > myAxes.numAxis || axis <= 0)
                    answer += QString("Axis MoveUnitsAbs:n - Invalid Axis Number:%1\r\n").arg(axis);
                else if(myAxes.Axis[axis-1].axisType == AXISTYPE_DELTASTEPS)
                    answer += QString("Axis MoveUnitsAbs:n - Axis-%1 of Type 3 can only Delta Steps\r\n").arg(axis);    // für Borys Typ 3
                else    // Befehl ausführen, mit Encoder implizit closed loop, ohne Encoder open loop nach Steps
                {
                    if(items.count()>=2)    // Mindestens zweiter Parameter Units vorhanden
                    {
                        myAxes.currentAxis = axis-1;    // Array muss vor dem if() stehen, sonst schwerer Fehler bei reduzierter Geschwindigkeit!!
                        if(items.count()==3)            // Dritter Parameter vorhanden
                        {
                            relVel = myLocale.toDouble(items[2]);   // relative velocity, argument must be <=1
                            if (relVel > 1 || relVel <= 0)
                                relVel = 1.0;
                        }
                        else
                            relVel = 1.0;
                        units = myLocale.toDouble(items[1]);
                        //myAxis.Axis[myAxis.currentAxis].positionUnitsTarget = units;
                        //myAxis.Axis[myAxis.currentAxis].positionUnitsTarget += myAxis.jogDeltaPosition;
                        //ui->lineEdit_Position_Aim->setText(deLocale.toString(myAxis.Axis[myAxis.currentAxis].positionUnitsTarget));
                        MoveToUnits(myAxes.currentAxis, units, relVel);
                        answer += QString("Axis MoveUnitsAbs:%1 y\r\n").arg(axis);
                    }
                    else
                    {
                        answer += "Axis MoveUnitsAbs:n - Missing Argument Units\r\n";
                    }
                }
            }

            if(cmd.startsWith(getstatus))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - getstatus.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis > myAxes.numAxis || axis <= 0)
                {
                    answer += QString("Axis GetStatus:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
                else    // Befehl ausführen
                {
                    //myAxis.currentAxis = axis-1;    // Fokus durch Abfrage nicht verändern !!
                    answer += QString("Axis GetStatus:%1").arg(axis);    // die Achsennummer
                    answer += " \"" + myAxesDialog.axisName[axis-1] + "\" ";
                    if(myAxes.Axis[axis-1].statusMove == 0)
                        answer += "n ";   // Achse ruht
                    else
                        answer += "y ";   // Achse fährt

                    answer += QString("%1 ").arg(myAxes.Axis[axis-1].axisType);     // für Borys: Es werden nur Typ 2 oder 3 verwendet (2= alles erlaubt, 3= nur Delta Steps)

                    if(myAxes.Axis[axis-1].isStabilized)
                        answer += "y ";   // Achse wird automatisch nachgeregelt
                    else
                        answer += "n ";

                    if(myAxes.Axis[axis-1].limitReverse)
                        answer += "1 ";   // Achse ist im Reverse Limit
                    else
                        answer += "0 ";
                    if(myAxes.Axis[axis-1].limitForward)
                        answer += "1 ";   // Achse ist im Forward Limit
                    else
                        answer += "0 ";

                    answer += myLocale.toString(myAxes.Axis[axis-1].positionSteps) + " ";
                    answer += myLocale.toString(myAxes.Axis[axis-1].positionUnits) + " ";

                    answer += QString("%1 %2\r\n").arg(!myAxes.Axis[axis-1].limitAux1).arg(!myAxes.Axis[axis-1].limitAux2); // für Borys invertiert
                }
            }

            if(cmd.startsWith(getparams))
            {
                validCmd = true;
                axis = items[0].right(items[0].size() - getparams.size()).toInt(); // fixen Befehlsteil abschneiden, Achsennummer bleibt übrig
                if(axis > myAxes.numAxis || axis <= 0)
                {
                    answer += QString("Axis GetParams:n - Invalid Axis Number:%1\r\n").arg(axis);
                }
                else    // Befehl ausführen
                {
                    answer += QString("Axis GetParams:%1").arg(axis);    // die Achsennummer
                    answer += " \"" + myAxesDialog.axisName[axis-1] + "\" ";
                    answer += QString("%L1 %L2 ").arg(myAxes.Axis[axis-1].indexEncoder_A).arg(myAxes.Axis[axis-1].indexEncoder_B);
                    answer += QString("%L1 %L2 %L3 ").arg(myAxes.Axis[axis-1].unitType).arg(myAxes.Axis[axis-1].feedbackType).arg(myAxes.Axis[axis-1].steps_per_unit);
                    answer += QString("%L1 %L2 %L3 ").arg(myAxes.Axis[axis-1].velocity).arg(myAxes.Axis[axis-1].acceleration).arg(myAxes.Axis[axis-1].hasBrake);
                    answer += QString("%L1 %L2 %L3 ").arg(myAxes.Axis[axis-1].CL_Approach).arg(myAxes.Axis[axis-1].CL_DeltaMove).arg(myAxes.Axis[axis-1].CL_WaitMove);
                    answer += QString("%L1 %L2 %L3 ").arg(myAxes.Axis[axis-1].CL_DeltaStab).arg(myAxes.Axis[axis-1].CL_Average).arg(myAxes.Axis[axis-1].isGantry);

                    answer += QString("%1 ").arg(myAxes.Axis[axis-1].axisType);     // die Art der Achse, Quickfix für Borys

                    if(myAxes.Axis[axis-1].isStabilized == 0)
                        answer += "n ";
                    else
                        answer += "y ";

                    answer += QString("%L1 %L2 %L3\r\n").arg(myAxes.Axis[axis-1].CL_WaitStab).arg(myAxes.Axis[axis-1].CL_DeltaFollow).arg(myAxes.Axis[axis-1].initDirection);

                    // "[AXIS_CONFIGURATION Num Name EncA EncB UnitT FBT StepsPerUnit Vel Acc Brake CL_App CL_DeltaM CL_WaitM CL_DeltaStab CL_Average  isGantry isCL isStab CL_WaitStab CL_DeltaF InitDir]\n";
                }
            }

            if(cmd.startsWith(setstabglobal))
            {
                validCmd = true;
                cmd = cmd.right(cmd.size() - setstabglobal.size()); // fixen Befehlsteil abschneiden
                if(cmd.toInt() == 1)
                {
                    myAxes.isStabilizedGlobal = true;
                    ui->checkBox_GlobalStab->setChecked(true);
                    answer += "Axis SetStabGlobal:1 y\r\n";
                }
                else
                {
                    myAxes.isStabilizedGlobal = false;
                    ui->checkBox_GlobalStab->setChecked(false);
                    answer += "Axis SetStabGlobal:0 y\r\n";
                }
            }



            if(cmd.startsWith(getglobalstatus))
            {
                validCmd = true;
                // Übersicht
                answer += "Axis GetGlobalStatus:";
                if(myAxes.isMovingGlobal)
                    answer += "y ";   // mindestens eine Achse fährt
                else
                    answer += "n ";   // alle Achsen ruhen
                if(myAxes.errorEncoderGlobal)
                    answer += "y ";   // mindestens ein Encoder hat einen Fehlerzustand (kein Signal, ZeroLost oder eine Deichsel schief)
                else
                    answer += "n ";   // alle Encoder sind Ok
                answer += QString("%1 %2 ").arg(myAxes.statusMotionGlobal).arg(myAxes.statusMotionArg);
                if(myAxes.isStabilizedGlobal)
                    answer += "y ";   // Stabilisierung aktiv
                else
                    answer += "n ";   // Stabilisierung für alle Achsen aus
                answer += "\r\n";
            }

        }   // Ende "axis " Befehlstyp


        if(cmd.startsWith(getposenc))   // Renishaw Resolute Encoder auslesen
        {
            validCmd = true;
            cmd = cmd.right(cmd.size() - getposenc.size()); // fixen Befehlsteil abschneiden

            enc = cmd.toInt();     // nur ein Argument, ist hier die Encoder Nummer

            if(enc > myEncoderChannels.numEncoder || enc <= 0)
            {
                answer += QString("Sens GetPosE:n - Invalid Encoder Number:%1\r\n").arg(enc);
            }
            else    // Befehl ausführen
            {
                answer += QString("Sens GetPosE:%1").arg(enc);
                answer += " \"" + myEncoderDialog.encName[enc-1] + "\" ";

                answer += myLocale.toString(myEncoderChannels.Encoder[enc-1].positionUnits);

                answer += "\r\n";
            }
        }


        int bitNo, bitValue;
        if(cmd.startsWith(diogetbit))   // einen digitalen Eingang auslesen
        {
            validCmd = true;
            cmd = cmd.right(cmd.size() - diogetbit.size()); // fixen Befehlsteil abschneiden

            bitNo = cmd.toInt();     // nur ein Argument, ist hier die Bit Nummer von 1 bis N

            if(bitNo > myInputs.numInputBits || bitNo <= 0)
            {
                answer += QString("DIO GetBit:n - Invalid Bit Number:%1\r\n").arg(bitNo);
            }
            else    // Befehl ausführen
            {
                bitValue = myInputs.state[bitNo-1];
                answer += QString("DIO GetBit:%1 %2 y\r\n").arg(bitNo).arg(bitValue);
             }
        }

        if(cmd.startsWith(diosetbit))   // einen digitalen Ausgang (alles Relais) setzen
        {
            validCmd = true;
            cmd = cmd.right(cmd.size() - diosetbit.size()); // fixen Befehlsteil abschneiden
            QStringList items = cmd.split(" ");

            bitNo = items[0].toInt();     // erstes Argument, ist hier die Bit Nummer von 1 bis N

            if(bitNo > myRelais.numRelaisBits || bitNo <= 0)
            {
                answer += QString("DIO SetBit:n - Invalid Bit Number:%1\r\n").arg(bitNo);
            }
            else    // Befehl ausführen
            {
                if(items.count()==2)    // Zweiter Parameter Bit Value vorhanden?
                {
                    bitValue = items[1].toInt();
                    myRelais.state[bitNo-1] = bitValue;     // aktualisiert über QTimer auch implizit die Dialogbox
                    answer += QString("DIO SetBit:%1 %2 y\r\n").arg(bitNo).arg(bitValue);
                }
                else
                {
                    answer += "DIO SetBit:n - Missing Argument Bit Value\r\n";
                }
            }
        }


        // Die OtF Befehle für die Tonics / ECM-702

        if(cmd.startsWith(otf_startscan))    // On-the-Fly, fährt eine oder zwei Achsen mit angepasster Geschwindigkeit, das Capture der Tonics ist implizit über die Lead-Axis
        {
            validCmd = true;
            cmd = cmd.right(cmd.size() - otf_startscan.size());      // fixen Befehlsteil abschneiden
            QStringList items = cmd.split(" ");
            if(items.count()!=5)                                // Immer fünf Items, Zahl der Sekunden, 2x Achsnummer und 2x Delta-Units
                answer += QString("OtF StartScan:n - Wrong Number of Arguments\r\n");
            else
            {
                int numSeconds = items[0].toInt();              // die gemeinsame Laufzeit der Achsen
                int otfAxis_1 = items[1].toInt();                 // hier Nummer der ersten OtF-Achse, welche mitläuft
                double deltaUnits_1 = myLocale.toDouble(items[2]);
                int otfAxis_2 = items[3].toInt();                 // hier Nummer der zweiten OtF-Achse, welche mitläuft. Wenn Null wird keine zweite Achse verwendet.
                double deltaUnits_2 = myLocale.toDouble(items[4]);

                if(myAxes.otfCapturing)     // gerade läuft eine Schussfolge
                    answer += QString("OtF StartScan:n - Already Scanning\r\n");
                else

                    if(numSeconds <= 0)
                        answer += QString("OtF StartScan:n - Number of Seconds must be greater zero:%1\r\n").arg(numSeconds);
                    else
                        if(otfAxis_1 > myAxes.numAxis || otfAxis_1 <= 0)
                            answer += QString("OtF StartScan:n - Invalid OtF Axis 1 Number:%1\r\n").arg(otfAxis_1);
                        else
                            if(otfAxis_2 > myAxes.numAxis || otfAxis_2 < 0)     // hier ist die Null erlaubt, dann läuft nur die eine OTF-Achse 1.
                                answer += QString("OtF StartScan:n - Invalid OtF Axis 2 Number:%1\r\n").arg(otfAxis_2);
                            else
                                if(deltaUnits_1 == 0 || deltaUnits_2 == 0)
                                    answer += QString("OtF StartScan:n - both OtF Intervalls must not be zero\r\n");
                                else    // Befehl OtF ausführen, OtF-Achsen starten, das Capture der Samples startet implizit über die Lead-Axis:
                                {
                                    myAxes.otfIsRemote = true;              // als remote gesteuert markieren, für die Antwort am Ende der Messung

                                    int deltaSteps_1 = deltaUnits_1 * myAxes.Axis[otfAxis_1 - 1].steps_per_unit;
                                    double velFactor_1 = double(abs(deltaSteps_1)) / double(myAxes.Axis[otfAxis_1 - 1].velocity * numSeconds);
                                    MoveDeltaSteps(otfAxis_1 - 1, deltaSteps_1, velFactor_1);       // OTF-Achse Eins mit relativer Reduzierung der Fahrgeschwindigkeit in Bewegung setzen

                                    if(otfAxis_2 > 0)  // dann auch zweite Achse fahren
                                    {
                                        int deltaSteps_2 = deltaUnits_2 * myAxes.Axis[otfAxis_2 - 1].steps_per_unit;
                                        double velFactor_2 = double(abs(deltaSteps_2)) / double(myAxes.Axis[otfAxis_2 - 1].velocity * numSeconds);
                                        MoveDeltaSteps(otfAxis_2 - 1, deltaSteps_2, velFactor_2);       // OTF-Achse Eins mit relativer Reduzierung der Fahrgeschwindigkeit in Bewegung setzen
                                    }

                                    answer += QString("OtF StartScan:%1 %2 y\r\n").arg(otfAxis_1).arg(otfAxis_2);
                                }

            }

        }

        if(cmd.startsWith(otf_getnmin))    // minimale Anzahl Sekunden für On-the-Fly abfragen
        {
            validCmd = true;
            cmd = cmd.right(cmd.size() - otf_getnmin.size());      // fixen Befehlsteil abschneiden
            QStringList items = cmd.split(" ");
            if(items.count()!=2)                                // Immer zwei Items, Achsnummer und Delta-Units
                answer += QString("OtF GetNmin:n - Wrong Number of Arguments\r\n");
            else
            {
                int otfAxis = items[0].toInt();                         // hier Nummer der OtF-Achse
                double deltaUnits = fabs(myLocale.toDouble(items[1]));  // gewünschter Fahrweg
                int secondsMin = deltaUnits *  myAxes.Axis[otfAxis - 1].steps_per_unit / myAxes.Axis[otfAxis - 1].velocity;
                if(secondsMin == 0) secondsMin = 1;                     // keinen Unfug zurückmelden
                answer += QString("OtF GetNmin:%1 %2\r\n").arg(otfAxis).arg(secondsMin);
            }
        }

        if(cmd.startsWith(otf_getlastscan))   // die Daten der letzten Schusssequenz senden, dies setzt implizit voraus, daß vorher writeOtfData() aufgerufen wurde
        {
            validCmd = true;
            if(answer != "")
            {
                clientConnection->write(answer.toLatin1());     // Puffer schreiben = flush
            }
            answer = "SOF";     // kein += löscht den answer Puffer!

            // Die Kopfzeile der Tabelle
            for(int axis=0; axis < myAxes.numAxis; ++axis)
            {
                if( myAxes.Axis[axis].numOTF > 0)
                    answer += QString(" %1=\"%2\"").arg(axis+1).arg(myAxesDialog.axisName[axis]);
            }
            answer += "\r\n";

            // Die Daten-Zeilen der Tabelle
            for(int j=0; j < myAxes.numOTFmax; ++j)
            {
                answer += QString("%1").arg(j+1);
                for(int axis=0; axis < myAxes.numAxis; ++axis)
                {
                    if(myAxes.Axis[axis].numOTF > 0)
                        answer += QString(" %1").arg(myAxes.Axis[axis].positionUnitsOTF[j], 0, 'f', 8);
                }
                answer += "\r\n";
            }

            answer += "EOF\r\n";
            clientConnection->write(answer.toLatin1());     // hier sofort schreiben, verhindert dies connection loss?
            answer = "";                                    // geschriebenen Puffer löschen
        }


        if(cmd.startsWith(restartecat))         // EtherCAT neu initialisieren
        {
            validCmd = true;
            answer += "Ecat Restart:y\r\n";

            ui->lineEdit_Status->setText("Restarting...");
            ui->lineEdit_Status->repaint();
            Restart_Ecat(myNetwork.ecName.toLatin1().data());   // EtherCAT neu starten
            InitConfigMainDialog();
            sprintf(cstring, "%d", myAxes.Axis[myAxes.currentAxis].positionStepsTarget);    // wird in Startup_Ecat in Restart_Ecat() auf 0 gesetzt
            ui->lineEdit_Steps_Aim->setText(cstring);
        }

        // der Schleppfehler kann nur manuell rückgesetzt werden
        if(cmd.startsWith(clearfollowingerror))         // Schleppfehler löschen
        {
            validCmd = true;
            if(myAxes.statusMotionGlobal == GSTATUS_FOLLOWING_ERROR)
            {
                myAxes.statusMotionGlobal = 0;
                answer += "Clear FollowingError:y\r\n";
            }
            else
                answer += "Clear FollowingError:n - Nothing to clear\r\n";
        }

        if(cmd.startsWith(loggingstart))         // Das Logging der Encoder Counts beginnen
        {
            validCmd = true;
            answer += "Logging Start:y\r\n";

            // Die Logfiles für die Encoder-Counts initialisieren:
            QTime   myTime = QTime::currentTime();
            QString path = QDir::homePath() + QString("/Data_Motion/EncoderCounts_%1-%2-%3.dat").arg(myTime.hour(), 2, 10, QChar('0')).arg(myTime.minute(), 2, 10, QChar('0')).arg(myTime.second(), 2, 10, QChar('0'));
            encoderlogfile.setFileName(path);
            if (encoderlogfile.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream out(&encoderlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

                out << "Logging started on " << QDateTime::currentDateTime().toString() << "\n";    // keine Leerzeile, ist besser für Igor
                out << "Time (sec since 1970)  ";
                for(int i=0; i < myEncoderChannels.numEncoder; ++i)
                {
                    if(myEncoderChannels.Encoder[i].doLogging)
                        out << i+1 << ": " << myEncoderDialog.encName[i] << "   ";
                }
                out << "\n";
                encoderlogfile.close();
            }

            path = QDir::homePath() + QString("/Data_Motion/QuickLog.csv");
            quicklogfile.setFileName(path);
            if (quicklogfile.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                quicklogfile.close();
            }
            quick.index = 0;
            quick.first = true;

            logCounter = ecatState.msCounter + 100;
            ui->pushButton_StartLogging->setText("Stop Logging");
            flags.doLoggingEncoder = true;
        }

        if(cmd.startsWith(loggingstop))         // Das Logging der Encoder Counts beenden
        {
            validCmd = true;
            answer += "Logging Stop:y\r\n";

            flags.doLoggingEncoder = false;
            ui->pushButton_StartLogging->setText("Start Logging");

            if(encoderlogfile.open(QIODevice::Append | QIODevice::Text))
            {
                QTextStream out(&encoderlogfile);
                out << "\nLogging stopped on " << QDateTime::currentDateTime().toString() << "\n";
                encoderlogfile.close();
            }
        }

        if(cmd.startsWith(process_start))         // Das Logging der Encoder Counts beginnen
        {
            validCmd = true;
            start_Process();
            answer += "Process Start:y\r\n";
        }

        if(cmd.startsWith(process_stop))         // Das Logging der Encoder Counts beginnen
        {
            validCmd = true;
            stop_Process();
            answer += "Process Stop:y\r\n";
        }

        if(cmd.startsWith(getquads))            // Die Quadranten abfragen
        {
            validCmd = true;
            answer += QString("GetQuads:%L1 %L2 %L3 %L4 ").arg(sourceControl.counts_dQ1, 0, 'f', 1).arg(sourceControl.counts_dQ2, 0, 'f', 1).arg(sourceControl.counts_dQ3, 0, 'f', 1).arg(sourceControl.counts_dQ4, 0, 'f', 1);
            answer += QString("%L1 %L2\r\n").arg(sourceControl.QX, 0, 'f', 3).arg(sourceControl.QY, 0, 'f', 3);
        }

        if(cmd.startsWith(help))
        {
            validCmd = true;
            answer += "\nAxis GetStatus:Axis-1\r\n";
            answer += "Axis Stop:Axis-1\r\n";
            answer += "Axis GetParams:Axis-1\r\n";
            answer += "Axis GetPosE:1\r\n";
            answer += "Sens Restart\r\n";
            answer += "Dio GetBit:1\r\n";
            answer += "Dio SetBit:1 0\r\n";
            answer += "Ecat Restart\r\n";
            answer += "Clear FollowingError\r\n";
            answer += "Logging Start\r\n";
            answer += "Logging Stop\r\n";
            answer += "Process Start\r\n";
            answer += "Process Stop\r\n";
            answer += "GetQuads\r\n";

            answer += "Help\r\n\r\n";
        }

        if(!validCmd)
        {
            answer += "Unknown Command:" + cmd.toLatin1() + "\r\n";
        }


    }   // end of while
    clientConnection->write(answer.toLatin1()); // die gesammelten Antworten für das ganze Befehlspaket
    //debug += answer;
}


void ecMainDialog::keyPressEvent(QKeyEvent* event)
{
    switch(  event->key() )
    {
    case Qt::Key_S:
        on_pushButton_Stop_clicked();   // sofortiger Stop
        break;
    default:
        break;
    }
}

//
//      Alle Überschreibungen von reject, diese regeln das Verhalten beim Schließen des jeweiligen Dialogs:
//
void ecMainDialog::reject()
{
    QMessageBox::StandardButton reply = QMessageBox::question(this, "Quit", "Do you want to quit ecTool?",  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        Stop_Ecat();

        fprintf(stderr, "closing Ethernet socket\n");
        if(myNetwork.enConnected)   // ist das so richtig ???
        {
            myServer->close();      // Annahme von Verbindungsversuchen stoppen
            clientConnection->disconnectFromHost();     // eventuell noch bestehende Client Verbindung beenden
        }

        fprintf(stderr, "...Ending ecTool...\n");

        if(protofile.open(QIODevice::Append | QIODevice::Text))
        {
            QTextStream out(&protofile);
            out << "\necTool stopped on " << QDateTime::currentDateTime().toString() << "\n";
            protofile.close();
        }

        if(mlogfile.open(QIODevice::Append | QIODevice::Text))
        {
            QTextStream out(&mlogfile);
            out << "\necTool stopped on " << QDateTime::currentDateTime().toString() << "\n";
            mlogfile.close();
        }
        // hier wird das Programm tatsächlich beendet:
        QDialog::reject();      // den MainDialog schließen,
        QApplication::quit();   // und alle anderen eventuell noch offenen Fenster
    }
}


void ecMainDialog::on_pushButton_OpenAxisConfiguration_clicked()
{
    if(!myDialogs.isOpen_Axis)
    {
        axisDialog->setWindowFlags(Qt::Dialog);
        axisDialog->open();     // nonblocking, Dialog läuft parellel zum Hauptfenster
        axisDialog->setWindowTitle("Axis");
        axisDialog->move(this->x() +60, this->y() +80);
        axisDialog->InitContent();
        myDialogs.isOpen_Axis = true;
        //axisDialog->activateWindow();
    }
    else
        axisDialog->activateWindow();

}

void ecMainDialog::on_pushButton_OpenEncoderConfiguration_clicked()
{
    if(!myDialogs.isOpen_Encoder)    // doppelt geöffnete Fenster verhindern
    {
        encoderDialog->setWindowFlags(Qt::Dialog);
        encoderDialog->open();
        encoderDialog->setWindowTitle("Encoder");
        encoderDialog->move(this->x() +30, this->y() +50);
        encoderDialog->InitContent();
        myDialogs.isOpen_Encoder = true;
        //usleep(100000); // etwas Zeit (0,1 s) für den Aufbau des Fensters lassen, damit axisDialog auch wirklich nach vorn kommt
    }
    else
        encoderDialog->activateWindow();

}



void ecMainDialog::on_checkBox_GlobalStab_clicked()
{
    myAxes.isStabilizedGlobal = ui->checkBox_GlobalStab->isChecked();
}


void ecMainDialog::on_pushButton_viewConfiguration_clicked()
{
    if(!myDialogs.isOpen_Configuration)    // doppelt geöffnete Fenster verhindern
    {
        configurationDialog->open();
        configurationDialog->setWindowTitle("Configuration");
        configurationDialog->InitContent();
        myDialogs.isOpen_Configuration = true;
    }
    else
    {
        configurationDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
    }
}


// der Schleppfehler kann nur manuell rückgesetzt werden
void ecMainDialog::on_pushButton_FollowingError_Reset_clicked()
{
    if(myAxes.statusMotionGlobal == GSTATUS_FOLLOWING_ERROR)
        myAxes.statusMotionGlobal = 0;
}





void ecMainDialog::on_pushButton_Init_clicked()
{
    int axis = myAxes.currentAxis;
    QMessageBox::StandardButton reply;

    // Achsen mit Resolute (macht aber wenig Sinn), Tonic (notwendig) und ohne Encoder aber mit vernünftigen Endlagenschaltern können initialisiert werden:
    if(myAxes.Axis[axis].axisType == AXISTYPE_NORMAL)
    {
        if( myAxes.Axis[axis].initDirection == -1)
            reply = QMessageBox::question(this, "Init", QString("Do you want to Initialize Axis %1 %2 and drive it into Reverse Limit?").arg(axis+1).arg(myAxesDialog.axisName[axis]), QMessageBox::Yes|QMessageBox::No);
        else
            reply = QMessageBox::question(this, "Init", QString("Do you want to Initialize Axis %1 %2 and drive it into Forward Limit?").arg(axis+1).arg(myAxesDialog.axisName[axis]), QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes)
        {
            if(myAxes.Axis[axis].statusMove == 0) // aktuelle Achse fährt nicht
            {
                if(myAxes.Axis[axis].indexEncoder_A > 0)
                    myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_A-1].referencing = 1;    // Zero on Mark aktivieren
                if(myAxes.Axis[axis].indexEncoder_B > 0)
                    myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_B-1].referencing = 1;    // Zero on Mark aktivieren

                myAxes.Axis[axis].isIniting = 3;    // Flag für Initialisierung der Achse setzen
                MoveDeltaSteps(axis, FUENFMILLE * myAxes.Axis[axis].initDirection , 1.0);    //  500 Millionen Steps sollten immer reichen um in den Reverse Limt zu fahren
            }
        }
    }
    else
    {
        QMessageBox::warning(this, "Init", "Error: Only Axis of Type 2 can be initialised!", QMessageBox::Ok);
    }
}

/*
void ecMainDialog::on_pushButton_Init_All_clicked()
{

}
*/





void ecMainDialog::on_pushButton_StartLogging_clicked()
{
    if(flags.doLoggingEncoder)
    {
        flags.doLoggingEncoder = false;
        ui->pushButton_StartLogging->setText("Start Logging");

        if(encoderlogfile.open(QIODevice::Append | QIODevice::Text))
        {
            QTextStream out(&encoderlogfile);
            out << "\nLogging stopped on " << QDateTime::currentDateTime().toString() << "\n";
            encoderlogfile.close();
        }
    }
    else
    {
        // Die Logfiles für die Encoder-Counts initialisieren:
        QTime   myTime = QTime::currentTime();
        QString path = QDir::homePath() + QString("/Data_Motion/EncoderCounts_%1-%2-%3.dat").arg(myTime.hour(), 2, 10, QChar('0')).arg(myTime.minute(), 2, 10, QChar('0')).arg(myTime.second(), 2, 10, QChar('0'));
        encoderlogfile.setFileName(path);
        if (encoderlogfile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            QTextStream out(&encoderlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

            out << "Logging started on " << QDateTime::currentDateTime().toString() << "\n";    // keine Leerzeile, ist besser für Igor
            out << "Time (sec since 1970)  ";
            for(int i=0; i < myEncoderChannels.numEncoder; ++i)
            {
                if(myEncoderChannels.Encoder[i].doLogging)
                    out << i+1 << ": " << myEncoderDialog.encName[i] << "   ";
            }
            out << "\n";
            encoderlogfile.close();
        }

        path = QDir::homePath() + QString("/Data_Motion/QuickLog.csv");
        quicklogfile.setFileName(path);
        if (quicklogfile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            quicklogfile.close();
        }
        quick.index = 0;
        quick.first = true;

        logCounter = ecatState.msCounter + 100;
        ui->pushButton_StartLogging->setText("Stop Logging");
        //ui->pushButton_StartLogging->setDown(true); dies hilft nicht viel, der Button muss im .ui als checkable gesetzt werden
        flags.doLoggingEncoder = true;
    }

}





/*
void hexaMoveToUnits(int axis, double units, double relVel)
{
    ecMainDialog::MoveToUnits(axis, units, relVel);
}
*/


//
//
//              Hier sind alle GUI-Funktionen für den Move Sequence Dialog
//              Ist wegen der engen Verzahnung mit myTimerTask in maindialog.cpp untergebracht
//
//  Autor:      Christian Grundel
//
//  28.07.2020  Neues Modul angelegt
//  29.07.2020  Nach maindialog.cpp verpflanzt
//


ecMoveSequenceDialog::ecMoveSequenceDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecMoveSequenceDialog)
{
    ui->setupUi(this);
    currentRow = 0;     // der erste Eintrag
}

void ecMoveSequenceDialog::reject()
{
    myDialogs.isOpen_MoveSequence = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}


// Initialisiert den Dialog
void ecMoveSequenceDialog::InitContent(bool useHexa)
{
    ui->lineEdit_StartPoint->setText(QString("%L1").arg(moveSequence.startPoint, 11, 'f', 6));
    ui->lineEdit_EndPoint->setText(QString("%L1").arg(moveSequence.endPoint, 11, 'f', 6));
    ui->lineEdit_SubDivisions->setText(QString("%1").arg(moveSequence.subDivisions));
    ui->lineEdit_Iterations->setText(QString("%1").arg(moveSequence.iterations));
    ui->lineEdit_RestTime->setText(QString("%1").arg(moveSequence.timePerStep));
    //ui->lineEdit_MoveSequenceAxis->setText(QString("%1").arg(moveSequence.axis + 1));
    ui->lineEdit_WaitTime->setText(QString("%1").arg(moveSequence.waitTime));
    ui->label_CountDown->setText(QString("%1").arg(moveSequence.iterationCounter));
    ui->label_WaitTimeLeft->setText(QString("%1").arg(moveSequence.waitTimeCounter));

    ui->listWidgetAoD->clear();
    for(int i=0; i < myAxes.numAxis; ++i)
    {
        ui->listWidgetAoD->addItem(QString("Axis-%1").arg(i+1));
    }
    if(useHexa)
    {
        ui->listWidgetAoD->addItem("Dof-X");
        ui->listWidgetAoD->addItem("Dof-Y");
        ui->listWidgetAoD->addItem("Dof-Z");
        ui->listWidgetAoD->addItem("Dof-Yaw");
        ui->listWidgetAoD->addItem("Dof-Pitch");
        ui->listWidgetAoD->addItem("Dof-Roll");
    }
    ui->listWidgetAoD->setCurrentRow(currentRow);
}

// Aktualisiert den Dialog
void ecMoveSequenceDialog::UpdateContent()
{
    ui->label_CountDown->setText(QString("%1").arg(moveSequence.iterationCounter));
    // aus zehntel Sekunden Minuten und ganze Sekunden machen:
    ui->label_WaitTimeLeft->setText(QString("%1:%2").arg(moveSequence.waitTimeCounter / 600).arg((moveSequence.waitTimeCounter % 600) / 10, 2, 10, QChar('0')));
    if(moveSequence.iterationCounter == 0)
    {
        ui->pushButton_moveSequenceStart->setText("Start");
        ui->pushButton_moveSequenceStart->setChecked(false);
    }
}

void ecMoveSequenceDialog::on_listWidgetAoD_itemClicked(QListWidgetItem *item)
{
    currentRow = ui->listWidgetAoD->currentRow();
    moveSequence.axis = currentRow;
}


void ecMoveSequenceDialog::on_lineEdit_StartPoint_editingFinished()
{
    moveSequence.startPoint = myLocale.toDouble(ui->lineEdit_StartPoint->text());
    ui->lineEdit_StartPoint->setText(QString("%L1").arg(moveSequence.startPoint, 11, 'f', 6));
}

void ecMoveSequenceDialog::on_lineEdit_EndPoint_editingFinished()
{
    moveSequence.endPoint = myLocale.toDouble(ui->lineEdit_EndPoint->text());
    ui->lineEdit_EndPoint->setText(QString("%L1").arg(moveSequence.endPoint, 11, 'f', 6));
}

void ecMoveSequenceDialog::on_lineEdit_SubDivisions_editingFinished()
{
    moveSequence.subDivisions = ui->lineEdit_SubDivisions->text().toInt();
    ui->lineEdit_SubDivisions->setText(QString::number(moveSequence.subDivisions));
}

void ecMoveSequenceDialog::on_lineEdit_Iterations_editingFinished()
{
    moveSequence.iterations = ui->lineEdit_Iterations->text().toInt();
    ui->lineEdit_Iterations->setText(QString::number(moveSequence.iterations));
}

void ecMoveSequenceDialog::on_lineEdit_RestTime_editingFinished()
{
    moveSequence.timePerStep = ui->lineEdit_RestTime->text().toInt();
    ui->lineEdit_RestTime->setText(QString::number(moveSequence.timePerStep));
}

/*
void ecMoveSequenceDialog::on_lineEdit_MoveSequenceAxis_editingFinished()
{
    moveSequence.axis = ui->lineEdit_MoveSequenceAxis->text().toInt() - 1;
    ui->lineEdit_MoveSequenceAxis->setText(QString::number(moveSequence.axis + 1));
}
*/


void ecMoveSequenceDialog::on_lineEdit_WaitTime_editingFinished()
{
    moveSequence.waitTime = ui->lineEdit_WaitTime->text().toInt();
    ui->lineEdit_WaitTime->setText(QString::number(moveSequence.waitTime));
}


void ecMoveSequenceDialog::on_pushButton_moveSequenceStart_clicked()
{

    if(moveSequence.iterationCounter == 0)
    {
        moveSequence.firstMove = true;  // einmal sofort den ersten Startpunkt anfahren
        moveSequence.reverse = false;
        //moveSequence.nextTime = ecatState.msCounter / 1000;     // ?? muss für WaitTime anpepasst werden, nach myQTimer verschoben
        if(moveSequence.waitTime > 0)
            moveSequence.waitTimeCounter = 600 * moveSequence.waitTime;     // ist in zehntel Sekunden
        else
            moveSequence.waitTimeCounter = 20;                              // Mindestwartezeit zwei Sekunden, billiger Trick, damit moveSequence.nextTime immer korrekt initialisiert wird
        moveSequence.iterationCounter = moveSequence.iterations;
        moveSequence.subdivisionCounter = 1;
        ui->pushButton_moveSequenceStart->setText("Stop");
        ui->label_CountDown->setText(QString::number(moveSequence.iterationCounter));
    }
    else
    {
        moveSequence.iterationCounter = 0;  // Bewegungssequenz abbrechen
        ui->pushButton_moveSequenceStart->setText("Start");
        ui->label_CountDown->setText(QString::number(moveSequence.iterationCounter));
    }
}



//
//  readwriteconfig     Die Routinen zum Lesen und Schreiben der Parameter von ecTool in Config-Datei
//
//              Hier sind alle Funktionen versammelt, die auf die Festplatte zugreifen
//
//  Autor:      Christian Grundel
//
//  04.07.2020  Neu als Modul
//  11.08.2020  readConfiguration aufgeräumt, Parameter doLogging ergänzt
//



// wenn das Item existiert, wird der Int Wert zugewiesen, sonst false
void myItemsToDouble(bool *doesNotExist, double *value, QStringList items, int index)
{
    if(items.size() > index)
        *value = myLocale.toDouble(items[index]);   // Item erfolgreich gelesen
    else
        *doesNotExist = true;   // Item existiert nicht, value unverändert lassen
}

// wenn das Item existiert, wird der bool Wert zugewiesen und true, sonst false
void myItemsToInt(bool *doesNotExist, int *value, QStringList items, int index)
{
    if(items.size() > index)
        *value = items[index].toInt();
    else
        *doesNotExist = true;   // Item existiert nicht, value unverändert lassen
}

// wenn das Item existiert, wird der qint64 Wert zugewiesen und true, sonst false
void myItemsToQint64(bool *doesNotExist, qint64 *value, QStringList items, int index)
{
    if(items.size() > index)
        *value = items[index].toLongLong();
    else
        *doesNotExist = true;   // Item existiert nicht, value unverändert lassen
}

void myItemsToBool(bool *doesNotExist, bool *value, QStringList items, int index)
{
    if(items.size() > index)
        *value = items[index].toInt();
    else
        *doesNotExist = true;   // Item existiert nicht
}

// Index zur eigentlichen Konfiguration
void writeDefaultConfig(QFile* myFile)
{
    QTextStream out(myFile);

    out << "Default Configuration Filename for ecTool\n";
    out << "Saved on " << QDateTime::currentDateTime().toString() << "\n";
    out << "\n";
    out << "[PATH OF DEFAULT CONFIGURATION]\n";
    out << "ConfigurationFileName \"" << myAxesDialog.defaultFileName << "\"\n";
    out << "\n[END_OF_FILE]\n";
}

void readDefaultConfig(QFile* myFile)
{
    QTextStream in(myFile);

    // die Zeile mit dem Pfadnamen suchen
    while (!in.atEnd()) {
        QString line = in.readLine();
        line = line.simplified();
        QStringList items = line.split(" ");
        int num_items = items.size();

        if(items[0] == "ConfigurationFileName" && num_items >= 2)
            myAxesDialog.defaultFileName = line.mid(22).remove("\"");  // schmutziger Trick
    }
}

// Die Konfiguration selbst
void writeConfiguration(QFile* myFile)
{
    QTextStream out(myFile);
    int i;

    out << "Configuration File for ecTool\n";
    out << "Saved on " << QDateTime::currentDateTime().toString() << "\n";
    out << "\n";
    out << "[COMMENT]\n";
    out << "Text \"" << myAxesDialog.comment << "\"\n";
    out << "\n";
    out << "[NETWORK]\n";
    out << "Ethernet \"" << myNetwork.enName << "\"\n"; // die Namen jetzt in Anführungszeichen
    out << "Port \"" << myNetwork.enPort << "\"\n";
    out << "ClientIP \"" << myNetwork.enIP << "\"\n";     // leer = wird nicht benutzt
    out << "EtherCAT \"" << myNetwork.ecName << "\"\n";     // leer = wird nicht benutzt
    out << "LoopBack " << myNetwork.isLocalLoopBack << "\n";

    out << "\n";
    out << "[ETHERCAT_SLAVES Num Indices]\n";
    if(configuredSlaves.simulateECM)
        out << "Simulation 1\n";    // Simulation der ECM an
    else
        out << "Simulation 0\n";    // Simulation der ECM aus
    out << "ECM701 " << foundSlaves.ECM701count;
    for(i = 0; i < foundSlaves.ECM701count; ++i)  out << " " << foundSlaves.ECM701index[i];
    out << "\nECM702 " << foundSlaves.ECM702count;
    for(i = 0; i < foundSlaves.ECM702count; ++i)  out << " " << foundSlaves.ECM702index[i];
    out << "\nECM712 " << foundSlaves.ECM712count;
    for(i = 0; i < foundSlaves.ECM712count; ++i)  out << " " << foundSlaves.ECM712index[i];
    out << "\nECM713 " << foundSlaves.ECM713count;
    for(i = 0; i < foundSlaves.ECM713count; ++i)  out << " " << foundSlaves.ECM713index[i];
    out << "\nA760-01-B " << foundSlaves.A760_01_B_count;
    for(i = 0; i < foundSlaves.A760_01_B_count; ++i)  out << " " << foundSlaves.A760_01_B_index[i];
    out << "\nEL1024 " << foundSlaves.EL1024count;
    for(i = 0; i < foundSlaves.EL1024count; ++i)  out << " " << foundSlaves.EL1024index[i];
    out << "\nEL1104 " << foundSlaves.EL1104count;
    for(i = 0; i < foundSlaves.EL1104count; ++i)  out << " " << foundSlaves.EL1104index[i];
    out << "\nEL2202 " << foundSlaves.EL2202count;
    for(i = 0; i < foundSlaves.EL2202count; ++i)  out << " " << foundSlaves.EL2202index[i];
    out << "\nEL2612 " << foundSlaves.EL2612count;
    for(i = 0; i < foundSlaves.EL2612count; ++i)  out << " " << foundSlaves.EL2612index[i];
    out << "\nEL2622 " << foundSlaves.EL2622count;
    for(i = 0; i < foundSlaves.EL2622count; ++i)  out << " " << foundSlaves.EL2622index[i];

    out << "\nOTHER  " << foundSlaves.OTHERcount << "\n";
    out << "\n";

    out << "[AXIS_CONFIGURATION Num Name EncA EncB UnitT FBT StepsPerUnit Vel Acc Brake CL_App CL_DeltaM CL_WaitM CL_DeltaStab CL_Average isGantry AxisType isStab CL_WaitStab CL_DeltaF jogSteps jogPosition motorDirection initSteps isUsed]\n";
    //out << "Count " << myAxisList.numAxis << "\n";

    // myLocale immer für englischen Dezimalpunkt
    for(i=0; i<myAxes.numAxis; i++)     // die aktuell tatsächlichen Achsen speichern
    {
        out << "AXIS " << i+1 <<" \""<< myAxesDialog.axisName[i] << "\" "<< myAxes.Axis[i].indexEncoder_A <<" "<< myAxes.Axis[i].indexEncoder_B
            <<" "<<  myAxes.Axis[i].unitType <<" "<<  myAxes.Axis[i].feedbackType <<" "<<  myLocale.toString(myAxes.Axis[i].steps_per_unit)
            <<" "<<  myAxes.Axis[i].velocity <<" "<<  myAxes.Axis[i].acceleration <<" "<<  myAxes.Axis[i].hasBrake
            <<" "<< myLocale.toString(myAxes.Axis[i].CL_Approach) <<" "<< myLocale.toString(myAxes.Axis[i].CL_DeltaMove) <<" "<< myAxes.Axis[i].CL_WaitMove
            <<" "<< myLocale.toString(myAxes.Axis[i].CL_DeltaStab) <<" "<< myAxes.Axis[i].CL_Average  <<" "<< myAxes.Axis[i].isGantry
            <<" "<< myAxes.Axis[i].axisType <<" "<< myAxes.Axis[i].isStabilized <<" "<< myAxes.Axis[i].CL_WaitStab <<" "<< myLocale.toString(myAxes.Axis[i].CL_DeltaFollow)
            <<" "<< myAxes.Axis[i].initDirection <<" "<< myAxes.Axis[i].microSteps <<" "<< myAxes.Axis[i].motorCurrentRun <<" "<< myAxes.Axis[i].motorCurrentStop
            <<" "<< myAxes.Axis[i].jogDeltaSteps <<" "<< myLocale.toString(myAxes.Axis[i].jogDeltaPosition) <<" "<< myAxes.Axis[i].motor_direction <<" "<< myAxes.Axis[i].initSteps
            <<" "<< myAxes.Axis[i].isUsed
            << "\n";
    }
    out << "NoSimulatedAxes " << myAxes.numSimulatedAxes << "\n";


    out << "\n[COLLISION_CONTROL Num sensAxis auxSelect targetAxis targetAxisRoF]\n";
    for(i=0; i < collisionControl.numRules; i++)     // die aktuell tatsächlichen Achsen speichern
    {
        out << "CC_Rule " << i+1 <<" "<< collisionControl.sensAxisIndex[i] <<" "<< collisionControl.auxSelect[i] <<" "<< collisionControl.targetAxisIndex[i] <<" "<< collisionControl.targetAxisRoF[i] << "\n";
    }

    out << "\n[ENCODER_CONFIGURATION Num Name UPC Offset limitLow limitHigh Type OffsetUnits Logging isUsed]\n";
    for(i=0; i<myEncoderChannels.numEncoder; i++)
    {
        out << "ENC " << (i+1) <<" \""<< myEncoderDialog.encName[i] <<"\" "<< myLocale.toString(myEncoderChannels.Encoder[i].units_per_count)
            <<" "<< myEncoderChannels.Encoder[i].offsetCounts  <<" "<< myEncoderChannels.Encoder[i].limitLow  <<" "<< myEncoderChannels.Encoder[i].limitHigh  <<" "<< myEncoderChannels.Encoder[i].encoderModus
            <<" "<< myLocale.toString(myEncoderChannels.Encoder[i].offsetUnits) <<" "<< myEncoderChannels.Encoder[i].doLogging <<" "<< myEncoderChannels.Encoder[i].isUsed <<"\n";
    }

    out << "\n[MOTOR_DRIVER_CONFIGURATION]\n";
    //out << "Motor_Ctrl "    << motorDriverConfig.motor_Ctrl << "\n";
    out << "Motor_tOff "    << motorDriverConfig.motor_tOff << "\n";
    out << "Motor_tBlank "  << motorDriverConfig.motor_tBlank << "\n";
    out << "Motor_tDecay "  << motorDriverConfig.motor_tDecay << "\n";


    out << "\n[EXTENSIONS]\n";
    out << "extend_Hexapod "        << myDialogs.extOpen_Hexapod << "\n";
    out << "extend_IO_Beckhoff "    << myDialogs.extOpen_IO_Beckhoff << "\n";
    out << "extend_MoveSequence "   << myDialogs.extOpen_MoveSequence << "\n";
    out << "extend_OTF_Tonic "      << myDialogs.extOpen_OTF_Tonic << "\n";
    out << "extend_SourceControl "  << myDialogs.extOpen_SourceControl << "\n";

    out << "\n[OTF]\n";
    out << "OTF_AxisNr "            << myAxes.otfAxisNr << "\n";


    out << "\n[HEXAPOD]";
    out << "\n";
    out << "LegBase_H1 " << myLocale.toString(hexa.LegBaseLocal_x[0], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[0], 'f', 6) << "\n";
    out << "LegBase_H2 " << myLocale.toString(hexa.LegBaseLocal_x[1], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[1], 'f', 6) << "\n";
    out << "LegBase_H3 " << myLocale.toString(hexa.LegBaseLocal_x[2], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[2], 'f', 6) << "\n";
    out << "LegBase_H4 " << myLocale.toString(hexa.LegBaseLocal_x[3], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[3], 'f', 6) << "\n";
    out << "LegBase_H5 " << myLocale.toString(hexa.LegBaseLocal_x[4], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[4], 'f', 6) << "\n";
    out << "LegBase_H6 " << myLocale.toString(hexa.LegBaseLocal_x[5], 'f', 6) << " " << myLocale.toString(hexa.LegBaseLocal_y[5], 'f', 6) << "\n";
    out << "\n";
    out << "LegUpper_H1 " << myLocale.toString(hexa.LegUpperLocal_x[0], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[0], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[0], 'f', 6) << "\n";
    out << "LegUpper_H2 " << myLocale.toString(hexa.LegUpperLocal_x[1], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[1], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[1], 'f', 6) << "\n";
    out << "LegUpper_H3 " << myLocale.toString(hexa.LegUpperLocal_x[2], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[2], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[2], 'f', 6) << "\n";
    out << "LegUpper_H4 " << myLocale.toString(hexa.LegUpperLocal_x[3], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[3], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[3], 'f', 6) << "\n";
    out << "LegUpper_H5 " << myLocale.toString(hexa.LegUpperLocal_x[4], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[4], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[4], 'f', 6) << "\n";
    out << "LegUpper_H6 " << myLocale.toString(hexa.LegUpperLocal_x[5], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_y[5], 'f', 6) << " " << myLocale.toString(hexa.LegUpperLocal_z[5], 'f', 6) << "\n";
    out << "\n";
    out << "LegLenght_H1 " << myLocale.toString(hexa.LegLenght[0], 'f', 6) << "\n";
    out << "LegLenght_H2 " << myLocale.toString(hexa.LegLenght[1], 'f', 6) << "\n";
    out << "LegLenght_H3 " << myLocale.toString(hexa.LegLenght[2], 'f', 6) << "\n";
    out << "LegLenght_H4 " << myLocale.toString(hexa.LegLenght[3], 'f', 6) << "\n";
    out << "LegLenght_H5 " << myLocale.toString(hexa.LegLenght[4], 'f', 6) << "\n";
    out << "LegLenght_H6 " << myLocale.toString(hexa.LegLenght[5], 'f', 6) << "\n";
    out << "\n";
    out << "HexaOffset_Z0 " << myLocale.toString(hexa.Offset_Z0, 'f', 6) << "\n";
    out << "HexaDelta_ZPP " << myLocale.toString(hexa.Delta_ZPP, 'f', 6) << "\n";
    out << "\n";

    out << "HexaAxis_H1 " << myLocale.toString(hexa.Axis_Number[0]) << "\n";
    out << "HexaAxis_H2 " << myLocale.toString(hexa.Axis_Number[1]) << "\n";
    out << "HexaAxis_H3 " << myLocale.toString(hexa.Axis_Number[2]) << "\n";
    out << "HexaAxis_H4 " << myLocale.toString(hexa.Axis_Number[3]) << "\n";
    out << "HexaAxis_H5 " << myLocale.toString(hexa.Axis_Number[4]) << "\n";
    out << "HexaAxis_H6 " << myLocale.toString(hexa.Axis_Number[5]) << "\n";


    out << "\n[DEBUG]\n";
    out << "DEBUG_STEPS " << flagsDebug.steps <<"\n";
    out << "DEBUG_ACCELERATION " << flagsDebug.acc <<"\n";
    out << "DEBUG_RESIDUAL " << flagsDebug.res <<"\n";
    out << "DEBUG_ENCODER " << flagsDebug.enc <<"\n";
    out << "DEBUG_ENCSTATE " << flagsDebug.encStatus <<"\n";
    if(flagsDebug.decimal == comma)
        out << "DEBUG_DECIMALSEPARATOR \",\"\n";
    else
        out << "DEBUG_DECIMALSEPARATOR \".\"\n";

    out << "\n[TARGET]\n";
    out << "LASERPULSERATE " << sourceControl.laserPulseRate << "\n";
    out << "PULSE_DELAY " << sourceControl.A760_TriggerDelay << "\n";

    out << "AXIS_TR " << sourceControl.TR_AxisNo << "\n";
    out << "AXIS_TT " << sourceControl.TT_AxisNo << "\n";
    out << "AXIS_TC " << sourceControl.TC_AxisNo << "\n";

    out << "T_TRANS_INC " << myLocale.toString(sourceControl.increment_trans) << "\n";
    out << "TT_RANGE_LOW " << myLocale.toString(sourceControl.TT_Range_low) << "\n";
    out << "TT_RANGE_HIGH " << myLocale.toString(sourceControl.TT_Range_high) << "\n";

    out << "TC_ACTIVE " << sourceControl.TC_Active << "\n";
    out << "TC_A_QUADS " << sourceControl.A_Quad_1 << " " << sourceControl.A_Quad_2 << "\n";
    out << "TC_B_QUADS " << sourceControl.B_Quad_1 << " " << sourceControl.B_Quad_2 << "\n";
    out << "TC_LIMIT " << myLocale.toString(sourceControl.TC_Limit) << "\n";
    out << "TC_DELTASTEPS " << sourceControl.TC_delta_steps << "\n";
    out << "TC_AVERAGE " << sourceControl.numAverage << "\n";

    out << "TC_OFFSET_Q1 " << sourceControl.offset_Q1 << "\n";
    out << "TC_OFFSET_Q2 " << sourceControl.offset_Q2 << "\n";
    out << "TC_OFFSET_Q3 " << sourceControl.offset_Q3 << "\n";
    out << "TC_OFFSET_Q4 " << sourceControl.offset_Q4 << "\n";

    out << "\n[END_OF_FILE]\n";
}

void readConfiguration(QFile* myFile)    // Konfiguration aus Datei lesen und in die globalen Variablen schreiben
{
    int i, countAxis = 0, countEnc = 0, countCCRules = 0;
    bool toShort;
    QTextStream in(myFile);

    toShort = false;        // Summenfehler für das Einlesen aller Items

    while (!in.atEnd()) {
        QString line = in.readLine();
        line = line.simplified();
        QStringList items = line.split(" ");
        int num_items = items.size();

        if(items[0] == "Text" && num_items >= 2)
            myAxesDialog.comment = line.mid(6).remove("\"");  // schmutziger Trick

        if(items[0] == "Ethernet" && num_items >= 2)
            myNetwork.enName = items[1].remove("\"");

        if(items[0] == "Port" && num_items >= 2)
            myNetwork.enPort = items[1].remove("\"");

        if(items[0] == "ClientIP" && num_items >= 2)
            myNetwork.enIP = items[1].remove("\"");

        if(items[0] == "EtherCAT" && num_items >= 2)
            myNetwork.ecName = items[1].remove("\"");

        if(items[0] == "LoopBack" && num_items >= 2)
            myNetwork.isLocalLoopBack = items[1].toInt();

        if(items[0] == "Simulation" && num_items >= 2)
            if(items[1] == "1") configuredSlaves.simulateECM = true;    // Simulation für Indexer und Encoder

        if(items[0] == "DEBUG_STEPS" && num_items >= 2)
            flagsDebug.steps = items[1].toInt();

        if(items[0] == "DEBUG_ACCELERATION" && num_items >= 2)
            flagsDebug.acc = items[1].toInt();

        if(items[0] == "DEBUG_RESIDUAL" && num_items >= 2)
            flagsDebug.res = items[1].toInt();

        if(items[0] == "DEBUG_ENCODER" && num_items >= 2)
            flagsDebug.enc = items[1].toInt();

        if(items[0] == "DEBUG_ENCSTATE" && num_items >= 2)
            flagsDebug.encStatus = items[1].toInt();

        if(items[0] == "DEBUG_DECIMALSEPARATOR" && num_items >= 2)
            if(items[1] == "\",\"")
                flagsDebug.decimal = comma;
            else
                flagsDebug.decimal = point;


        if(items[0] == "ECM701" && num_items >= 2)
        {
            configuredSlaves.ECM701count = items[1].toInt();
            if(num_items >= configuredSlaves.ECM701count+2)
                for(i = 0; i < configuredSlaves.ECM701count; ++i) configuredSlaves.ECM701index[i] = items[i + 2].toInt();   // Achtung, stürzt ab, wenn die items Liste zu kurz ist!
            else
                toShort = true;
        }
        if(items[0] == "ECM702" && num_items >= 2)
        {
            configuredSlaves.ECM702count = items[1].toInt();
            if(num_items >= configuredSlaves.ECM702count+2)
                for(i = 0; i < configuredSlaves.ECM702count; ++i) configuredSlaves.ECM702index[i] = items[i + 2].toInt();   // Achtung, stürzt ab, wenn die items Liste zu kurz ist!
            else
                toShort = true;
        }
        if(items[0] == "ECM712" && num_items >= 2)
        {
            configuredSlaves.ECM712count = items[1].toInt();
            if(num_items >= configuredSlaves.ECM712count+2)
                for(i = 0; i < configuredSlaves.ECM712count; ++i) configuredSlaves.ECM712index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "ECM713" && num_items >= 2)
        {
            configuredSlaves.ECM713count = items[1].toInt();
            if(num_items >= configuredSlaves.ECM713count+2)
                for(i = 0; i < configuredSlaves.ECM713count; ++i) configuredSlaves.ECM713index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "A760-01-B" && num_items >= 2)
        {
            configuredSlaves.A760_01_B_count = items[1].toInt();
            if(num_items >= configuredSlaves.A760_01_B_count+2)
                for(i = 0; i < configuredSlaves.A760_01_B_count; ++i) configuredSlaves.A760_01_B_index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "EL1024" && num_items >= 2)
        {
            configuredSlaves.EL1024count = items[1].toInt();
            if(num_items >= configuredSlaves.EL1024count+2)
                for(i = 0; i < configuredSlaves.EL1024count; ++i) configuredSlaves.EL1024index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "EL1104" && num_items >= 2)
        {
            configuredSlaves.EL1104count = items[1].toInt();
            if(num_items >= configuredSlaves.EL1104count+2)
                for(i = 0; i < configuredSlaves.EL1104count; ++i) configuredSlaves.EL1104index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "EL2202" && num_items >= 2)
        {
            configuredSlaves.EL2202count = items[1].toInt();
            if(num_items >= configuredSlaves.EL2202count+2)
                for(i = 0; i < configuredSlaves.EL2202count; ++i) configuredSlaves.EL2202index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "EL2612" && num_items >= 2)
        {
            configuredSlaves.EL2612count = items[1].toInt();
            if(num_items >= configuredSlaves.EL2612count+2)
                for(i = 0; i < configuredSlaves.EL2612count; ++i) configuredSlaves.EL2612index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }
        if(items[0] == "EL2622" && num_items >= 2)
        {
            configuredSlaves.EL2622count = items[1].toInt();
            if(num_items >= configuredSlaves.EL2622count+2)
                for(i = 0; i < configuredSlaves.EL2622count; ++i) configuredSlaves.EL2622index[i] = items[i + 2].toInt();
            else
                toShort = true;
        }

        if(items[0] == "OTHER" && num_items >= 2)
            configuredSlaves.OTHERcount = items[1].toInt();

        if(items[0] == "AXIS")  // Stürzt nicht mehr ab, wenn die items Liste zu kurz ist
        {
            countAxis += 1;
            i = items[1].toInt()-1;

            myAxesDialog.axisName[i] = items[2].remove("\"");

            myItemsToInt(&toShort, &myAxes.Axis[i].indexEncoder_A, items, 3);
            myItemsToInt(&toShort, &myAxes.Axis[i].indexEncoder_B, items, 4);
            myItemsToInt(&toShort, &myAxes.Axis[i].unitType, items, 5);
            //myItemsToInt(&toShort, &myAxes.Axis[i].feedbackType, items, 6);
            myItemsToDouble(&toShort, &myAxes.Axis[i].steps_per_unit, items, 7);

            myItemsToInt(&toShort, &myAxes.Axis[i].velocity, items, 8);
            myAxes.Axis[i].velocity_backup = myAxes.Axis[i].velocity;          // Kopie sichern

            myItemsToInt(&toShort, &myAxes.Axis[i].acceleration, items, 9);

            myItemsToInt(&toShort, &myAxes.Axis[i].hasBrake, items, 10);
            myItemsToDouble(&toShort, &myAxes.Axis[i].CL_Approach, items, 11);
            myItemsToDouble(&toShort, &myAxes.Axis[i].CL_DeltaMove, items, 12);
            myItemsToInt(&toShort, &myAxes.Axis[i].CL_WaitMove, items, 13);

            myItemsToDouble(&toShort, &myAxes.Axis[i].CL_DeltaStab, items, 14);
            myItemsToInt(&toShort, &myAxes.Axis[i].CL_Average, items, 15);
            myItemsToInt(&toShort, &myAxes.Axis[i].isGantry, items, 16);
            myItemsToInt(&toShort, &myAxes.Axis[i].axisType, items, 17);
            if(myAxes.Axis[i].axisType == 0) myAxes.Axis[i].axisType = 2;    // für die Kompatibilität zu alten Config Files

            myItemsToBool(&toShort, &myAxes.Axis[i].isStabilized, items, 18);
            myItemsToInt(&toShort, &myAxes.Axis[i].CL_WaitStab, items, 19);

            myItemsToDouble(&toShort, &myAxes.Axis[i].CL_DeltaFollow, items, 20);

            myItemsToInt(&toShort, &myAxes.Axis[i].initDirection, items, 21);

            myItemsToInt(&toShort, &myAxes.Axis[i].microSteps, items, 22);
            myItemsToInt(&toShort, &myAxes.Axis[i].motorCurrentRun, items, 23);
            myItemsToInt(&toShort, &myAxes.Axis[i].motorCurrentStop, items, 24);

            myItemsToInt(&toShort, &myAxes.Axis[i].jogDeltaSteps, items, 25);
            myItemsToDouble(&toShort, &myAxes.Axis[i].jogDeltaPosition, items, 26);

            myItemsToInt(&toShort, &myAxes.Axis[i].motor_direction, items, 27);  // die Vorzugsdrehrichtung

            myItemsToInt(&toShort, &myAxes.Axis[i].initSteps, items, 28);
            myItemsToBool(&toShort, &myAxes.Axis[i].isUsed, items, 29);          // ob die Achse überwacht wird, ersetzt "NU"

        }
        if(items[0] == "NoSimulatedAxes" && num_items >= 2)
            myAxes.numSimulatedAxes = items[1].toInt();

        if(items[0] == "CC_Rule")  // Stürzt nicht mehr ab, wenn die items Liste zu kurz ist
        {
            countCCRules += 1;
            i = items[1].toInt()-1;

            myItemsToInt(&toShort, &collisionControl.sensAxisIndex[i], items, 2);
            myItemsToInt(&toShort, &collisionControl.auxSelect[i], items, 3);
            myItemsToInt(&toShort, &collisionControl.targetAxisIndex[i], items, 4);
            myItemsToInt(&toShort, &collisionControl.targetAxisRoF[i], items, 5);
        }


        if(items[0] == "ENC")  // Achtung, stürzt ab, wenn die items Liste zu kurz ist!
        {
            if(num_items > 3)                   // zumindest die Grundparamter aus alten Konfigurationsdateien einlesen:
            {
                countEnc += 1;
                i = items[1].toInt()-1;
                myEncoderDialog.encName[i] = items[2].remove("\"");
            }

            myItemsToDouble(&toShort, &myEncoderChannels.Encoder[i].units_per_count, items, 3);
            myItemsToQint64(&toShort, &myEncoderChannels.Encoder[i].offsetCounts, items, 4);
            myItemsToQint64(&toShort, &myEncoderChannels.Encoder[i].limitLow, items, 5);
            myItemsToQint64(&toShort, &myEncoderChannels.Encoder[i].limitHigh, items, 6);
            myItemsToInt(&toShort, &myEncoderChannels.Encoder[i].encoderModus, items, 7);     // der Encoder Typ, Sonderbehandlung für Ringe an Drehachsen
            myItemsToDouble(&toShort, &myEncoderChannels.Encoder[i].offsetUnits, items, 8);   // Zusätzlicher Offset in Units, Sonderbehandlung für Ringe an Drehachsen
            myItemsToBool(&toShort, &myEncoderChannels.Encoder[i].doLogging, items, 9);
            myItemsToBool(&toShort, &myEncoderChannels.Encoder[i].isUsed, items, 10);       // ersetzt "NU"
       }

        // Die Konfiguration der Motor-Treiber einlesen. Achtung, falsche Werte sind gefährlich!
            // if(items[0] == "Motor_Ctrl" && num_items >= 2)
            //    motorDriverConfig.motor_Ctrl = items[1].toInt();
        if(items[0] == "Motor_tOff" && num_items >= 2)
            motorDriverConfig.motor_tOff = items[1].toInt();
        if(items[0] == "Motor_tBlank" && num_items >= 2)
            motorDriverConfig.motor_tBlank = items[1].toInt();
        if(items[0] == "Motor_tDecay" && num_items >= 2)
            motorDriverConfig.motor_tDecay = items[1].toInt();

        // Die zu öffnenden erweiterten Dialoge
        if(items[0] == "extend_Hexapod" && num_items >= 2)
            myDialogs.extOpen_Hexapod = items[1].toInt();
        if(items[0] == "extend_IO_Beckhoff" && num_items >= 2)
            myDialogs.extOpen_IO_Beckhoff = items[1].toInt();
        if(items[0] == "extend_MoveSequence" && num_items >= 2)
            myDialogs.extOpen_MoveSequence = items[1].toInt();
        if(items[0] == "extend_OTF_Tonic" && num_items >= 2)
            myDialogs.extOpen_OTF_Tonic = items[1].toInt();
        if(items[0] == "extend_SourceControl" && num_items >= 2)
            myDialogs.extOpen_SourceControl = items[1].toInt();

        // Die Führungsachse für OTF Capture
        if(items[0] == "OTF_AxisNr" && num_items >= 2)
            myAxes.otfAxisNr = items[1].toInt();


        // Die Konfiguration des Hexapods
        if(items[0] == "LegBase_H1" && num_items > 2)
        {
            hexa.LegBaseLocal_x[0] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[0] = myLocale.toDouble(items[2]);
        }
        if(items[0] == "LegBase_H2" && num_items > 2)
        {
            hexa.LegBaseLocal_x[1] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[1] = myLocale.toDouble(items[2]);
        }
        if(items[0] == "LegBase_H3" && num_items > 2)
        {
            hexa.LegBaseLocal_x[2] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[2] = myLocale.toDouble(items[2]);
        }
        if(items[0] == "LegBase_H4" && num_items > 2)
        {
            hexa.LegBaseLocal_x[3] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[3] = myLocale.toDouble(items[2]);
        }
        if(items[0] == "LegBase_H5" && num_items > 2)
        {
            hexa.LegBaseLocal_x[4] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[4] = myLocale.toDouble(items[2]);
        }
        if(items[0] == "LegBase_H6" && num_items > 2)
        {
            hexa.LegBaseLocal_x[5] = myLocale.toDouble(items[1]);
            hexa.LegBaseLocal_y[5] = myLocale.toDouble(items[2]);
        }

        if(items[0] == "LegUpper_H1" && num_items > 3)
        {
            hexa.LegUpperLocal_x[0] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[0] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[0] = myLocale.toDouble(items[3]);
        }
        if(items[0] == "LegUpper_H2" && num_items > 3)
        {
            hexa.LegUpperLocal_x[1] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[1] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[1] = myLocale.toDouble(items[3]);
        }
        if(items[0] == "LegUpper_H3" && num_items > 3)
        {
            hexa.LegUpperLocal_x[2] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[2] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[2] = myLocale.toDouble(items[3]);
        }
        if(items[0] == "LegUpper_H4" && num_items > 3)
        {
            hexa.LegUpperLocal_x[3] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[3] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[3] = myLocale.toDouble(items[3]);
        }
        if(items[0] == "LegUpper_H5" && num_items > 3)
        {
            hexa.LegUpperLocal_x[4] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[4] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[4] = myLocale.toDouble(items[3]);
        }
        if(items[0] == "LegUpper_H6" && num_items > 3)
        {
            hexa.LegUpperLocal_x[5] = myLocale.toDouble(items[1]);
            hexa.LegUpperLocal_y[5] = myLocale.toDouble(items[2]);
            hexa.LegUpperLocal_z[5] = myLocale.toDouble(items[3]);
        }

        if(items[0] == "HexaOffset_Z0" && num_items > 1)
            hexa.Offset_Z0 = myLocale.toDouble(items[1]);
        if(items[0] == "HexaDelta_ZPP" && num_items > 1)
            hexa.Delta_ZPP = myLocale.toDouble(items[1]);

        if(items[0] == "LegLenght_H1" && num_items > 1)
            hexa.LegLenght[0] = myLocale.toDouble(items[1]);
        if(items[0] == "LegLenght_H2" && num_items > 1)
            hexa.LegLenght[1] = myLocale.toDouble(items[1]);
        if(items[0] == "LegLenght_H3" && num_items > 1)
            hexa.LegLenght[2] = myLocale.toDouble(items[1]);
        if(items[0] == "LegLenght_H4" && num_items > 1)
            hexa.LegLenght[3] = myLocale.toDouble(items[1]);
        if(items[0] == "LegLenght_H5" && num_items > 1)
            hexa.LegLenght[4] = myLocale.toDouble(items[1]);
        if(items[0] == "LegLenght_H6" && num_items > 1)
            hexa.LegLenght[5] = myLocale.toDouble(items[1]);

        if(items[0] == "HexaAxis_H1" && num_items > 1)
            hexa.Axis_Number[0] = myLocale.toInt(items[1]);
        if(items[0] == "HexaAxis_H2" && num_items > 1)
            hexa.Axis_Number[1] = myLocale.toInt(items[1]);
        if(items[0] == "HexaAxis_H3" && num_items > 1)
            hexa.Axis_Number[2] = myLocale.toInt(items[1]);
        if(items[0] == "HexaAxis_H4" && num_items > 1)
            hexa.Axis_Number[3] = myLocale.toInt(items[1]);
        if(items[0] == "HexaAxis_H5" && num_items > 1)
            hexa.Axis_Number[4] = myLocale.toInt(items[1]);
        if(items[0] == "HexaAxis_H6" && num_items > 1)
            hexa.Axis_Number[5] = myLocale.toInt(items[1]);

        if(items[0] == "LASERPULSERATE" && num_items >= 2)
            sourceControl.laserPulseRate = items[1].toInt();
        if(items[0] == "PULSE_DELAY" && num_items >= 2)
            sourceControl.A760_TriggerDelay = myLocale.toInt(items[1]);

        if(items[0] == "AXIS_TR" && num_items >= 2)
            sourceControl.TR_AxisNo = items[1].toInt();
        if(items[0] == "AXIS_TT" && num_items >= 2)
            sourceControl.TT_AxisNo = items[1].toInt();
        if(items[0] == "AXIS_TC" && num_items >= 2)
            sourceControl.TC_AxisNo = items[1].toInt();
        if(items[0] == "T_TRANS_INC" && num_items >= 2)
            sourceControl.increment_trans = myLocale.toDouble(items[1]);
        if(items[0] == "TT_RANGE_LOW" && num_items >= 2)
            sourceControl.TT_Range_low = myLocale.toDouble(items[1]);
        if(items[0] == "TT_RANGE_HIGH" && num_items >= 2)
            sourceControl.TT_Range_high = myLocale.toDouble(items[1]);

        // Targetkorrektur mittel Quadrantendiode
        if(items[0] == "TC_ACTIVE" && num_items >= 2)
            sourceControl.TC_Active = items[1].toInt();
        if(items[0] == "TC_DELTASTEPS" && num_items >= 2)
            sourceControl.TC_delta_steps = items[1].toInt();
        if(items[0] == "TC_A_QUADS" && num_items >= 3)
        {
            sourceControl.A_Quad_1 = items[1].toInt();
            sourceControl.A_Quad_2 = items[2].toInt();
        }
        if(items[0] == "TC_B_QUADS" && num_items >= 3)
        {
            sourceControl.B_Quad_1 = items[1].toInt();
            sourceControl.B_Quad_2 = items[2].toInt();
        }
        if(items[0] == "TC_LIMIT" && num_items >= 2)
            sourceControl.TC_Limit = myLocale.toDouble(items[1]);

        if(items[0] == "TC_AVERAGE" && num_items >= 2)
            sourceControl.numAverage = items[1].toInt();

        if(items[0] == "TC_OFFSET_Q1" && num_items >= 2)
            sourceControl.offset_Q1 = items[1].toInt();
        if(items[0] == "TC_OFFSET_Q2" && num_items >= 2)
            sourceControl.offset_Q2 = items[1].toInt();
        if(items[0] == "TC_OFFSET_Q3" && num_items >= 2)
            sourceControl.offset_Q3 = items[1].toInt();
        if(items[0] == "TC_OFFSET_Q4" && num_items >= 2)
            sourceControl.offset_Q4 = items[1].toInt();




    }   // end of while
    myAxes.numAxisConfig = countAxis;                   // zu klären: Wie ist der Verhältnis gefundener zu konfigurierter Achsen - ist geklärt, verwendet werden immer die tatsächlich gefundene Achsen
                                                        // wenn diese von der Konfiguration abweichen gibt es einen Topology Error
    myEncoderChannels.numEncoderConfig = countEnc;      //
    if(countCCRules == 0) countCCRules = 1;             // damit es immer ein Template im Config-File gibt
    collisionControl.numRules = countCCRules;           // die Zahl der verwendeten logischen Verknüpfungen/Regeln ist gleich der Zahl der Einträge im .cfg-File

    if(toShort)
        fprintf(stderr, "Error reading configuration file: Too less arguments!\n");
}


bool checkConfiguration()   // Konfiguration aus Datei mit den tatsächlich gefundenen Slaves vergleichen, findet auch Änderungen in der Reihenfolge
{
    int i;
    bool result = true;

    if(configuredSlaves.ECM701count != foundSlaves.ECM701count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.ECM701count; ++i)
            if(configuredSlaves.ECM701index[i] != foundSlaves.ECM701index[i]) result = false;

    if(configuredSlaves.ECM702count != foundSlaves.ECM702count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.ECM702count; ++i)
            if(configuredSlaves.ECM702index[i] != foundSlaves.ECM702index[i]) result = false;

    if(configuredSlaves.ECM712count != foundSlaves.ECM712count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.ECM712count; ++i)
            if(configuredSlaves.ECM712index[i] != foundSlaves.ECM712index[i]) result = false;

    if(configuredSlaves.ECM713count != foundSlaves.ECM713count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.ECM713count; ++i)
            if(configuredSlaves.ECM713index[i] != foundSlaves.ECM713index[i]) result = false;

    if(configuredSlaves.A760_01_B_count != foundSlaves.A760_01_B_count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.A760_01_B_count; ++i)
            if(configuredSlaves.A760_01_B_index[i] != foundSlaves.A760_01_B_index[i]) result = false;

    if(configuredSlaves.EL1024count != foundSlaves.EL1024count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.EL1024count; ++i)
            if(configuredSlaves.EL1024index[i] != foundSlaves.EL1024index[i]) result = false;

    if(configuredSlaves.EL1104count != foundSlaves.EL1104count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.EL1104count; ++i)
            if(configuredSlaves.EL1104index[i] != foundSlaves.EL1104index[i]) result = false;

    if(configuredSlaves.EL2202count != foundSlaves.EL2202count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.EL2202count; ++i)
            if(configuredSlaves.EL2202index[i] != foundSlaves.EL2202index[i]) result = false;

    if(configuredSlaves.EL2612count != foundSlaves.EL2612count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.EL2612count; ++i)
            if(configuredSlaves.EL2612index[i] != foundSlaves.EL2612index[i]) result = false;

    if(configuredSlaves.EL2622count != foundSlaves.EL2622count)
        result = false;
    else
        for(i = 0; i < configuredSlaves.EL2622count; ++i)
            if(configuredSlaves.EL2622index[i] != foundSlaves.EL2622index[i]) result = false;

    return result;
}

void writeOtfData()
{
    QString     fileName = QDir::homePath() + "/Data_Motion/OtfData.log";
    QFile       otfdatafile(fileName);

    if(otfdatafile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        fprintf(stderr, "Write OTF Capture Data to File \"OtfData.log\" \n");
        QTextStream out(&otfdatafile);                                   // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        out << "OTF Capture Data in Units\n";
        out << "Saved on " << QDateTime::currentDateTime().toString() << "\n";

        // Die Achsen mit OTF-Encodern suchen und Array dazu aufbauen
        myAxes.numOTFmax = 0;
        for(int axis=0; axis < myAxes.numAxis; ++axis)
        {
            int         numOTF;
            int         indexA = myAxes.Axis[axis].indexEncoder_A -1;
            int         indexB = myAxes.Axis[axis].indexEncoder_B -1;
            long long   diffCounts;

            myAxes.Axis[axis].numOTF = 0;
            // Nur Encoder A genutzt
            if((indexA >= 0) && (indexB == -1))
            {
                numOTF = myEncoderChannels.Encoder[indexA].otfIndex;
                myAxes.Axis[axis].numOTF = numOTF;
                if(numOTF > myAxes.numOTFmax) myAxes.numOTFmax = numOTF;
                for(int j=0; j < numOTF; ++j)
                {
                    diffCounts = myEncoderChannels.Encoder[indexA].otfCounts[j] - myEncoderChannels.Encoder[indexA].offsetCounts;
                    myAxes.Axis[axis].positionUnitsOTF[j] = myEncoderChannels.Encoder[indexA].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[indexA].offsetUnits;
                }
            }

            // Nur Encoder B genutzt
            if((indexB >= 0) && (indexA == -1))
            {
                numOTF = myEncoderChannels.Encoder[indexB].otfIndex;
                myAxes.Axis[axis].numOTF = numOTF;
                if(numOTF > myAxes.numOTFmax) myAxes.numOTFmax = numOTF;
                for(int j=0; j < numOTF; ++j)
                {
                    diffCounts = myEncoderChannels.Encoder[indexB].otfCounts[j] - myEncoderChannels.Encoder[indexB].offsetCounts;
                    myAxes.Axis[axis].positionUnitsOTF[j] = myEncoderChannels.Encoder[indexB].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[indexB].offsetUnits;
                }
            }

            // Beide Encoder A und B genutzt, dies aktiviert implizit Mittelwertbildung, feedbackType wird hier nicht mehr genutzt
            if((indexA >= 0) && (indexB >= 0))
            {
                numOTF = myEncoderChannels.Encoder[indexA].otfIndex;
                myAxes.Axis[axis].numOTF = numOTF;
                if(numOTF > myAxes.numOTFmax) myAxes.numOTFmax = numOTF;
                for(int j=0; j < numOTF; ++j)
                {
                    diffCounts = myEncoderChannels.Encoder[indexA].otfCounts[j] - myEncoderChannels.Encoder[indexA].offsetCounts;
                    double posA = myEncoderChannels.Encoder[indexA].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[indexA].offsetUnits;

                    diffCounts = myEncoderChannels.Encoder[indexB].otfCounts[j] - myEncoderChannels.Encoder[indexB].offsetCounts;
                    myAxes.Axis[axis].positionUnitsOTF[j] = 0.5 * (posA + myEncoderChannels.Encoder[indexB].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[indexB].offsetUnits);
                }
            }
        }

        // Die Kopfzeile der Tabelle
        for(int axis=0; axis < myAxes.numAxis; ++axis)
        {
            if( myAxes.Axis[axis].numOTF > 0)
                out << "\t" << axis+1 << "=\"" << myAxesDialog.axisName[axis] << "\"";
        }
        out << "\n";

        //QLocale::setDefault(QLocale::German);   // Dezimal-Komma für Quickplot ???
        // Die Zeilen der Tabelle
        for(int j=0; j < myAxes.numOTFmax; ++j)
        {
            out << j+1;
            for(int axis=0; axis < myAxes.numAxis; ++axis)
            {
                if(myAxes.Axis[axis].numOTF > 0)
                    out << "\t" << QString("%L1").arg(myAxes.Axis[axis].positionUnitsOTF[j], 0, 'f', 8);
            }
            out << "\n";
        }
        //QLocale::setDefault(myLocale);      // Globales locale für die App wiederherstellen

        out << "[END_OF_FILE]\n";
        otfdatafile.close();
    }
}

void writeRotationState()
{
    QString     fileName = QDir::homePath() + "/Config_Motion/RotationState.cfg";     // den Status der 361 Grad Ringe laden
    QFile       myFile(fileName);

    if (myFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        fprintf(stderr, "Write Rotation State to File \"RotationState.cfg\" \n");

        QTextStream out(&myFile);       // Pointer auf Objekt übergeben
        out << "Rotation State for ecTool\n";
        out << "Saved on " << QDateTime::currentDateTime().toString() << "\n";
        out << "\n";
        for(int i=0; i<myEncoderChannels.numEncoder; i++)
        {
            if(myEncoderChannels.Encoder[i].encoderModus == ENCMODUS_R200)
            {
                if(myEncoderChannels.Encoder[i].rotationState == -1)
                    out << "ENC " << (i+1) << " minus\n";
                else
                    if(myEncoderChannels.Encoder[i].rotationState == 1)
                        out << "ENC " << (i+1) << " plus\n";
                    else
                        out << "ENC " << (i+1) << " undefined\n";
            }
        }
        out << "\n[END_OF_FILE]\n";
        myFile.close();
    }
    else
        fprintf(stderr, "Error: Cannot write Rotation State to File!\n");
}


void readRotationState()    // bei Programmstart Rotation Status aus Datei lesen und in die Encoder Liste eintragen
{
    QString     fileName = QDir::homePath() + "/Config_Motion/RotationState.cfg";     // den Status der 361 Grad Ringe laden
    QFile       myFile(fileName);

    if(myFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        fprintf(stderr, "Found Rotation State File \"RotationState.cfg\" - reading State in.\n\n");

        QTextStream in(&myFile);

        while (!in.atEnd()) {
            QString line = in.readLine();
            line = line.simplified();
            QStringList items = line.split(" ");

            if(items[0] == "ENC" && items.size() == 3)      // Absturzschutz, falls die items Liste zu kurz ist. Sollte hier nie kommen.
            {
                int enc = items[1].toInt()-1;
                if(items[2] == "plus")
                    myEncoderChannels.Encoder[enc].rotationState = 1;
                if(items[2] == "minus")
                    myEncoderChannels.Encoder[enc].rotationState = -1;
            }

        }
        myFile.close();
    }
    else
        fprintf(stderr, "Warning: No File \"RotationState.cfg\" found, Rotation State is undefined!\n\n");
}


void writeAxesState()
{
    QFile   axesstatefile;          // jeweils letzte Position aller Achsen
    QString line;

    QString path = QDir::homePath() + QString("/Data_Motion/AxesState.log");    // Das Logfile für den Zustand der Achsen:
    axesstatefile.setFileName(path);

    if(axesstatefile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&axesstatefile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        out << "AXES:\n";
        for(int axis = 0; axis < myAxes.numAxis; ++axis)
        {
            line = QString("%L1 %L2 %L3").arg(axis+1).arg(myAxesDialog.axisName[axis]).arg(myAxes.Axis[axis].positionUnits);
            out << line + "\n";
        }

        out << "\nENCODERS:\n";
        for(int enc = 0; enc < myEncoderChannels.numEncoder; ++enc)
        {
            line = QString("%L1 %L2 %L3").arg(enc+1).arg(myEncoderDialog.encName[enc]).arg(myEncoderChannels.Encoder[enc].counts);
            out << line + "\n";
        }

        axesstatefile.close();
    }

}


//
//
//              Hier sind alle GUI-Funktionen für den Move Sequence Dialog
//              Ist wegen der engen Verzahnung mit myTimerTask in maindialog.cpp untergebracht
//
//  Autor:      Christian Grundel
//
//  28.07.2020  Neues Modul angelegt
//  29.07.2020  Nach maindialog.cpp verpflanzt
//  08.08.2020  Alle drei Drehungen drin, Achsen fahren
//  09.08.2020  Parameter Delta_ZPP ergänzt, ist die Distanz zwischen dem Pivot und dem Ursprung der Platform
//  10.08.2020  Draudsicht auf das Layout funktioniert, die Lösung war mit new QGraphicsScene() ein neues Objekt zu erzeugen.


//#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsRectItem>

ecHexapodDialog::ecHexapodDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecHexapodDialog)
{
    ui->setupUi(this);
    ui->graphicsView->scale(0.9, 0.9);  // nur einmalig bei Erzeugung des Dialogs aufrufen, sonst wird es immer kleiner
}

void ecHexapodDialog::reject()
{
    myDialogs.isOpen_Hexapod = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}

// Initialisiert den Dialog und frischt ihn auf
void ecHexapodDialog::SetContent()
{
    ui->lineEdit_dof_X->setText(QString("%L1").arg(hexa.dof_X, 11, 'f', 6));
    ui->lineEdit_dof_Y->setText(QString("%L1").arg(hexa.dof_Y, 11, 'f', 6));
    ui->lineEdit_dof_Z->setText(QString("%L1").arg(hexa.dof_Z, 11, 'f', 6));
    ui->lineEdit_dof_Yaw->setText(QString("%L1").arg(hexa.dof_Yaw, 11, 'f', 6));
    ui->lineEdit_dof_Pitch->setText(QString("%L1").arg(hexa.dof_Pitch, 11, 'f', 6));
    ui->lineEdit_dof_Roll->setText(QString("%L1").arg(hexa.dof_Roll, 11, 'f', 6));
    ui->lineEdit_dof_Z0->setText(QString("%L1").arg(hexa.Offset_Z0, 11, 'f', 6));
    ui->lineEdit_dof_ZPP->setText(QString("%L1").arg(hexa.Delta_ZPP, 11, 'f', 6));

    ui->label_Axis_H1->setText(QString("%L1").arg(hexa.AxisH[0], 11, 'f', 6));
    ui->label_Axis_H2->setText(QString("%L1").arg(hexa.AxisH[1], 11, 'f', 6));
    ui->label_Axis_H3->setText(QString("%L1").arg(hexa.AxisH[2], 11, 'f', 6));
    ui->label_Axis_H4->setText(QString("%L1").arg(hexa.AxisH[3], 11, 'f', 6));
    ui->label_Axis_H5->setText(QString("%L1").arg(hexa.AxisH[4], 11, 'f', 6));
    ui->label_Axis_H6->setText(QString("%L1").arg(hexa.AxisH[5], 11, 'f', 6));


    // Draufsicht von oben auf das Layout des Hexapods
    QGraphicsScene *scene = new QGraphicsScene();

    QPen redPen(Qt::red);
    QPen bluePen(Qt::blue);
    //QPen blackPen(Qt::black);

    for(int i=0; i<5; ++i)
    {
        scene->addLine(hexa.LegBaseLocal_x[i], -hexa.LegBaseLocal_y[i], hexa.LegBaseLocal_x[i+1], -hexa.LegBaseLocal_y[i+1], bluePen);
        scene->addLine(hexa.LegUpperLocal_x[i], -hexa.LegUpperLocal_y[i], hexa.LegUpperLocal_x[i+1], -hexa.LegUpperLocal_y[i+1], redPen);
    }
    scene->addLine(hexa.LegBaseLocal_x[5], -hexa.LegBaseLocal_y[5], hexa.LegBaseLocal_x[0], -hexa.LegBaseLocal_y[0], bluePen);
    scene->addLine(hexa.LegUpperLocal_x[5], -hexa.LegUpperLocal_y[5], hexa.LegUpperLocal_x[0], -hexa.LegUpperLocal_y[0], redPen);

    QGraphicsTextItem *item;
    for(int i=0; i<6; ++i)
    {
        item = scene->addText(QString("H%1").arg(i+1));
        item->setPos(hexa.LegBaseLocal_x[i], -hexa.LegBaseLocal_y[i]);
    }
    for(int i=0; i<6; ++i)
    {
        item = scene->addText(QString("H%1").arg(i+1));
        item->setPos(hexa.LegUpperLocal_x[i], -hexa.LegUpperLocal_y[i]);
    }

    item = scene->addText("View from Top");
    //item->setDefaultTextColor(Qt::blue);
    item->setPos(-200, -250);

    item = scene->addText("Base");
    item->setDefaultTextColor(Qt::blue);
    item->setPos(-50, -250);

    item = scene->addText("Upper");
    item->setDefaultTextColor(Qt::red);
    item->setPos(50, -250);


    scene->addLine(0, 0, 0, -120);  // y-Achse
    scene->addLine(0, -120, 6, -110);
    scene->addLine(0, -120, -6, -110);

    scene->addLine(0, 0, 120, 0);   // x-Achse
    scene->addLine(120, 0, 110, 6);
    scene->addLine(120, 0, 110, -6);

    item = scene->addText("X");
    item->setPos(110, 0);

    item = scene->addText("Y");
    item->setPos(0, -110);

    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();

}


void ecHexapodDialog::on_lineEdit_dof_X_editingFinished()
{
    hexa.dof_X = myLocale.toDouble(ui->lineEdit_dof_X->text());
    ui->lineEdit_dof_X->setText(QString("%L1").arg(hexa.dof_X, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Y_editingFinished()
{
    hexa.dof_Y = myLocale.toDouble(ui->lineEdit_dof_Y->text());
    ui->lineEdit_dof_Y->setText(QString("%L1").arg(hexa.dof_Y, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Z_editingFinished()
{
    hexa.dof_Z = myLocale.toDouble(ui->lineEdit_dof_Z->text());
    ui->lineEdit_dof_Z->setText(QString("%L1").arg(hexa.dof_Z, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Z0_editingFinished()
{
    hexa.Offset_Z0 = myLocale.toDouble(ui->lineEdit_dof_Z0->text());
    ui->lineEdit_dof_Z0->setText(QString("%L1").arg(hexa.Offset_Z0, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_ZPP_editingFinished()
{
    hexa.Delta_ZPP = myLocale.toDouble(ui->lineEdit_dof_ZPP->text());
    ui->lineEdit_dof_ZPP->setText(QString("%L1").arg(hexa.Delta_ZPP, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Yaw_editingFinished()
{
    hexa.dof_Yaw = myLocale.toDouble(ui->lineEdit_dof_Yaw->text());
    ui->lineEdit_dof_Yaw->setText(QString("%L1").arg(hexa.dof_Yaw, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Pitch_editingFinished()
{
    hexa.dof_Pitch = myLocale.toDouble(ui->lineEdit_dof_Pitch->text());
    ui->lineEdit_dof_Pitch->setText(QString("%L1").arg(hexa.dof_Pitch, 11, 'f', 6));
}

void ecHexapodDialog::on_lineEdit_dof_Roll_editingFinished()
{
    hexa.dof_Roll = myLocale.toDouble(ui->lineEdit_dof_Roll->text());
    ui->lineEdit_dof_Roll->setText(QString("%L1").arg(hexa.dof_Roll, 11, 'f', 6));
}

// Achswerte nur berechnen aber nicht hinfahren
void ecHexapodDialog::on_pushButton_calculateHexaPose_clicked()
{
    DOF_to_Axes();
    SetContent();
}

void ecHexapodDialog::on_pushButton_moveHexaToPose_clicked()
{
    DOF_to_Axes();
    SetContent();
    //Move_Axes_to_DOF();

    double maxDelta, delta[6], velFactor;

    // den längsten Fahrweg suchen
    maxDelta = 0;
    for(int i = 0; i < 6; ++i)
    {
        delta[i] = fabs(hexa.AxisH[i] - myAxes.Axis[hexa.Axis_Number[i]-1].positionUnits);
        if(delta[i] > maxDelta)
            maxDelta = delta[i];
    }

    for(int i = 0; i < 6; ++i)
    {
        if(maxDelta > 0)
            velFactor = delta[i] / maxDelta;
        else
            velFactor = 1.0;
        hexaMoveToUnits(hexa.Axis_Number[i]-1, hexa.AxisH[i], velFactor);     // ist über connect() im MainDialog mit MoveToUnits verbunden
    }

}


// das Quadrat
double pow2(double argument)
{
    return argument*argument;
}

// Aus den Degree of Freedoms die Werte für die Achsenpositionen berechnen
// Die Parallelkinematik geht in diese Richtung analytisch mit geschlossenen Gleichungen
void DOF_to_Axes()
{
    struct
    {
        double LegUpper_x[6];
        double LegUpper_y[6];
        double LegUpper_z[6];
    } temp;

    double phi;

    /*
    phi = -120.0 * M_PI / 180.0;
    double x_120 = hexa.LegBaseLocal_x[0] * cos(phi) - hexa.LegBaseLocal_y[0] * sin(phi);
    double y_120 = hexa.LegBaseLocal_x[0] * sin(phi) + hexa.LegBaseLocal_y[0] * cos(phi);
    */


    // erster Schritt, aus den DOFs die globalen Koordinaten der oberen Endpunkte der Beine berechnen,
    // hierin liegt implizit die Definition der DOFs

    // Alle Drehungen erfolgen jeweils um (0,0,0)
    // Danach kommt die Translation mit Z0 als Offset, dies macht effektiv (0,0,Z0) zum pivot

    // Erste Translation, setzt die Differenz zwischen Ursprung Platform und Pivot
    for(int i = 0; i < 6; ++i)
    {
        temp.LegUpper_x[i] = hexa.LegUpperLocal_x[i];
        temp.LegUpper_y[i] = hexa.LegUpperLocal_y[i];
        temp.LegUpper_z[i] = hexa.LegUpperLocal_z[i] - hexa.Delta_ZPP;
    }

    // Drehung um x = roll
    phi = hexa.dof_Roll *  M_PI / 180.0;
    for(int i = 0; i < 6; ++i)
    {
        hexa.LegUpper_y[i] = temp.LegUpper_y[i] * cos(phi) - temp.LegUpper_z[i] * sin(phi);
        hexa.LegUpper_z[i] = temp.LegUpper_y[i] * sin(phi) + temp.LegUpper_z[i] * cos(phi);
        hexa.LegUpper_x[i] = temp.LegUpper_x[i];
    }

    // Drehung um y = pitch
    phi = hexa.dof_Pitch *  M_PI / 180.0;     // Grad zu Bogenmaß
    for(int i = 0; i < 6; ++i)
    {
        temp.LegUpper_x[i] = hexa.LegUpper_x[i] * cos(phi) - hexa.LegUpper_z[i] * sin(phi);
        temp.LegUpper_z[i] = hexa.LegUpper_x[i] * sin(phi) + hexa.LegUpper_z[i] * cos(phi);
        temp.LegUpper_y[i] = hexa.LegUpper_y[i];
    }

    // Drehung um z = yaw
    phi = hexa.dof_Yaw *  M_PI / 180.0;     // Grad zu Bogenmaß
    for(int i = 0; i < 6; ++i)
    {
        hexa.LegUpper_x[i] = temp.LegUpper_x[i] * cos(phi) - temp.LegUpper_y[i] * sin(phi);
        hexa.LegUpper_y[i] = temp.LegUpper_x[i] * sin(phi) + temp.LegUpper_y[i] * cos(phi);
        hexa.LegUpper_z[i] = temp.LegUpper_z[i];
    }

    // Zweite Translation, der Pivot wird auf z=Z0 angehoben, die dofs xyz dazu
    for(int i = 0; i < 6; ++i)
    {
        hexa.LegUpper_x[i] = hexa.LegUpper_x[i] + hexa.dof_X;
        hexa.LegUpper_y[i] = hexa.LegUpper_y[i] + hexa.dof_Y;
        hexa.LegUpper_z[i] = hexa.LegUpper_z[i] + hexa.dof_Z + hexa.Offset_Z0;
    }

    // zweiter Schritt, aus den globalen Koordinaten der oberen und unteren Endpunkte der Beine die Achs-Werte berechnen
    for(int i = 0; i < 6; ++i)
    {
        double delta_hq = pow2(hexa.LegLenght[i]) - pow2(hexa.LegUpper_x[i] - hexa.LegBaseLocal_x[i]) - pow2(hexa.LegUpper_y[i] - hexa.LegBaseLocal_y[i]);
        hexa.AxisH[i] = hexa.LegUpper_z[i] - sqrt(delta_hq);
    }

}



//
//          Hier folgen alle GUI-Funktionen für den Achsen Parameter Dialog
//

//  Der Konstruktor des Dialogs für die Achsparameter
ecAxisDialog::ecAxisDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecAxisDialog)
{

    ui->setupUi(this);

}

void ecAxisDialog::reject()
{
    myDialogs.isOpen_Axis = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}


// Aktualisiert den sich dynamisch ändernden Teil der Achsentabelle
void ecAxisDialog::UpdateContent()
{
    for(int column = 0; column < myAxes.numAxis; ++column)
    {
        if(myAxes.Axis[column].axisDriverType == 1 ) // Auswahl nur für die Achsen des ECM-713 anzeigen
        {
            ui->tableWidget_Axis->setItem(25, column, new QTableWidgetItem(QString("%L1").arg(myAxes.Axis[column].motorStatus, 1, 16).toUpper()));
        }

        /*
            item = new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].positionUnits, 0, 'f', 6));
            ui->tableWidget_Encoder->setItem(9, column, item);

            status = "Invalid";     // sollte nie kommen
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_OK)
                status = "Ok";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_WARN)
                status = "Warn";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_LIMITGANTRY)
                status = "Limit";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_ZEROLOST)
                status = "ZeroLost";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_ERROR)
                status = "Error";

            item = new QTableWidgetItem(status);
            ui->tableWidget_Encoder->setItem(10, column, item);

            if(myEncoderChannels.Encoder[column].revision != 0)
            {
                item = new QTableWidgetItem(QString("%1").arg(0.1 * myEncoderChannels.Encoder[column].revision));
                ui->tableWidget_Encoder->setItem(11, column, item);
            }
            */
    }
}

// Initialisiert die Achsentabelle mit Inhalt aus den Parametern
void ecAxisDialog::InitContent()
{
    int column, index, row;
    QString     qString;
    QStringList header;

    // hier die Axentabelle:

    ui->tableWidget_Axis->setColumnCount(myAxes.numAxis);
    ui->tableWidget_Axis->setRowCount(27);  // mit "In Use" auf 27 Zeilen angewachsen

    header << "Name" << "Limits in Use" << "Unit" << "Scale Motor (Steps/Unit)" << "Velocity (Steps/s)" << "Acceleration (Steps/s2)"
           << "Encoder A" << "Encoder B" << "Reserved" << "Is Gantry with" << "Axis Type" << "Is Stabilized"
           << "CL Approach Factor" << "CL Delta Move (Units)" << "CL Wait Move (ms)" << "CL Delta Stab (Units)" << "CL Wait Stab (ms)" << "CL Average (ms)" << "CL Delta Follow (Units)"
           << "Init Direction" << "Init Steps" << "Driver" << "microSteps" << "Run Current (Arms)" << "Stop Current (Arms)" << "Motor Status" << "Motor Direction";
    ui->tableWidget_Axis->setVerticalHeaderLabels(header);
    ui->tableWidget_Axis->setCurrentCell(0, myAxes.currentAxis);

    //QTableWidgetItem *headerItem = new QTableWidgetItem(tr("Cubes"));
    //QListWidget *listWidget_Unit = new QListWidget(this);

    ui->tableWidget_Axis->verticalHeaderItem(2)->setToolTip("for informational purpose only");                                  // Unit
    ui->tableWidget_Axis->verticalHeaderItem(3)->setToolTip("Microsteps per Unit of movement, the Sign is sense of travel");    // Scale
    ui->tableWidget_Axis->verticalHeaderItem(6)->setToolTip("Index of Encoder A to be used with this Axis");                    // Encoder A
    ui->tableWidget_Axis->verticalHeaderItem(7)->setToolTip("Index of Encoder B to be used with this Axis");                    // Encoder B
    ui->tableWidget_Axis->verticalHeaderItem(8)->setToolTip("Whether one or the arithmetic mean of two Encoders are used");     // Feedback

    ui->tableWidget_Axis->verticalHeaderItem(19)->setToolTip("Which Limit Switch is used for Referencing");                     // Init Direction
    ui->tableWidget_Axis->verticalHeaderItem(20)->setToolTip("Number of reverse Steps from Limit Switch searching the Zero Mark");                     // Init Steps

    ui->tableWidget_Axis->verticalHeaderItem(22)->setToolTip("Used for ECM-713 Motor Driver, changes are effective only after \"Restart EtherCAT\"");     // microSteps
    ui->tableWidget_Axis->verticalHeaderItem(23)->setToolTip("Run Current of Motor, changes are effective immediatly");         // Run Current
    ui->tableWidget_Axis->verticalHeaderItem(24)->setToolTip("Stop Current of Motor, changes are effective immediatly");        // Stop Current

    ui->tableWidget_Axis->verticalHeaderItem(26)->setToolTip("Travel Direction of Motor");        // Motor Direction

    for(column = 0; column < myAxes.numAxis; column++)
    {
        //sprintf(cstring, "Axis %2d", column+1);
        ui->tableWidget_Axis->setHorizontalHeaderItem(column, new QTableWidgetItem(QString("Axis %1").arg(column+1, 2)));

        row = 0;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(myAxesDialog.axisName[column]));

        ++row;
        if(myAxes.Axis[column].isUsed)
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("yes"));
        else
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("no"));

        ++row;
        switch(myAxes.Axis[column].unitType)
        {
        case 0:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_1));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_2));
            break;
        case 2:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_3));
            break;
        default:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("invalid"));
            break;
        }

        ++row;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].steps_per_unit, 0, 'f', 2) ));

        ++row;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString::number(myAxes.Axis[column].velocity)));

        ++row;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString::number(myAxes.Axis[column].acceleration)));

        ++row;
        index = myAxes.Axis[column].indexEncoder_A;
        if(index == 0)
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
        }
        else
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Enc%1").arg(index, 2)));
        }

        ++row;
        index = myAxes.Axis[column].indexEncoder_B;
        if(index == 0)
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
        }
        else
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Enc%1").arg(index, 2)));
        }

        ++row;
        /*
        switch(myAxes.Axis[column].feedbackType)
        {
        case 0:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("A"));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("B"));
            break;
        case 2:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("Mean A,B"));
            break;
        default:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("invalid"));
            break;
        }
        */

        ++row;
        index = myAxes.Axis[column].isGantry;
        if(index == 0)
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
        }
        else
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Axis %1").arg(index, 2)));
        }

        ++row;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Type %1").arg(myAxes.Axis[column].axisType)));

        ++row;
        if(myAxes.Axis[column].isStabilized)
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("yes"));
        else
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("no"));

        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_Approach) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_DeltaMove) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_WaitMove) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_DeltaStab) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_WaitStab) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_Average) ));
        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].CL_DeltaFollow) ));

        ++row;
        switch(myAxes.Axis[column].initDirection)
        {
        case -1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("Reverse"));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("Forward"));
            break;
        default:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("invalid"));
        }

        ui->tableWidget_Axis->setItem(++row, column, new QTableWidgetItem( QString("%L1").arg(myAxes.Axis[column].initSteps) ));

        ++row;
        switch(myAxes.Axis[column].axisDriverType)
        {
        case 0:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("ECM-712"));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("ECM-713"));
            break;
        }

        if(myAxes.Axis[column].axisDriverType == 1 ) // Auswahl nur für die Achsen des ECM-713 anzeigen
        {
            ++row;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(1 << myAxes.Axis[column].microSteps)));

            ++row;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(0.02 * myAxes.Axis[column].motorCurrentRun, 0, 'f', 1)));

            ++row;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(0.02 * myAxes.Axis[column].motorCurrentStop, 0, 'f', 1)));

            ++row;  // Motor Status
            ++row;
            if(myAxes.Axis[column].motor_direction == 1)
                ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("Positive"));
            else
                ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("Negative"));

        }

        if(myAxes.Axis[column].isGantry != 0)
        {   // bei Gantry alle Parameter durch Sternchen ersetzen, da unwirksam
            for(int i=2; i<=8; ++i)
                ui->tableWidget_Axis->setItem(i, column,  new QTableWidgetItem("*"));
            for(int i=10; i<=20; ++i)
                ui->tableWidget_Axis->setItem(i, column,  new QTableWidgetItem("*"));

        }

    }

}


void ecAxisDialog::on_tableWidget_Axis_cellDoubleClicked(int clickedRow, int column)
{
    int row, chooseRow;
    bool ok;
    QString text;
    double  dWert;
    int     iWert;

    //myAxis.currentAxis = column;  die aktuelle Achse soll *nicht* verändert werden, egal welche Parameter editiert wurden

    row = 0;
    if(clickedRow == row)     // die erste Zeile mit den symbolischennamen der Achsen
    {
        text = QInputDialog::getText(this, "Axis " + QString("%L1").arg(column+1) , "Input new axis name:", QLineEdit::Normal, "", &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(0, column, new QTableWidgetItem(text));
            myAxesDialog.axisName[column] = text;
        }

    }

    ++row;
    if(clickedRow == row)     // in use, ersetzt "NU"
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_YESNO);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        if(chooseRow == 0)
        {
            myAxes.Axis[column].isUsed = false;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("no"));
        }
        else
        {
            myAxes.Axis[column].isUsed = true;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("yes"));
        }
    }


    ++row;
    if(clickedRow == row)     // die zweite Zeile mit den Einheiten
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_UNIT);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        switch(chooseRow)
        {
        case 0:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_1));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_2));
            break;
        case 2:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem(UNIT_3));
            break;
        default:
            ui->tableWidget_Axis->setItem(row, column,  new QTableWidgetItem("invalid"));
            break;
        }
        myAxes.Axis[column].unitType = chooseRow;   // impliziter Returnwert über Global
    }

    ++row;
    if(clickedRow == row)     // die dritte Zeile mit dem mechanischen Übersetzungsverhältnis
    {
        dWert = myAxes.Axis[column].steps_per_unit;
        dWert = QInputDialog::getDouble(this, "Axis " + QString("%L1").arg(column+1) , "Input Motor Scale in Steps/Unit:", dWert, -1e8, 1e8, 2, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert, 0, 'f', 2)));
            myAxes.Axis[column].steps_per_unit = dWert;               // keine Zerlegung in den Betrag
            //myAxes.Axis[column].steps_direction = copysign(1.0, dWert);     // und das Vorzeichen
        }
    }

    ++row;
    if(clickedRow == row)
    {
        iWert = myAxes.Axis[column].velocity;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Velocity in Steps/s:", iWert, 1, 5000000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].velocity = iWert;
            myAxes.Axis[column].velocity_backup = iWert; // Kopie
        }
    }

    ++row;
    if(clickedRow == row)
    {
        iWert = myAxes.Axis[column].acceleration;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Acceleration in Steps/s2:", iWert, 1, 5000000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].acceleration = iWert;
        }
    }

    ++row;
    if(clickedRow == row)     // die achte und neunte Zeile mit der Auswahl der Encoder
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_ENCODER);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück
        if(chooseRow == 0)
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
        }
        else
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Enc%1").arg(chooseRow)));
        }
        myAxes.Axis[column].indexEncoder_A = chooseRow;
    }

    ++row;
    if(clickedRow == row)     // die achte und neunte Zeile mit den Encodern
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_ENCODER);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        if(chooseRow == 0)
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
        }
        else
        {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Enc%1").arg(chooseRow)));
        }
        myAxes.Axis[column].indexEncoder_B = chooseRow;
    }

    ++row;
    /*
    if(clickedRow == row)     // die Zeile mit der Art des Feedback
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_FEEDBACK);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        switch(chooseRow)
        {
        case 0:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("A"));
            break;
        case 1:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("B"));
            break;
        case 2:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("Mean A,B"));
            break;
        default:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("invalid"));
            break;
        }
        myAxes.Axis[column].feedbackType = chooseRow;
    }
    */

    ++row;
    if(clickedRow == row)     // die elfte Zeile mit der Gantry Auswahl
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_GANTRY);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        if(chooseRow == 0 || chooseRow == column+1)     // kein Gantry mit sich selbst!
        {
            //ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("none"));
            myAxes.Axis[column].isGantry = 0;
            this->InitContent();    // die Sternchen wieder wegmachen
        }
        else
        {           
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Axis %1").arg(chooseRow)));

            myAxes.Axis[column].isGantry = chooseRow;           
            for(int i=2; i<=8; ++i)
                ui->tableWidget_Axis->setItem(i, column,  new QTableWidgetItem("*"));
            for(int i=10; i<=20; ++i)
                ui->tableWidget_Axis->setItem(i, column,  new QTableWidgetItem("*"));
        }
    }

    ++row;
    if(clickedRow == row)     // Axis Type
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_AXISTYPE);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück
        chooseRow += 2;     // nur Typ 2 und 3, Typ 1 ist für Zeiss 454
        myAxes.Axis[column].axisType = chooseRow;
        ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("Type %1").arg(chooseRow)));
    }

    ++row;
    if(clickedRow == row)     // Is Stabilized
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_YESNO);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück
        if(chooseRow == 0)
        {
            myAxes.Axis[column].isStabilized = false;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("no"));
        }
        else
        {
            myAxes.Axis[column].isStabilized = true;
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem("yes"));
        }
    }

    ++row;
    if(clickedRow == row)
    {
        dWert = myAxes.Axis[column].CL_Approach;
        dWert = QInputDialog::getDouble(this, "Axis " + QString("%1").arg(column+1) , "Input Approach Factor:", dWert, 0, 2, 3, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myAxes.Axis[column].CL_Approach = dWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        dWert = myAxes.Axis[column].CL_DeltaMove;
        dWert = QInputDialog::getDouble(this, "Axis " + QString("%1").arg(column+1) , "Input Delta Move in Units:", dWert, 0, 10000, 6, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myAxes.Axis[column].CL_DeltaMove = dWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        iWert = myAxes.Axis[column].CL_WaitMove;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Wait Move in ms:", iWert, 0, 10000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].CL_WaitMove = iWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        dWert = myAxes.Axis[column].CL_DeltaStab;
        dWert = QInputDialog::getDouble(this, "Axis " + QString("%1").arg(column+1) , "Input Delta Stab in Units:", dWert, 0, 10000, 6, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myAxes.Axis[column].CL_DeltaStab = dWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        iWert = myAxes.Axis[column].CL_WaitStab;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Wait Stab in ms:", iWert, 1, 100000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].CL_WaitStab = iWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        iWert = myAxes.Axis[column].CL_Average;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Number of Averages:", iWert, 1, 5000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].CL_Average = iWert;
        }
    }

    ++row;
    if(clickedRow == row)
    {
        dWert = myAxes.Axis[column].CL_DeltaFollow;
        dWert = QInputDialog::getDouble(this, "Axis " + QString("%1").arg(column+1) , "Input Delta Follow in Units:", dWert, 0, 100, 2, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myAxes.Axis[column].CL_DeltaFollow = dWert;
        }
    }

    ++row;
    if(clickedRow == row)     // die Zeile mit der Fahrrichtung für die Initialisierung
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_INITDIRECTION);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        switch(chooseRow)
        {
        case 0:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("Reverse"));
            myAxes.Axis[column].initDirection = -1;
            break;
        case 1:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("Forward"));
            myAxes.Axis[column].initDirection = 1;
            break;
        default:
            ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("invalid"));
            break;
        }
    }

    ++row;
    if(clickedRow == row)   // die Steps fürs rausfahren nach init
    {
        iWert = myAxes.Axis[column].initSteps;
        iWert = QInputDialog::getInt(this, "Axis " + QString("%1").arg(column+1) , "Input Number of Steps to drive free after Init", iWert, 0, 10000000, 1, &ok);
        if(ok) {
            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(iWert)));
            myAxes.Axis[column].initSteps = iWert;
        }
    }


    ++row;  // Leerzeile

    if(myAxes.Axis[column].axisDriverType == 1 ) // Auswahl nur für die Achsen des ECM-713 anzeigen
    {
        ++row;
        if(clickedRow == row)     // Wahl der microSteps
        {
            ecChooseList *chooseList = new ecChooseList(CLIST_MICROSTEPS);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(1 << (chooseRow + 2))));

            myAxes.Axis[column].microSteps = chooseRow + 2;
        }

        ++row;
        if(clickedRow == row)     // Wahl des Laufstroms
        {
            ecChooseList *chooseList = new ecChooseList(CLIST_AXISCURRENT);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(0.1 * (float)chooseRow, 0, 'f', 1)));
            myAxes.Axis[column].motorCurrentRun = chooseRow * 5;
        }

        ++row;
        if(clickedRow == row)     // Wahl des Haltestroms
        {
            ecChooseList *chooseList = new ecChooseList(CLIST_AXISCURRENT);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

            ui->tableWidget_Axis->setItem(row, column, new QTableWidgetItem(QString("%1").arg(0.1 * (float)chooseRow, 0, 'f', 1)));
            myAxes.Axis[column].motorCurrentStop = chooseRow * 5;
        }

        ++row;
        ++row;
        if(clickedRow == row)     // Wahl der Vorzugsdrehrichtung des Motors
        {
            ecChooseList *chooseList = new ecChooseList(CLIST_MOTORDIRECTION);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück
            if(chooseRow == 0)
            {
                myAxes.Axis[column].motor_direction = 1;
                ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("Positive"));
            }
            else
            {
                myAxes.Axis[column].motor_direction = -1;
                ui->tableWidget_Axis->setItem(clickedRow, column,  new QTableWidgetItem("Negative"));
            }

        }
    }

    //ui->tableWidget->setCurrentCell(row, column);
    emit axisParameterChanged();   // Signal für Update des Hauptfensters senden

}


//
//          Hier nur die dynamischen Auswahllisten
//

ecChooseList::ecChooseList(int select, QWidget *parent) :
    QDialog(parent), ui(new Ui::ecChooseList)
{
    ui->setupUi(this);

    switch(select)
    {
    case CLIST_UNIT:
        ui->listWidget_Choose->addItem(UNIT_1);
        ui->listWidget_Choose->addItem(UNIT_2);
        ui->listWidget_Choose->addItem(UNIT_3);
        //ui->listWidget_Choose->setCurrentRow();
        break;
    case CLIST_ROTATIONTYPE:
        ui->listWidget_Choose->addItem("Lin");
        ui->listWidget_Choose->addItem("R180");
        ui->listWidget_Choose->addItem("R200");
        ui->listWidget_Choose->addItem("Gantry");
        break;
    case CLIST_AXISTYPE:
        ui->listWidget_Choose->setMinimumWidth(220);
        this->adjustSize();     // vergrößert den Dialog passend zum Inhalt (der Listenbreite)
        ui->listWidget_Choose->addItem("Type 2: normal");
        ui->listWidget_Choose->addItem("Type 3: only Delta Steps");
        break;
    case CLIST_YESNO:
        ui->listWidget_Choose->addItem("no");
        ui->listWidget_Choose->addItem("yes");
        break;
    case CLIST_ENCODER:
        ui->listWidget_Choose->setMinimumWidth(280);
        ui->listWidget_Choose->setMinimumHeight(220);
        this->adjustSize();
        ui->listWidget_Choose->addItem("none");    // 0 = wird nicht verwendet
        for(int i=0; i< myEncoderChannels.numEncoder; i++)
        {
            switch(myEncoderChannels.Encoder[i].encoderType)
            {
            case 1:
                sprintf(cstring, "Encoder %2d   (ECM-701  4-ch absolute)", i+1);
                break;
            case 2:
                sprintf(cstring, "Encoder %2d   (ECM-713  1-ch absolute)", i+1);
                break;
            case 3:
                sprintf(cstring, "Encoder %2d   (ECM-702  2-ch incremental)", i+1);
                break;
            default:
                sprintf(cstring, "Encoder %2d   (undefined)", i+1);
                break;

            }
            ui->listWidget_Choose->addItem(cstring);
        }
        break;
    case CLIST_FEEDBACK:
        ui->listWidget_Choose->addItem("A");
        ui->listWidget_Choose->addItem("B");
        ui->listWidget_Choose->addItem("Mean A,B");
        break;
    case CLIST_GANTRY:
        ui->listWidget_Choose->setMinimumHeight(220);
        this->adjustSize();
        ui->listWidget_Choose->addItem("none");    // 0 = wird nicht verwendet
        for(int i=0; i< myAxes.numAxis; i++)
        {
            sprintf(cstring, "Axis %d", i+1);
            ui->listWidget_Choose->addItem(cstring);
        }
        break;
    case CLIST_INITDIRECTION:
        ui->listWidget_Choose->setMinimumWidth(220);
        this->adjustSize();     // vergrößert den Dialog passend zum Inhalt (der Listenbreite)
        ui->listWidget_Choose->addItem("Reverse");
        ui->listWidget_Choose->addItem("Forward");
        break;

    case CLIST_MICROSTEPS:
        ui->listWidget_Choose->setMinimumHeight(220);
        this->adjustSize();
        for(int i=2; i <= 8; i++)
        {
            sprintf(cstring, "%d", 1 << i);
            ui->listWidget_Choose->addItem(cstring);
        }
        break;

    case CLIST_AXISCURRENT:
        ui->listWidget_Choose->setMinimumHeight(220);
        this->adjustSize();
        for(int i=0; i <= 50; i++)  // 0 bis 5 Aeff, in 0,1 A Schritten
        {
            ui->listWidget_Choose->addItem(QString("%L1").arg( 0.1 * (float)i, 0, 'f', 1));
        }
        break;

    case CLIST_REFERENCING:
        ui->listWidget_Choose->setMinimumWidth(140);
        //ui->listWidget_Choose->setMinimumHeight(220);
        this->adjustSize();
        ui->listWidget_Choose->addItem("Off");
        ui->listWidget_Choose->addItem("Zero on Mark");
        ui->listWidget_Choose->addItem("Zero Static");
        break;

    case CLIST_MOTORDIRECTION:
        ui->listWidget_Choose->setMinimumWidth(140);
        this->adjustSize();
        ui->listWidget_Choose->addItem("Positive");
        ui->listWidget_Choose->addItem("Negative");
        break;

    }
}

void ecChooseList::on_listWidget_Choose_itemClicked()
{
    myrow = ui->listWidget_Choose->currentRow();   // keine globale Variable mehr, sondern schön sauberer Return-Wert
    close();
}


int ecChooseList::exec()
{
    QDialog::exec();
    return myrow;
}


//
//          Hier sind alle GUI-Funktionen für den EtherCAT Konfigurations Dialog
//


//  Dialog zur Anzeige aller gefundenen EtherCAT Slaves
ecConfigurationDialog::ecConfigurationDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecConfigurationDialog)
{
    ui->setupUi(this);
}

void ecConfigurationDialog::reject()
{
    myDialogs.isOpen_Configuration = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}


// Initialisiert den Configuration Dialog
void ecConfigurationDialog::InitContent()
{
    // Zähler im GUI aktualisieren:
    sprintf(cstring, "%d", foundSlaves.mySlavescount);
    ui->lineEdit_Num_Total->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.OTHERcount);
    ui->lineEdit_Num_OTHER->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.ECM701count);
    ui->lineEdit_Num_ECM701->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.ECM702count);
    ui->lineEdit_Num_ECM702->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.ECM712count);
    ui->lineEdit_Num_ECM712->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.ECM713count);
    ui->lineEdit_Num_ECM713->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.A760_01_B_count);
    ui->lineEdit_Num_A760_01_B->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL1018count);
    ui->lineEdit_Num_EL1018->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL1024count);
    ui->lineEdit_Num_EL1024->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL1084count);
    ui->lineEdit_Num_EL1084->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL1104count);
    ui->lineEdit_Num_EL1104->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL2202count);
    ui->lineEdit_Num_EL2202->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL2612count);
    ui->lineEdit_Num_EL2612->setText(cstring);

    sprintf(cstring, "%d", foundSlaves.EL2622count);
    ui->lineEdit_Num_EL2622->setText(cstring);


    // die aus der Konfigurationsdatei gelesenen Werte:
    sprintf(cstring, "%d", configuredSlaves.ECM701count);
    ui->lineEdit_Num_ECM701_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.ECM702count);
    ui->lineEdit_Num_ECM702_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.ECM712count);
    ui->lineEdit_Num_ECM712_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.ECM713count);
    ui->lineEdit_Num_ECM713_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.A760_01_B_count);
    ui->lineEdit_Num_A760_01_B_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL1018count);
    ui->lineEdit_Num_EL1018_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL1024count);
    ui->lineEdit_Num_EL1024_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL1104count);
    ui->lineEdit_Num_EL1104_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL1084count);
    ui->lineEdit_Num_EL1084_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL2202count);
    ui->lineEdit_Num_EL2202_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL2612count);
    ui->lineEdit_Num_EL2612_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.EL2622count);
    ui->lineEdit_Num_EL2622_conf->setText(cstring);

    sprintf(cstring, "%d", configuredSlaves.OTHERcount);
    ui->lineEdit_Num_OTHER_conf->setText(cstring);
}



//
//          Hier sind alle GUI-Funktionen für den EtherCAT Konfigurations Dialog
//


//  Dialog zum Test der Beckhoff Module
ecBeckhoffDialog::ecBeckhoffDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecBeckhoffDialog)
{
    ui->setupUi(this);
}

void ecBeckhoffDialog::reject()
{
    myDialogs.isOpen_IO_Beckhoff = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}



// Setzt die Relais Häkchen gemäß .state und aktualisiert die Encodertabelle laufend mit den gelesenen ADC-Werten
void ecBeckhoffDialog::UpdateContent()
{
    // die Relais

    ui->checkBox_R1->setChecked(myRelais.state[0]);
    ui->checkBox_R2->setChecked(myRelais.state[1]);
    ui->checkBox_R3->setChecked(myRelais.state[2]);
    ui->checkBox_R4->setChecked(myRelais.state[3]);
    ui->checkBox_R5->setChecked(myRelais.state[4]);
    ui->checkBox_R6->setChecked(myRelais.state[5]);

    // die Inputs
    ui->radioButton_IN_1->setChecked(myInputs.state[0]);

    ui->radioButton_IN_2->setChecked(myInputs.state[1]);
    ui->radioButton_IN_3->setChecked(myInputs.state[2]);
    ui->radioButton_IN_4->setChecked(myInputs.state[3]);
    ui->radioButton_IN_5->setChecked(myInputs.state[4]);
    ui->radioButton_IN_6->setChecked(myInputs.state[5]);
    ui->radioButton_IN_7->setChecked(myInputs.state[6]);
    ui->radioButton_IN_8->setChecked(myInputs.state[7]);
    ui->radioButton_IN_9->setChecked(myInputs.state[8]);
    ui->radioButton_IN_10->setChecked(myInputs.state[9]);
}


//  Die Relais manuell schalten:
void ecBeckhoffDialog::on_checkBox_R1_clicked()
{
    myRelais.state[0] = ui->checkBox_R1->isChecked();
}

void ecBeckhoffDialog::on_checkBox_R2_clicked()
{
    myRelais.state[1] = ui->checkBox_R2->isChecked();
}

void ecBeckhoffDialog::on_checkBox_R3_clicked()
{
    myRelais.state[2] = ui->checkBox_R3->isChecked();
}

void ecBeckhoffDialog::on_checkBox_R4_clicked()
{
    myRelais.state[3] = ui->checkBox_R4->isChecked();
}

void ecBeckhoffDialog::on_checkBox_R5_clicked()
{
    myRelais.state[4] = ui->checkBox_R5->isChecked();
}

void ecBeckhoffDialog::on_checkBox_R6_clicked()
{
    myRelais.state[5] = ui->checkBox_R6->isChecked();
}


//
//          Hier sind alle GUI-Funktionen für den Encoder Parameter Dialog
//


//  Der Dialog für die Resolute Encoder
ecEncoderDialog::ecEncoderDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecEncoderDialog)
{
    ui->setupUi(this);
}

void ecEncoderDialog::reject()
{
    myDialogs.isOpen_Encoder = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}


void ecEncoderDialog::on_tableWidget_Encoder_cellDoubleClicked(int row, int column)
{
    int chooseRow;
    bool ok;
    QString text;
    double  dWert;
    //quint32 uiWert;
    qint64 qint64_Wert;

    if(row == 0)     // die erste Zeile mit den symbolischen Namen der Encoder
    {       
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input new encoder name:", QLineEdit::Normal, "", &ok);
        if(ok) {
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(text));
            myEncoderDialog.encName[column] = text;
        }
    }

    if(row == 2)     // in use, ersetzt "NU"
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_YESNO);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        if(chooseRow == 0)
        {
            myEncoderChannels.Encoder[column].isUsed = false;
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem("no"));
        }
        else
        {
            myEncoderChannels.Encoder[column].isUsed = true;
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem("yes"));
        }
    }


    if(row == 3)     // die vierte Zeile mit dem Encoder Modus
    {
            ecChooseList *chooseList = new ecChooseList(CLIST_ROTATIONTYPE);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

            switch(chooseRow)
            {
            case 0:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("Lin"));
                break;
            case 1:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("R180"));
                break;
            case 2:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("R200"));
                break;
            case 3:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("Gantry"));
                break;
            default:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("invalid"));
                break;
            }
            myEncoderChannels.Encoder[column].encoderModus = chooseRow;   // impliziter Returnwert über Global
    }

    if(row == 4)     // die fünfte Zeile mit dem Encoder Maßstab
    {
        text = QString("%L1").arg(myEncoderChannels.Encoder[column].units_per_count);
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input Encoder Scale in Units/Count:", QLineEdit::Normal, text, &ok);
        if(ok) {
            dWert = myLocale.toDouble(text);
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myEncoderChannels.Encoder[column].units_per_count = dWert;
        }
    }

    if(row == 5)   // Offset für Encoder
    {
        text = QString("%L1").arg(myEncoderChannels.Encoder[column].offsetCounts);
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input Offset in Counts:", QLineEdit::Normal, text, &ok);
        if(ok) {
            qint64_Wert = text.toLongLong();
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(qint64_Wert)));
            myEncoderChannels.Encoder[column].offsetCounts = qint64_Wert;
        }
    }


    if(row == 6)   // Limit Low für Encoder
    {
        text = QString("%L1").arg(myEncoderChannels.Encoder[column].limitLow);
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input Lower Limit in Counts:", QLineEdit::Normal, text, &ok);
        if(ok) {
            qint64_Wert = text.toLongLong();
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(qint64_Wert)));
            myEncoderChannels.Encoder[column].limitLow = qint64_Wert;
        }
    }


    if(row == 7)   // Limit High für Encoder
    {
        text = QString("%L1").arg(myEncoderChannels.Encoder[column].limitHigh);
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input Higher Limit in Counts:", QLineEdit::Normal, text, &ok);
        if(ok) {
            qint64_Wert = text.toLongLong();
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(qint64_Wert)));
            myEncoderChannels.Encoder[column].limitHigh = qint64_Wert;
        }
    }

    if(row == 9)   // Offset in Units (Grad) für Encoder von Drehachsen
    {
        text = QString("%L1").arg(myEncoderChannels.Encoder[column].offsetUnits);
        text = QInputDialog::getText(this, "Encoder " + QString("%L1").arg(column+1) , "Input Offset in Units:", QLineEdit::Normal, text, &ok);
        if(ok) {
            dWert = myLocale.toDouble(text);
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem(QString("%L1").arg(dWert)));
            myEncoderChannels.Encoder[column].offsetUnits = dWert;
        }
    }

    if(row == 11)   // Offset in Units (Grad) für Encoder von Drehachsen
    {
        int reply = QMessageBox::question(this, "Pseudo-Init", QString("Do you really want to set all Tonic Encoders to be initialized?"), QMessageBox::Yes|QMessageBox::No);

        if (reply == QMessageBox::Yes)
        {
            for(int i = 0; i < myEncoderChannels.numEncoder; ++i)
                myEncoderChannels.Encoder[i].encState = ENCSTATE_OK;
        }
    }

    if(row == 13)   // Control Byte?? für ECM-702 Tonic
    {
        if(myEncoderChannels.Encoder[column].encoderType == 3)  // nur für inkrementelle Encoder
        {
            ecChooseList *chooseList = new ecChooseList(CLIST_REFERENCING);
            chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

            switch(chooseRow)
            {
            case 0:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("Off"));
                break;
            case 1:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("Zero on Mark"));
                break;
            case 2:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("Zero Static"));
                //if(myEncoderChannels.Encoder[column].encState == ENCSTATE_ZEROLOST)     // einmalig den Zero Fehler löschen
                    myEncoderChannels.Encoder[column].encState = ENCSTATE_OK;
                break;
            default:
                ui->tableWidget_Encoder->setItem(row, column,  new QTableWidgetItem("invalid"));
                break;
            }
            myEncoderChannels.Encoder[column].referencing = chooseRow;
        }
    }

    if(row == 14)     // soll dieser Encoder-Kanal geloggt werden?
    {
        ecChooseList *chooseList = new ecChooseList(CLIST_YESNO);
        chooseRow = chooseList->exec();   // exec startet eigene Event-Schleife und kehrt erst beim Schliessen der Liste zurück

        if(chooseRow == 0)
        {
            myEncoderChannels.Encoder[column].doLogging = false;
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem("no"));
        }
        else
        {
            myEncoderChannels.Encoder[column].doLogging = true;
            ui->tableWidget_Encoder->setItem(row, column, new QTableWidgetItem("yes"));
        }
    }
}


// Initialisiert die Encodertabelle mit Inhalt aus den Parametern
void ecEncoderDialog::InitContent()
{
    int column; // index, row;
    QStringList header;

    // hier die Encodertabelle:

    ui->tableWidget_Encoder->setColumnCount(myEncoderChannels.numEncoder);
    ui->tableWidget_Encoder->setRowCount(15);   // 14=controlByte,  15=Logging

    header.clear();
    header << "Name" << "Type" << "In Use" << "Modus" << "Scale (Units/Count)" << "Offset (Counts)" << "Limit Low (Counts)" << "Limit High (Counts)" << "Counts" << "Offset (Units)" << "Position" << "Status" << "Revision" << "Referencing" << "Logging";
    ui->tableWidget_Encoder->setVerticalHeaderLabels(header);

    for(column = 0; column < myEncoderChannels.numEncoder; column++)
    {
        sprintf(cstring, "Encoder %2d", column+1);
        ui->tableWidget_Encoder->setHorizontalHeaderItem(column, new QTableWidgetItem(tr(cstring)));

        ui->tableWidget_Encoder->setItem(0, column, new QTableWidgetItem(myEncoderDialog.encName[column]));

        switch(myEncoderChannels.Encoder[column].encoderType)
        {
        case ENCTYPE_ECM_701:
            ui->tableWidget_Encoder->setItem(1, column,  new QTableWidgetItem("absolute-701"));     // die Resolute Kanäle
            break;
        case ENCTYPE_ECM_713:
            ui->tableWidget_Encoder->setItem(1, column,  new QTableWidgetItem("absolute-713"));     // auch Resolute
            break;
        case ENCTYPE_ECM_702:
            ui->tableWidget_Encoder->setItem(1, column,  new QTableWidgetItem("incremental"));      // die Tonics
            break;
        default:
            ui->tableWidget_Encoder->setItem(1, column,  new QTableWidgetItem("invalid"));
            break;
        }

        if(myEncoderChannels.Encoder[column].isUsed)
            ui->tableWidget_Encoder->setItem(2, column, new QTableWidgetItem("yes"));
        else
            ui->tableWidget_Encoder->setItem(2, column, new QTableWidgetItem("no"));

        switch(myEncoderChannels.Encoder[column].encoderModus)
        {
        case 0:
            ui->tableWidget_Encoder->setItem(3, column,  new QTableWidgetItem("Lin"));
            break;
        case 1:
            ui->tableWidget_Encoder->setItem(3, column,  new QTableWidgetItem("R180"));
            break;
        case 2:
            ui->tableWidget_Encoder->setItem(3, column,  new QTableWidgetItem("R200"));
            break;
        case 3:
            ui->tableWidget_Encoder->setItem(3, column,  new QTableWidgetItem("Gantry"));
            break;
        default:
            ui->tableWidget_Encoder->setItem(3, column,  new QTableWidgetItem("invalid"));
            break;
        }

        ui->tableWidget_Encoder->setItem(4, column, new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].units_per_count)));

        ui->tableWidget_Encoder->setItem(5, column, new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].offsetCounts)));

        ui->tableWidget_Encoder->setItem(6, column, new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].limitLow)));

        ui->tableWidget_Encoder->setItem(7, column, new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].limitHigh)));

        ui->tableWidget_Encoder->setItem(9, column, new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].offsetUnits)));

        if(myEncoderChannels.Encoder[column].encoderType == 3)  // nur für inkrementelle Encoder
        {
            switch(myEncoderChannels.Encoder[column].referencing)
            {
            case 0:
                ui->tableWidget_Encoder->setItem(13, column,  new QTableWidgetItem("Off"));
                break;
            case 1:
                ui->tableWidget_Encoder->setItem(13, column,  new QTableWidgetItem("Zero on Mark"));
                break;
            case 2:
                ui->tableWidget_Encoder->setItem(13, column,  new QTableWidgetItem("Zero Static"));
                break;
            default:
                ui->tableWidget_Encoder->setItem(13, column,  new QTableWidgetItem("invalid"));
                break;
            }
        }

        if(myEncoderChannels.Encoder[column].doLogging)
            ui->tableWidget_Encoder->setItem(14, column, new QTableWidgetItem("yes"));
        else
            ui->tableWidget_Encoder->setItem(14, column, new QTableWidgetItem("no"));

    }
}



// Aktualisiert die Encodertabelle laufend mit den gelesenen Counts und den anderen sich dynamisch ändernden Inhalten
void ecEncoderDialog::UpdateContent()
{
    QString status;
    QTableWidgetItem *item;

        for(int column = 0; column < myEncoderChannels.numEncoder; ++column)
        {
            item = new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].counts));
            ui->tableWidget_Encoder->setItem(8, column, item);

            item = new QTableWidgetItem(QString("%L1").arg(myEncoderChannels.Encoder[column].positionUnits, 0, 'f', 6));
            ui->tableWidget_Encoder->setItem(10, column, item);

            status = "Invalid";     // sollte nie kommen
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_OK)
                status = "Ok";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_WARN)
                status = "Warning";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_LIMIT)
                status = "Limit";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_ZEROLOST)
                status = "ZeroLost";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_READ_ERROR)
                status = "Read Error";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_CRC_ERROR)
                status = "CRC Error";
            if(myEncoderChannels.Encoder[column].encState == ENCSTATE_NO_CONNECT)
                status = "No Connect";
            item = new QTableWidgetItem(status);
            ui->tableWidget_Encoder->setItem(11, column, item);

            if(myEncoderChannels.Encoder[column].revision != 0)
            {
                item = new QTableWidgetItem(QString("%1").arg(0.1 * myEncoderChannels.Encoder[column].revision, 0, 'f', 1));
                ui->tableWidget_Encoder->setItem(12, column, item);
            }
        }

}

void ecMainDialog::on_pushButton_userLogin_clicked()
{
    if(userLevel == USERLEVELVIEWER)
    {
        QString         passwd, hashString;
        QByteArray      data;
        QFile           userfile;
        QInputDialog    inputDialog;

        //inputDialog.setWindowFlags(inputDialog.windowFlags() & (~Qt::WindowContextHelpButtonHint));

        passwd = inputDialog.getText(this, "Authorization" , "Input Password:", QLineEdit::Password, "");
        passwd += "ecToolExpert";       // some salt
        data = passwd.toLatin1();

        QCryptographicHash hashF(QCryptographicHash::Md5);
        hashF.addData(data);
        data = hashF.result();
        hashF.reset();
        passwd = QString::fromLatin1(data.toHex());     // jetzt der Hash als Text

        QString path = QDir::homePath() + QString(USERHASHPATH);    // Das verschlüsselte Passwort
        userfile.setFileName(path);
        hashString = "";
        if(userfile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            hashString = QString::fromLatin1(userfile.readAll());
            hashString = hashString.mid(0, 32);
            userfile.close();
        }

        if(hashString == passwd)    // Authentifizierung erfolgreich
        {
            // Einloggen:
            userLevel = USERLEVELEXPERT;
            setUserLevel(userLevel);
            ui->pushButton_userLogin->setText("Logout");
        }
        else
        {
            QMessageBox::critical(NULL, "Invalid", "Wrong Password!",  QMessageBox::Ok);
        }
    }
    else
    {
        // Ausloggen:
        userLevel = USERLEVELVIEWER;
        setUserLevel(userLevel);
        ui->pushButton_userLogin->setText("Login");
    }
}

void ecMainDialog::setUserLevel(int userLevel)
{
    bool    enable;

    switch(userLevel)
    {
    case USERLEVELVIEWER:
        this->setWindowTitle(MAIN_WINDOW_TITLE + check + USERTEXTVIEWER);
        if(myDialogs.isOpen_Axis)
        {
            axisDialog->close();     // damit der Experte das nicht vergessen kann
            //myDialogs.isOpenAxis = false; // dies erfolgt schon in ecAxisDialog::reject()
        }
        if(myDialogs.isOpen_Encoder)
        {
            encoderDialog->close();     // damit der Experte das nicht vergessen kann
        }
        if(myDialogs.isOpen_IO_Beckhoff)
        {
            beckhoffDialog->close();
        }
        if(myDialogs.isOpen_Hexapod)
        {
            hexapodDialog->close();
        }
        if(myDialogs.isOpen_MoveSequence)
        {
            movesequenceDialog->close();     // damit der Experte das nicht vergessen kann
        }
        if(myDialogs.isOpen_OTF_Tonic)
        {
            otfTonicDialog->close();
        }
        if(myDialogs.isOpen_SourceControl)
        {
            sourceControlDialog->close();
        }

        enable = false;
        break;

    case USERLEVELEXPERT:
        this->setWindowTitle(MAIN_WINDOW_TITLE + check + USERTEXTEXPERT);
        enable = true;
        break;
    }

    ui->pushButton_LoadConfiguration->setEnabled(enable);
    ui->pushButton_SaveConfiguration->setEnabled(enable);
    ui->pushButton_ecRestart->setEnabled(enable);

    ui->pushButton_userChangePWD->setEnabled(enable);

    ui->pushButton_OpenAxisConfiguration->setEnabled(enable);
    ui->pushButton_OpenEncoderConfiguration->setEnabled(enable);

    ui->pushButton_Init->setEnabled(enable);

    ui->pushButton_MoveTo_Steps->setEnabled(enable);
    ui->pushButton_JogMinus_Steps->setEnabled(enable);
    ui->pushButton_JogPlus_Steps->setEnabled(enable);

    ui->pushButton_MoveTo_Position->setEnabled(enable);
    ui->pushButton_JogMinus_Position->setEnabled(enable);
    ui->pushButton_JogPlus_Position->setEnabled(enable);

    ui->pushButton_SaveLastMove->setEnabled(enable);
    ui->pushButton_StartLogging->setEnabled(enable);
    ui->pushButton_GetRecord->setEnabled(enable);

    ui->pushButton_Extend->setEnabled(enable);

    ui->pushButton_FollowingError_Reset->setEnabled(enable);

    ui->checkBox_IgnoreLimits->setEnabled(enable);
    ui->checkBox_Stabilize->setEnabled(enable);
    ui->checkBox_GlobalStab->setEnabled(enable);

    ui->lineEdit_DeltaSteps->setEnabled(enable);
    ui->lineEdit_DeltaPosition->setEnabled(enable);
    ui->lineEdit_Steps_Aim->setEnabled(enable);
    ui->lineEdit_Position_Aim->setEnabled(enable);
    ui->lineEdit_Tail->setEnabled(enable);

    //ui->listWidget_OTF_Axis->setEnabled(enable);


    // soll der auch deaktiviert sein??
    //ui->pushButton_Stop->setEnabled(enable);
}

void ecMainDialog::on_pushButton_userChangePWD_clicked()
{
    QString         passwd, hashString;
    QByteArray      data;
    QFile           userfile;
    QInputDialog    inputDialog;

    passwd = inputDialog.getText(this, "Authorization" , "Input old Password:", QLineEdit::Password, "");
    passwd += "ecToolExpert";       // some salt
    data = passwd.toLatin1();

    QCryptographicHash hashF(QCryptographicHash::Md5);
    hashF.addData(data);
    data = hashF.result();
    hashF.reset();
    passwd = QString::fromLatin1(data.toHex());     // jetzt der Hash als Text

    QString path = QDir::homePath() + QString(USERHASHPATH);    // Das verschlüsselte Passwort
    userfile.setFileName(path);
    if(userfile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        hashString = QString::fromLatin1(userfile.readAll());
        hashString = hashString.mid(0, 32);
        userfile.close();
    }

    if(hashString == passwd)    // Authentifizierung mit altem Passwort erfolgreich
    {
        passwd = inputDialog.getText(this, "Authorization" , "Input new Password:", QLineEdit::Password, "");
        passwd += "ecToolExpert";       // some salt
        data = passwd.toLatin1();

        hashF.addData(data);
        data = hashF.result();
        hashF.reset();

        QString path = QDir::homePath() + QString(USERHASHPATH);    // Das verschlüsselte Passwort wird überschrieben
        userfile.setFileName(path);
        if(userfile.open(QIODevice::WriteOnly | QIODevice::Text))
        {
            userfile.write(data.toHex());
            userfile.close();
        }
    }
    else
    {
        QMessageBox::critical(NULL, "Invalid", "Wrong Password!",  QMessageBox::Ok);
    }
}

void ecMainDialog::on_pushButton_GetRecord_clicked()
{

}

void ecMainDialog::on_pushButton_Extend_clicked()
{
    if(myDialogs.extOpen_IO_Beckhoff)
    {
        if(!myDialogs.isOpen_IO_Beckhoff)                   // doppelt geöffnete Fenster verhindern
        {
            beckhoffDialog->setWindowFlags(Qt::Dialog);     // Damit kommt der Minimize Button in der Titelzeiel
            beckhoffDialog->open();
            //beckhoffDialog->setWindowFlags(beckhoffDialog->windowFlags() | Qt::WindowMinimizeButtonHint);
            beckhoffDialog->setWindowTitle("Beckhoff IO Modules");
            myDialogs.isOpen_IO_Beckhoff = true;
        }
        else
        {
            //beckhoffDialog->raise();    // schon offenes Fenster in den Vordergrund bringen
            beckhoffDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
        }
    }

    if(myDialogs.extOpen_Hexapod)
    {
        if(!myDialogs.isOpen_Hexapod)    // doppelt geöffnete Fenster verhindern
        {
            hexapodDialog->setWindowFlags(Qt::Dialog);
            hexapodDialog->open();
            hexapodDialog->setWindowTitle("Move Hexapod in DOFs");
            hexapodDialog->SetContent();
            myDialogs.isOpen_Hexapod = true;
        }
        else
        {
            hexapodDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
        }
    }

    if(myDialogs.extOpen_MoveSequence)
    {
        if(!myDialogs.isOpen_MoveSequence)    // doppelt geöffnete Fenster verhindern
        {
            movesequenceDialog->setWindowFlags(Qt::Dialog);
            movesequenceDialog->open();
            movesequenceDialog->setWindowTitle("Configure Automatic Move Sequences");
            movesequenceDialog->InitContent(myDialogs.extOpen_Hexapod);     // die DOFs nur anbieten, wenn ein Hexapod verwendet wird
            myDialogs.isOpen_MoveSequence = true;
        }
        else
        {
            movesequenceDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
        }
    }

    if(myDialogs.extOpen_OTF_Tonic)
    {
        if(!myDialogs.isOpen_OTF_Tonic)    // doppelt geöffnete Fenster verhindern
        {
            otfTonicDialog->setWindowFlags(Qt::Dialog);
            otfTonicDialog->open();
            otfTonicDialog->setWindowTitle("Configure OTF Capture");
            otfTonicDialog->InitContent();
            myDialogs.isOpen_OTF_Tonic = true;
        }
        else
        {
            otfTonicDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
        }
    }

    if(myDialogs.extOpen_SourceControl)
    {
        if(!myDialogs.isOpen_SourceControl)    // doppelt geöffnete Fenster verhindern
        {
            //sourceControlDialog->setWindowFlags(Qt::Window);  mit dieser Einstellung wird der Dialog immer vom Hauptfenster verdeckt
            sourceControlDialog->setWindowFlags(Qt::Dialog);    // Juhu, mit Qt::Dialog das gewünschte Verhalten, immer das zuletzt angeklickte Fenster im Vordergrund
            sourceControlDialog->open();
            sourceControlDialog->setWindowTitle("Source Control");
            sourceControlDialog->InitContent();
            myDialogs.isOpen_SourceControl = true;
        }
        else
        {
            sourceControlDialog->activateWindow();   // bringt das Fenster nach vorn und setzt den Fokus darauf
        }
    }

}



//
//   Dialog für die OTF-Funktion mit inkrementellen Encodern
//
ecOtfTonicDialog::ecOtfTonicDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecOtfTonicDialog)
{
    ui->setupUi(this);

    // Liste zur Auswahl der OTF-Achse
    ui->listWidget_OTF_Axis->addItem("none");
    for(int i=0; i<myAxes.numAxis; ++i)
    {
        ui->listWidget_OTF_Axis->addItem(QString("%1  %2").arg(i+1, 2).arg(myAxesDialog.axisName[i]));
    }
    //ui->listWidget_OTF_Axis->setCurrentRow(0);
}

// Wechsel der aktuellen OTF-Achse
void ecOtfTonicDialog::on_listWidget_OTF_Axis_clicked(const QModelIndex &index)
{
    myAxes.otfAxisNr = ui->listWidget_OTF_Axis->currentRow();     // 0 = none
}

void ecOtfTonicDialog::InitContent()
{
    ui->listWidget_OTF_Axis->setCurrentRow(myAxes.otfAxisNr);
}

void ecOtfTonicDialog::reject()
{
    myDialogs.isOpen_OTF_Tonic = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}



/*
void ecOtfTonicDialog::UpdateContent()
{
}
*/



//
//   Dialog für die Quelle Projekt 483
//
ecSourceControlDialog::ecSourceControlDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::ecSourceControlDialog)
{
    ui->setupUi(this);
}

void ecSourceControlDialog::InitContent()
{
    ui->lineEdit_TR_No->setText(QString("%1").arg(sourceControl.TR_AxisNo));
    ui->lineEdit_TT_No->setText(QString("%1").arg(sourceControl.TT_AxisNo));
    ui->lineEdit_TC_No->setText(QString("%1").arg(sourceControl.TC_AxisNo));
    ui->lineEdit_TargetTransIncrement->setText(QString("%1").arg(sourceControl.increment_trans, 0, 'f', 3));
    ui->lineEdit_TC_Limit->setText(QString("%1").arg(sourceControl.TC_Limit, 0, 'f', 3));
    ui->lineEdit_TC_Steps->setText(QString("%1").arg(sourceControl.TC_delta_steps));
}

void ecSourceControlDialog::UpdateContent()
{
    ui->lineEdit_TR_Pos->setText(QString("%1").arg(myAxes.Axis[sourceControl.TR_AxisNo-1].positionUnitsAverage, 0, 'f', 3));
    ui->lineEdit_TT_Pos->setText(QString("%1").arg(myAxes.Axis[sourceControl.TT_AxisNo-1].positionUnitsAverage, 0, 'f', 3));
    ui->lineEdit_TC_Pos->setText(QString("%1").arg(myAxes.Axis[sourceControl.TC_AxisNo-1].positionUnitsAverage, 0, 'f', 3));

    ui->lineEdit_Q1->setText(QString("%1").arg(sourceControl.counts_dQ1, 0, 'f', 1));
    ui->lineEdit_Q2->setText(QString("%1").arg(sourceControl.counts_dQ2, 0, 'f', 1));
    ui->lineEdit_Q3->setText(QString("%1").arg(sourceControl.counts_dQ3, 0, 'f', 1));
    ui->lineEdit_Q4->setText(QString("%1").arg(sourceControl.counts_dQ4, 0, 'f', 1));

    ui->lineEdit_QX ->setText(QString("%1").arg(sourceControl.QX, 0, 'f', 3));
    ui->lineEdit_QY ->setText(QString("%1").arg(sourceControl.QY, 0, 'f', 3));

    ui->radioButton_TT_Flag->setChecked(sourceControl.TT_Flag);
    ui->radioButton_TC_Flag->setChecked(sourceControl.TC_Flag);
    ui->radioButton_T_Run_Flag->setChecked(sourceControl.processRunning);

    ui->lineEdit_ShotsCounter->setText(QString("%1").arg(sourceControl.shotsCounter));
}

void ecSourceControlDialog::reject()
{
    myDialogs.isOpen_SourceControl = false;
    QDialog::reject();      // den Dialog tatsächlich schließen
}


void writeLastLaserShotSequence()
{
    // Den Zustand des Targets nach der jeweils letzten Schusssequenz abspeichern:
    QString line = QDir::homePath() + "/Data_Motion/lastTargetState.dat";     // home/user/Data_Motion für letzte Targetposition
    QFile file(line);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&file); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben

        out << "Last State of Target\n";
        out << "Saved on " << QDateTime::currentDateTime().toString() << "\n";
        out << "\n";
        out << "[DATA]\n";
        line = QString("%L1").arg(myAxes.Axis[sourceControl.TR_AxisNo-1].positionUnits, 0, 'f', 6);
        out << "TR_POS " << line << "\n";
        line = QString("%L1").arg(myAxes.Axis[sourceControl.TT_AxisNo-1].positionUnits, 0, 'f', 6);
        out << "TT_POS " << line << "\n";
        line = QString("%L1").arg(myAxes.Axis[sourceControl.TC_AxisNo-1].positionUnits, 0, 'f', 6);
        out << "TC_POS " << line << "\n";
        line = QString("%L1").arg(sourceControl.QX);
        out << "TC_QX " << line << "\n";
        out << "[END_OF_FILE]\n";
        file.close();
    }
}

void ecSourceControlDialog::on_pushButton_Start_clicked()
{
    if(sourceControl.processRunning)     // es läuft schon eine Schussfolge
    {
        // nichts machen !
    }
    else
    {
        //((qobject_cast<ecMainDialog*>)(this->parent()))->start_Process(); dies führt leider beim Zugriff auf zu segfault

        sourceControl.shotsCounter = 0;     // zählt hoch
        sourceControl.processRunning = true;

        // Target Rotation in Bewegung setzen. Es findet *keine* Synchronization mit dem Laser statt
        // einzeln zu Fuß, was sonst MoveDeltaSteps() wäre da der ecSourceControlDialog nicht auf ecMainDialog zugreifen kann!
        qint64 deltaSteps =  FUENFMILLE;      // soviele Steps, daß praktisch unendliche Targetrotation ausgelöst wird
        int axis = sourceControl.TR_AxisNo-1;
        flags.lastMoveType = flags.STEPS;
        myAxes.Axis[axis].positionStepsTarget += deltaSteps;
        //ui->lineEdit_Steps_Aim->setText(QString::number(myAxes.Axis[axis].positionStepsTarget));
        MovePlan(axis, myAxes.Axis[axis].positionStepsTarget, 1.0);
    }
}

void ecSourceControlDialog::on_pushButton_Stop_clicked()
{
    sourceControl.processRunning = false;   // dies schließt den Shutter
    usleep(100000);                         // 0.1 Sekunden warten für sicheren Verschluß des Shutter
    StopAxis(sourceControl.TR_AxisNo-1);    // Target Rotation anhalten
}

void ecMainDialog::start_Process()          // Schusssequenz des Lasers und Protokollierung starten
{
    sourceControl.shotsCounter = 0;         // zählt hoch
    sourceControl.processRunning = true;

    // Target Rotation in Bewegung setzen. Es findet *keine* Synchronization mit dem Laser statt
    qint64 deltaSteps =  FUENFMILLE;        // soviele Steps, daß praktisch unendliche Targetrotation ausgelöst wird
    MoveDeltaSteps(sourceControl.TR_AxisNo-1, deltaSteps, 1.0);
}

void ecMainDialog::stop_Process()           // Schusssequenz des Lasers und Protokollierung anhalten
{
    sourceControl.processRunning = false;   // dies schließt den Shutter
    usleep(100000);                         // 0.1 Sekunden warten für sicheren Verschluß des Shutter
    StopAxis(sourceControl.TR_AxisNo-1);    // Target Rotation anhalten
}
