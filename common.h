//
//  Hier sind alle gemeinsamen defines und die Typendeklaration der globalen Variablen sowie die Deklaration der gemeinsam genutzten Funktionen
//

#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdint.h>             // enthält die Definition von uint32_t etc.

//
//      die Konstanten
//

// Achtung, alle C Arrays fangen mit 0 an, die Zählung der Slaves am eCAT Bus hingegen mit 1 !

#define MAXSLAVENUMBER 100      // maximale Länge der Liste gefundener Slaves je Typ
#define MAXAXISNUMBER 40        // maximale Anzahl der Achsen, je 4 pro Indexer-Modul, hier auf 10 Module begrenzt, sonst wird mit dem lastMove Array die 32-bit Adressierung gesprengt
#define MAXENCODERNUMBER 400    // maximal mögliche Zahl der Encoder-Kanäle, je 4 pro Modul
#define MAXRELAISNUMBER 100     // bis auf den Laser Trigger sind alle digitalen Ausgänge Relais
#define MAXINPUTNUMBER 200      // alle Digital Ins
#define MAXPATHPOINTS 300000    // 300.000 Zyklen gleich 300 Sekunden = 5 min Speicher für einen Move

#define MAXOTFSAMPLES 100000    // 100.000 Sample = Trigger-Events während die OTF-Führungsachse fährt

#define MAXITERATIONS 10        // die maximale Anzahl von Submoves im Closed Loop Modus
#define MAXAVERAGE 1000         // maximal 1 Sekunde

#define AVERAGEENCODER 100      // Anzahl der zu mittelnden Counts fürs Logging, entspricht 100 ms

#define MAXNUMSWITCHES 20       // maximale Anzahl an konfigurierbaren Kollisionsschaltern

#define ZWEI_E32_M1 4294967295  // numerische Konstante 2 hoch 32 - 1, wenn alle Encoderbits High sind, wird zur Zeit nicht genutzt
#define ZWEI_E31    2147483648  // numerische Konstante 2 hoch 31, halber REXA Ring (= 180 Grad)
#define ZWEI_E32    4294967296  // numerische Konstante 2 hoch 32, ganzer REXA Ring (= 360 Grad)
#define EIN_GRAD      11930465  // numerische Konstante = Winkel von 1 Grad in Counts des REXA Ring


#define FUENFMILLE   500000000  // für Init Fahrt in den Limit

// globaler Summenfehler statusMotionGlobal, für alle Achsen und Encoder gemeinsam:
// für Fehler -1 bis -4 ist statusMotionArg der beftreffende Encoder, für -5 und -6 die Achse
#define GSTATUS_ISMOVING          1         // mindestens eine Achse fährt, sonst alles OK
#define GSTATUS_IDLE              0         // Grundzustand, keine Fehler, alle Achsen ruhen, warte auf Bewegungsbefehle
#define GSTATUS_ELH              -1         // Encoder Limit High (nur bei Gantry)
#define GSTATUS_ELL              -2         // Encoder Limit Low
#define GSTATUS_ENCODER_ZEROLOST -3         // ein Tonic hat die Referenzierung verloren
#define GSTATUS_ENCODER_ERROR    -4         // CRC-Fehler, Encoder Störung, kein Signal oder Encoder nicht angeschlossen
#define GSTATUS_FLIMIT           -5         // Forward Limit Switch
#define GSTATUS_RLIMIT           -6         // Reverse Limit Switch
#define GSTATUS_FOLLOWING_ERROR  -7         // eine Achse blockiert / läuft nicht korrekt mit
#define GSTATUS_TOPOLOGY_ERROR   -9         // Tatsächliche EtherCAT Konfiguration paßt nicht zu der konfigurierten aus der .cfg-Datei
#define GSTATUS_ECAT_ERROR      -10

// Zustand der einzelnen Encoder-Kanäle:
#define ENCSTATE_OK           0     // der betreffende Encoder-Kanal arbeitet einwandfrei, die counts und damit die Position sind gültig
#define ENCSTATE_WARN        -1     // schwaches Signal, aber noch gültig - nur für die Resolute
#define ENCSTATE_LIMIT       -2     // Soft Limit überschritten, zB. Deichsel schief - nur für die Resolute
#define ENCSTATE_ZEROLOST    -3     // die Referenz zur Nullmarke stimmt nicht mehr, counts sind ungültig - nur für Tonic
#define ENCSTATE_READ_ERROR  -4     // kein Signal, Störung zwischen Lesekopf und Maßstab (bei Tonic auch gar nicht angeschlossen), counts sind ungültig
#define ENCSTATE_CRC_ERROR   -5     // Lesekopf nicht angeschlossen oder Kabel / Datentelegramm gestört - nur für Resolute
#define ENCSTATE_NO_CONNECT  -6     // Lesekopf nicht angeschlossen - nur für Resolute

// die unterschiedlichen EncoderModi:
#define ENCMODUS_LIN         0       // normaler Modus für die Resolute, der einzige Modus für die Tonics ?
#define ENCMODUS_R180        1       // RESA-Ring plus-minus 180 Grad
#define ENCMODUS_R200        2       // RESA-Ring plus-minus 200 Grad maximal, mit bis zu 40 Grad Überlappung
#define ENCMODUS_GANTRY      3       // Überschreiten beider Encoderlimits führt zu Stop, sonst wie Forward und Reverse Limit gehandhabt

// die Bits im Status vom Tonic
#define ZERO_BIT            1       // Referenzmarke wurde überfahren
#define ALARM_BIT           2       // kein Signal
#define TRIGGER_BIT         4       // externer Trigger am ECM-702

#define AXISTYPE_NORMAL     2       // alle Achsen die Position können (ob mit oder ohne Encoder)
#define AXISTYPE_DELTASTEPS 3       // für Borys zu 3 gemacht, Typ 1 gibt es in ecTool nicht: nur joggen möglich

// Die Art des Encoders:
#define ENCTYPE_ECM_701     1
#define ENCTYPE_ECM_713     2
#define ENCTYPE_ECM_702     3

//
//      die Typdefinitionen für Variablen
//

// alle internen und externen Parameter für Laser, Target und On-the-Fly
typedef struct
{
    // Shutter Control
    int     shotsCounter = 0;           // "zählt" die Laserpulse hoch, ist Fake, ein reiner Timer
    int     intervalCounter = 0;        // Hilfszähler für shotsCounter
    bool    processRunning = false;     // true = Prozess läuft, TR legt los, ab Cruising Phase geht der Shutter auf - läufzt endlos bis manuell gestoppt wird
    bool    shutterState = false;       // wenn true dann High am BNC Output
    int     laserPulseRate = 100;       // Schussfrequenz des lasers in Hz - Konstante
    //bool    laserShooting = false;      // Benachrichtigung für QTimer Routine, daß geschossen wurde

    // Target
    int     TR_AxisNo = 1;              // Indices der Achsen für die Target-Bewegung, wird aus Default.cfg überschrieben
    int     TT_AxisNo = 2;
    int     TC_AxisNo = 3;
    //double  increment_rot = 0.1;        // Abstand zwischen den Kratern auf der Targetoberfläche in Einheiten der Tragetrotation - ist implizit die Geschwindigkeit der Drehachse
    double  increment_trans = 0.1;      // Abstand zwischen den Kratern auf der Targetoberfläche in Einheiten der Tragettranslation
    bool    TT_Flag = false;            // markiert den Wechsel in die nächste Spur
    double  TT_Range_low = 0.5;         // untere Grenze für nutzbaren Targetbereich
    double  TT_Range_high = 68.0;       // obere Grenze

    // Automatische Target Korrektur
    bool    TC_Flag = false;            // Target Korrektur fährt
    bool    TC_Active = false;          // wenn true wird auf den Spalt stabilisiert
    int     TC_delta_steps = 500;       // die Korrekturbewegung in Steps mit Vorzeichen, direkt aus der Konfiguration gelesen
    int     TC_delta_steps_temp = 0;    // interne temporäre Variable für die Ausführung der Bewegung
    int     A_Quad_1, A_Quad_2;         // Index der Quadranten für Summe "A"
    int     B_Quad_1, B_Quad_2;         // Index der Quadranten für Summe "A"
    double  TC_Limit;                   // Grenze ab der eine Korrekturbewegung erfolgt, in Einheiten von Counts der Differenz D = A - B

    // interne Variablen Pinhole Kamera
    int     counts_Q1;                  // die Counts vom Ladungsverstärker für die 4-Quadranten-Diode
    int     counts_Q2;
    int     counts_Q3;
    int     counts_Q4;
    int     counts_Q1A[100];                  // für die gleitenden Mittelwerte
    int     counts_Q2A[100];
    int     counts_Q3A[100];
    int     counts_Q4A[100];
    double  counts_dQ1;                  // die Counts vom Ladungsverstärker für die 4-Quadranten-Diode nach der Mittelung
    double  counts_dQ2;
    double  counts_dQ3;
    double  counts_dQ4;
    double  QX, QY;                     // jeweils zwei Segmente zusammnegefasst und dann die Differenz daraus, normiert

    // Parameter aus .cfg für Pinhole Kamera
    double  offset_Q1 = 650;            // individueller Offset per Quadrant, wird nach Mittelung abgezigen
    double  offset_Q2 = 650;
    double  offset_Q3 = 650;
    double  offset_Q4 = 650;
    int     numAverage = 50;            // gleitender Mittelwert
    int     A760_TriggerDelay = 250;    // in µs

    // sonstige Flags
    bool    isRemote = false;           // false = manuell ausgelöste Schusssequenz, true = über TCP/IP bestellt
} sourceControl_t;



typedef union
{
    volatile uint32_t	Long;
    volatile uint8_t	Byte[4];
} ULONG;

typedef union
{
    volatile uint16_t	Word;
    volatile uint8_t	Byte[2];
} UWORD;

enum decimal_t {comma, point};

typedef struct
{
    bool steps;     // die Motorschritte schreiben
    bool acc;       // die Beschleunigung mit in lastMove schreiben
    bool res;       // das Residual schreiben
    enum decimal_t decimal;    // Zahlenformat für "Save Last Move"
    int enc = 0;    // wenn ungleich Null den enstprechenden Encoder mit auslesen
    int encStatus = 0;  // wenn ungleich Null den enstprechenden Encoder Status mit auslesen
} flagsDebug_t;


typedef struct
{
    int mySlavescount = 0;  // Kopie von ec_slavecount

    int ECM701count = 0;    // Zahl der Slaves, die tatsächlich am Bus gefunden werden
    int ECM702count = 0;
    int ECM712count = 0;
    int ECM713count = 0;    // Einkanal Motortreiber mit Endstufe und Encoder
    int A760_01_B_count = 0;
    int EL1018count = 0;    // 8CH
    int EL1024count = 0;	// 4CH Digital In, Typ 2
    int EL1084count = 0;    // 4CH Digital In, masseschaltend
    int EL1104count = 0;    // 4CH Digital In, Typ 3
    int EL2202count = 0;    // 2CH Digital Out
    int EL2612count = 0;    // 2CH Relais Wechsler 30 VDC
    int EL2622count = 0;    // 2CH Relais Out
    //int A760_01count = 0;   // die Detektion
    int OTHERcount = 0;
    int ECM701index[MAXSLAVENUMBER];    // Position der Slaves innerhalb der EtherCAT Kette, SOEM beginnt den Index mit 1
    int ECM702index[MAXSLAVENUMBER];
    int ECM712index[MAXSLAVENUMBER];
    int ECM713index[MAXSLAVENUMBER];
    int A760_01_B_index[MAXSLAVENUMBER];
    int EL1018index[MAXSLAVENUMBER];
    int EL1024index[MAXSLAVENUMBER];
    int EL1084index[MAXSLAVENUMBER];
    int EL1104index[MAXSLAVENUMBER];
    int EL2202index[MAXSLAVENUMBER];
    int EL2612index[MAXSLAVENUMBER];
    int EL2622index[MAXSLAVENUMBER];
    //int A760_01index[MAXSLAVENUMBER];
    bool simulateECM = false;        // eventuell vorhandene Indexer und Encoder ignorieren, reine Simulation der Bewegung
} slavesInfo_t;


// 8-Bit Werte für die Konfiguration des Treiber-IC

#define TBLANK_1us          50   // 50x 20ns     Sets current trip blanking time, in increments of 20 ns, 1 us minimum
#define TBLANK_2us          100  // 100x 20ns
#define TBLANK_3us          150  // 150x 20ns

#define TOFF_6us            11  // 12x 500ns    Sets fixed off time, in increments of 500 ns
#define TOFF_8us            15  // 16x 500ns
#define TOFF_10us           19  // 20x 500ns
#define TOFF_12us           23  // 24x 500ns
#define TOFF_14us           27  // 28x 500ns

#define TDECAY_2us          4   //  4x  500ns   Sets mixed decay transition time, in increments of 500 ns
#define TDECAY_4us          8   //  8x  500ns
#define TDECAY_6us          12  // 12x  500ns
#define TDECAY_8us          16  // 16x  500ns

typedef struct
{
    //int     motor_Ctrl      = 0;      wird hier nicht verwendet, nur die vier Bit fürs µStepping werden live geschrieben, der Rest fix in der ECM-713-Firmware 0.9
    // default Werte für Projekt 483 Quelle BliX:
    int     motor_tOff      = TOFF_14us;
    int     motor_tBlank    = TBLANK_1us;
    int     motor_tDecay    = TDECAY_2us;
} motorDriver_t;

typedef struct
{
    int     numRules = 2;                       // Anzahl der Regeln zur Kollisionsvermeidung
    int     sensAxisIndex[MAXNUMSWITCHES];      // von welcher Achse die AUX gelesen werden, muß ein ECM-713 sein
    int     auxSelect[MAXNUMSWITCHES];          // 1 oder 2, welcher der beiden AUX Eingänge wird gelesen
    int     targetAxisIndex[MAXNUMSWITCHES];    // welche Achse wird überwacht
    int     targetAxisRoF[MAXNUMSWITCHES];      // -1 = wird auf Reverse Limit gemappt, 1 = wird auf Forward Limit gemappt

} collisionControl_t;


typedef struct
{
    // die Parameter aus .cfg-Datei oder manuell eingegeben:
    //QString name = "NU";                // NU = not used, geht nicht in Summenfehler Limit ein
    bool    isUsed = true;
    int     isGantry = 0;               // 0 = eigenständige Achse, Wert>0 = Folgeachse läuft passive bei Achse Nr. wert mit
    int     indexEncoder_A = 0;         // an welchen Encoder-Kanälen, 0 = kein Encoder, Zählung beginnt mit 1
    int     indexEncoder_B = 0;
    //int indexIndexer;                 // nicht nötig, implizite Zählung: Axis[0] = Indexer 1, Kanal 1 und so fort
    int     unitType = 0;               // 0 = mm, 1 = radian, 2 = grad
    int     feedbackType = 0;           // 0 = A, 1 = B, 2 = Mittelwert von A und B  - wird nicht mehr verwendet, vorerst immer Null
    double  steps_per_unit = 1000;      // der mechanische Übersetzungsfaktor, das Vorzeichen ist unabhängig von der Vorzugsdrehrichtung, Steps relativ Encoder
    int     motor_direction = 1;        // die Vorzugsdrehrichtung für den Schrittmotor, 1=positiv, -1=negativ
    //int offset_count_A = 0;
    //int offset_count_B = 0;
    int      velocity = 50000;           // maximale Fahrgeschwindigkeit in Schritten pro Sekunde
    int      velocity_backup = 50000;    // Kopie davon
    int      acceleration = 100000;
    //double      velocity_mplan;                // für die Bewegungsplanung
    //double      acceleration_mplan;            // dies ist der Wert, den die Bewegungsplanung nutzt
    int     hasBrake = 0;               // 0= Achse hat keine Motorbremse, sonst Index zu Relais - ist nicht mehr implementiert
    double  CL_DeltaFollow = 0;         // maximaler Schleppfehler in Units, überwacht Schrittverluste, 0 = Überwachung deaktiviert
    double  CL_Approach = 0.9;          // Konvergenzfaktor für Closed Loop, in der Regel etwas kleiner als Eins
    double  CL_DeltaMove = 0.01;        // Zielwert der maximalen Abweichung in Units
    int     CL_WaitMove = 50;           // Wartezeit nach jedem Submove in ms
    double  CL_DeltaStab = 0.02;        // maximale Abweichung in Units für automatische Stabilisierung
    int     CL_WaitStab = 500;            // Wartezeit für nächsten Stabilisierungmove in ms
    int     CL_Average = 20;            // gleitender Mittelwert für Encoderwerte
    int     axisType = AXISTYPE_NORMAL; // ist jetzt die Art der Achse: 2 = mit oder ohne Encoder absolute Positionierung möglich, 3 = nur Delta Steps
    bool    isStabilized = false;       // für jede Achse einzeln merken, ob automatische Stabilisierung aktiv
    int     initDirection = -1;         // -1 = Init fährt in Reverse Limit, 1 = Forward Limit
    int     initSteps = 0;              // wie weit soll noch vom Limit Switch wieder zurück gefahren werden

    int     microSteps = 6;             // Von 0= Full Step bis 8= 256 µSteps, 6 = 64 µSteps als Default
    int     motorCurrentRun = 50;       // Laufstrom 0 bis 255 gleich 0 bis 5 Aeff, 50 = 1.0 A als Default
    int     motorCurrentStop = 25;      // Haltestrom, 25 = 0.5 A als Default
    int     motorStatus;                // der vom Motortreiber zurückgemeldete Zustand
    int     axisDriverType = 0;             // 0=ECM-712 externer Motortreiber, 1=ECM-713 mit integriertem Motortreiber

    double      jogDeltaPosition = 1.0;
    int     jogDeltaSteps = 100;      // eine Umdrehung mit 64 µSteps - nö

    // die Variablen und Bwegungsplanung je Achse:
    bool    isMovingClosedLoop = false;     // Neu, wird bei Closed Loop Move dynamisch auf true gesetzt, false = default = moving in steps
    int     statusMove;                     // 0= inaktiv, 1= Bewegung läuft, 2= vom Planner initialisiert, wartet auf Ausführung, 3= wird vorbereitet, blockt Stabilisierung
    long long  T1, T2, T3, T4;                 // die Grenzen der drei Bewegungsphasen in ms
    double  T2_mplan, T3_mplan, T4_mplan;     // gebrochene Zeiten für die genaue Bewegungsplanung in Sekunden
    //int  vCruise;                         // die Fahrgeschwindigkeit im konstanten Bereich in Steps pro Sekunde
    double  aRamp;
    double  sRamp;                          //  der in der Beschleunigungsrampe zurückgelegte Weg in Steps
    long long  sCruise;
    long long  deltaSteps;                     //  der gewünschte Fahrweg eines Moves in Steps, mit Vorzeichen für Richtung
    int     direction;                      //  die Laufrichtung, Vorzeichen von deltaSteps, 1 = vorwärts, -1 = rückwärts
    long long  positionSteps = 0;              // Stand der Achse in Steps mit Vorzeichen, läuft im move live mit
    //long long  positionStepsShadow = 0;        // Schatten des Zählers im Indexer, wird nicht benötigt
    long long  positionStepsOffset = 0;        // für den Init Befehl, da sich die einzelne Achse eines ECM-712 nicht wirklich resetten lässt
                                            // ist die Differenz zwischen Shadow und den Steps überall sonst im Programm
    long long  positionStepsStart = 0;         // merkt sich Position in Steps zu Beginn eines Moves
    long long  positionStepsTarget = 0;        // Ziel des Moves in Steps gleich Anzeige im GUI, alter Wert zugleich Start von Delta/Joggen
    long long  positionCounts;                 // Encoder-Counts, eigentlich unsigned
    long long  positionCountsTarget;
    double  positionUnits;                  // aktuelle Position in Units
    double  positionUnitsStart;             // merkt sich Position in Units zu Beginn eines Moves
    double  positionUnitsAverage;           // aktuelle Position als gleitender Mittelwert auf den geregelt wird
    double  positionUnitsTarget;            // die Zielposition in Units für den aktuellen Move im Closed Loop Modus

    int     CL_WaitMoveDowncount = 0;
    int     CL_downcountIterations;
    int     CL_StabWaitDowncount = 0;           // Zähler in Sekunden bis frühester Korrektur
    double  positionUnitsLast[MAXAVERAGE];      // Speicher für gleitenden Mittelwert
    bool    limitReverse, limitForward;         // Endlagenschalter, true = Limit ist erreicht
    bool    limitReverseEncoder, limitForwardEncoder;       // Encoder Limits, true = Limit ist erreicht
    bool    limitAux1 = true, limitAux2 = true;             //  die beiden zusätzlichen Eingänge am ECM-713, true entspricht nichts angeschlossen/Stromkreis offen
    //bool    followingError = false;             // diese Achse weist eine Diskrepanz zwischen Steps und Encoder auf, Motor blockiert
    int     isIniting = 0;                      // Flag für die Initialisierung der Achse, 0= Normalzustand, 3= Init Move in den Reverse Limit, 2= fährt aus dem Limit wieder raus, 1= fährt noch limitSteps weiter raus um eine Nullmarke zu finden

    // Bewegungsspeicher für jede Achse:
    //double      lastMoveStepsDouble[MAXPATHPOINTS];         // nur für Diagnostik des Move Algorithmus, rausgenommen um Speicher zusparen
    long long      lastMoveSteps[MAXPATHPOINTS];               // Profil der jeweils letzten Bewegung für Diagnose, die berechneten Soll-Schritte
    long long      lastMoveStepsIst[MAXPATHPOINTS];            // Profil der jeweils letzten Bewegung für Diagnose, die vom Indexer zurückgelesenen Ist-Schritte
    double      lastMovePosition[MAXPATHPOINTS];            // die aus den Encoderwerten berechnete Position
    double      lastMovePositionAverage[MAXPATHPOINTS];     // gleitender Mittelwert daraus
    double      lastMoveDeltaFollow[MAXPATHPOINTS];         // für Diagnose der Schleppfehlerüberwachung
    //double      lastMoveDebugEnc[MAXPATHPOINTS];            // für Diagnose von Encodern
    int         indexPath = 0;
    int         lastMoveTailDowncount = 0;                  // Zähler fürs Nachlaufen des Protokolls, einer für jede Achse

    double      posDouble;                          // damit der letzte Wert vor Bewegungsstop erhalten bleibt

    // OTF Speicher für jede Achse:
    double  positionUnitsOTF[MAXOTFSAMPLES];        // die von den ECM-702 gesammelten Counts on Trigger in Units umgerechnet
    int     numOTF = 0;                             // die Zahl der OTF Samples einer Achse, 0 = Speicher ist leer

} AxisInfo_t;


typedef struct
{
    int         numAxis;                        // gesamte Zahl aller Achsen in der aktuell realen Konfiguration, wird bei Programmstart und Restart EtherCAT automatisch auf 4* foundSlaves.ECM712count plus 1* foundSlaves.ECM713count gesetzt
    int         numAxisConfig;                  // Zahl der konfigurierten Achsen laut .cfg-Datei
    int         currentAxis = 0;                // gerade in der Liste im GUI ausgewählt
    int         otfAxisNr = 0;                  // Achse für On-the-Fly mit Tonics, 0 = none, sonst die Achsennummer 1 bis N
    bool        otfCapturing = false;           // ist true währen die Führungsachse fährt, dies ist die Torzeit
    bool        otfReady = false;               // Signal an den maindialog, daß eine otf Sequenz fertig ist
    bool        otfIsRemote = false;            // wenn true, wurde der OTF per TCP/IP von Borys ausgelöst
    bool        isMovingGlobal = false;         // true = mindestens eine der Achsen bewegt sich, false = alles ruht
    bool        ignoreLimits = false;           // Limitschalter ignorieren, ist global für alle Achsen, nur temporär
    int         lastMoveTail = 0;               // Nachlaufzeit für alle Achsenprotokolle in ms

    bool        errorEncoderGlobal = 0;         // Globales Flag für alle Achsen, true = mindestens ein Encoder gestört
    bool        isStabilizedGlobal = false;     // Globales Flag zum Ein- und Ausschalten der automatischen Stabilisierung
    bool        initFlag = false;               // für einmalige Initialisierung der Zielpositionen bei Programmstart

    volatile bool   puaBlock = false;           // schützt positionUnitsAverage vor Update, wenn gerade ein MoveToUnits berechnet wird
    bool        puaSplit = false;               // Marker nur für Diagnostik, wenn true wäre positionUnitsAverage ohne Schutz gespalten

    //QString     comment = "";                   // Kommentar über die gesamte Konfiguration
    int         statusMotionGlobal = 0;         // Globaler Status zur Diagnose, wird im GUI angezeigt,
    int         statusMotionArg = 0;            // letzte Achse oder Encoder, welcher einen Fehler meldet

    int         numSimulatedAxes = 0;           // Anzahl simulierter Achsen ganz ohne Hardware

    //QString     defaultFileName = "Default_Project";    // Der Name der Datei, die zum Speichern der Konfiguration verwendet wird
    int         numOTFmax = 0;                  // gemeinsamer Index für alle OTF-Achsen, das Maximum der gefundenen Einträge

    AxisInfo_t      Axis[MAXAXISNUMBER];        // alle meine Achsen
} myAxes_t;

typedef struct
{
    // feste Parameter aus Konfiguration:
    //QString     name = "NU";                    // NU = not used, geht nicht in Summenfehler Limit ein
    //QString     encoderTypeName = "";           // Absolut oder inkrementell
    bool        isUsed = false;
    int         encoderType = 0;                // 1=ECM-701  2=ECM-713  3=ECM-702
    int         encoderModus = ENCMODUS_LIN;         // 0=lineare Achse oder Abschnitt Ring, 1=Ring +/-180 Grad, 2 =Ring +/-181 Grad mit Überlappung
    double      units_per_count = 5e-6;         // physische Einheiten pro Encoder-Counts, hier 5 nm pro Count
    long long   offsetCounts = 0;               // Offset in Counts, Stelle am Encoder die physisch Units=0 entspricht, ist anders die Counts signed, kann negativ sein
    long long   limitLow = 0;                   // Virtueller Limit Switch in Counts, 0 = keine Limitfunktion
    long long   limitHigh = 0;
    double      offsetUnits = 0;                // zusätzlicher Offset für Drehachsen, dort in Grad
    uint16_t    revision = 0;                   // die Versionsnummer jeweils eines ECM-701- oder 702-Moduls, ist für jeweils vier oder zwei Kanäle gleich

    // Bewegungsvariablen:
    long long   counts;                         // die aktuellen Counts aller Encoderkanäle, fängt mit Index 0 an
    long long   counts_mean;                    // gleitender Mittelwert nur fürs Logging, der läuft immer durch
    long long   countsLast[AVERAGEENCODER];     // Speicher für gleitenden Mittelwert
    double      positionUnits;                  // gelesener Positionswert des Resolute in Units umgerechnet, zB mm
    int         encStateRaw;                    // Zustand wie direkt vom Encoder gelesen, ist bei Resolute mit encState identisch
    int         encState = ENCSTATE_ZEROLOST;   // 0 = alles Ok, Rest siehe ENCSTATE_
    int         rotationState = 0;              // -1 = in negativer Laufrichtung kurz vor Reverse Limit, +1 = in positiver Laufrichtung kurz vor Forward Limit
    uint16_t    trigCount;                      // Achtung eigene Indizierung, da dieser Zähler nur einmal je Modul vorhanden ist!

    // nur für die Tonics, dies wird bei den Resolute ignoriert
    int         referencing = 0;                // 0 = überfährt Zero Marken unverändert, 1 = wird bei Marke gelöscht, 2 = wird sofort auf Null gesetzt
    bool        trigpolNegative = false;        // true = negative Flanke triggert den Position Capture, sonst die positive

    // für das Logging
    bool        doLogging = false;

    // für das OTF Capture
    int         otfIndex = 0;                   // 0 = OTF Sample Speicher ist leer, sonst Anzahl der Captures
    long long   otfCounts[MAXOTFSAMPLES];

} EncoderInfo_t;
/*
typedef struct
{
    int         index = 0;      // 0 = OTF Sample Speicher ist leer, sonst Anzahl der Captures
    long long   counts[MAXOTFSAMPLES];

} otfData_t;
*/
typedef struct
{
    int             numEncoder;                         // Anzahl der verfügbaren Encoderkanäle, wird beim Start von eCAT automatisch aus der zahl der verfügbaren Module berechnet
    int             numEncoderConfig;                   // Zahl aus der .cfg-Datei
    bool            state_Trigger_Resolute;             // false = Trigger interner EterCAT-Zyklus, true = externer Trigger, für alle ECM-701 gleich
    EncoderInfo_t   Encoder[MAXENCODERNUMBER];          // Liste beginnt mit 0, ein Eintrag für jeden Kanal = Lesekopf
    //otfData_t       otfData[MAXENCODERNUMBER];          // OTF Daten je Tonic Encoder, für Resolute Kanäle zunächst ohne Funktion

    //double          positionDebug[];
    double          lastMoveDebugEnc[MAXPATHPOINTS];    // ein Encoder kann für Diagnose bei saveLastMove mitgespeichert werden
    double          lastMoveDebugCapture[MAXPATHPOINTS];
    int             lastMoveDebugEncStatus[MAXPATHPOINTS];
} myEncoderChannels_t;


#define WKC_INIT    0
#define WKC_OK      1
#define WKC_ERR     2

typedef struct
{
    int     workingCounterState = WKC_INIT; // 0=init 1=OK 2=Error
    bool    allSlavesOperational;
    int     numErrorCycles = 0;             // Summe der Zyklen mit WKC Fehler
    long long  msCounter = 0;                  // durchlaufender Zähler ab Programmstart, "Betriebsstunden" in ms
    long long  protoCounter = 0;
    long long  eCycleCounter = 0;              // durchlaufender Zähler der eCat Frames, sind faktisch ms seit Start oder Restart
    int     deltaJitter;                    // Jitter nach ec_send_processdata() in µs
    int     deltaJitterMax = 0;
    int     deltaLoop;                      // Ende der rt_Loop, Maß für den Anteil an der Zykluszeit von 1000 µs
    int     deltaLoopMax = 0;
    int     deltaSleep;                     // direkt nach nanosleep
    int     deltaSleepMax = 0;
} ecatState_t;


// alle Relais in der Reihenfolge EL2622, EL2612
typedef struct
{
    bool    state[MAXRELAISNUMBER];
    int     numRelaisBits = 0;      // Die Zahl der gefundenen Relais-Kontakte = Zahl der Ausgangs-Bits. Der einzige Ausgangsbaustein EL2202 der kein Relais ist, steuert den Laser und ist bewußt ausgenommen!
} myRelais_t;

// alle Eingänge in der Reihenfolge EL1024, EL1104, EL1084, EL1018
typedef struct
{
    bool    state[MAXINPUTNUMBER];
    int     numInputBits = 0;       // Anzahl der digitalen Eingänge
} myInputs_t;

typedef struct
{
    bool refreshMoveFinished;
    enum lastMoveType_t {STEPS, UNITS, COUNTS} lastMoveType;    // für die aktualisierung der neuen Zielwerte und des GUI nach einem Move
    bool doWriteRotationState = false;
    bool doLoggingEncoder = false;                              // wenn ja jede Sekunde die Encoder-Werte in Log-Datei schreiben
} flags_t;


//
//      Die Deklarationen der gemeinsamen Funktionen aus ecTool,
//      die auch in maindialog genutzt werden.
//

void lock_AllMemory(void);
void set_cpu_dma_latency(void);

void create_RT(void);
void elevate_RT(void);

void Startup_Ecat(char *ifname);
void Restart_Ecat(char *ifname);
void Stop_Ecat();

void MovePlan(int axis, int targetSteps, double relVel);
void StopAxis(int axis);
void StopAllAxis();



#endif // GLOBALS_H
