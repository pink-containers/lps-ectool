//
//
//  ecTool      EtherCAT Master mit graphischer Benutzeroberfläche als allgemeines Inbetriebnahmetool für BESTEC Projekte
//              verwendet SOEM und QT4
//
//              In diesem Modul sind im wesentlichen die Real Time und Bewegungs-Routinen drin, der GUI-Kram ist nach maindialog.cpp ausgelagert
//
//  Autor:      Christian Grundel
//
//              Im Modul ecTool.cpp ist die Bewegungssteuerung und der Zugriff auf die Hardware enthalten
//              Am 8.10.20 konsolidiert und aufgeräumt
//              23.03.21 wichtige Ergänzung: CRC Check für die Resolute
//              16.03.22 Shutter Steuerung funktioniert
//              08.06.22 StopAllAxis() bricht auch den Prozess ab
//

#define EC_TIMEOUTMON 500

#include "common.h"

#include <fcntl.h>          // File Control Definitions
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <unistd.h>         // für microsleep
#include <pthread.h>
#include <sys/mman.h>       /* Memory locking functions */
//#include <linux/serial.h>
//#include <termios.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include "soem/ethercattype.h"
#include "soem/nicdrv.h"
#include "soem/ethercatbase.h"
#include "soem/ethercatmain.h"
#include "soem/ethercatdc.h"
//#include "soem/ethercatcoe.h"
//#include "soem/ethercatfoe.h"
#include "soem/ethercatconfig.h"
//#include "soem/ethercatprint.h"


//
//          lokale Typdefinitionen für ecTool
//

// für das Auslesen der 4-Bit Inputs
typedef struct
{
    bool ch1;
    bool ch2;
    bool ch3;
    bool ch4;
} fourBits_t;

// für das Auslesen der 8-Bit Inputs
typedef struct
{
    bool ch1;
    bool ch2;
    bool ch3;
    bool ch4;
    bool ch5;
    bool ch6;
    bool ch7;
    bool ch8;
} eightBits_t;


//
//          gemeinsame globale Variablen für ecTool und maindialog
//

slavesInfo_t                foundSlaves;            // Array fängt bei Null an
myEncoderChannels_t         myEncoderChannels;      // Array fängt bei Null an
myAxes_t                    myAxes;                 // Array fängt bei Null an
motorDriver_t               motorDriverConfig;      // die interne Konfiguration der ECM-713 Endstufen
collisionControl_t          collisionControl;
ecatState_t                 ecatState;
myRelais_t                  myRelais;
myInputs_t                  myInputs;

sourceControl_t             sourceControl;


//
//          lokale globale Variablen für ecTool
//

long long       diag_StepsIst;

int             fd_serial;
pthread_t       thread1;

char            IOmap[4096];   // viel Platz für die Prozessdaten
int             expectedWKC, oloop, iloop, cycle_no = 0;
boolean         needlf;
volatile int    wkc;

flags_t         flags;
flagsDebug_t    flagsDebug;

uint8_t         currentgroup = 0;

pthread_cond_t  cond  = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


//
//          Deklaration der lokalen Funktionen von ecTool
//

void        *rt_Loop(void *ptr);                            // Deklaration der Real Time Schleife
static void MovePlanRT(int axis, int targetSteps);          // nur intern hier verwenden, das "static" erzwingt das
static void StopAxisRT(int axis);

void        set_EL2202_CH1(uint16 slave_no, bool output, bool tristate);
void        set_EL2202_CH2(uint16 slave_no, bool output, bool tristate);
void        set_EL2612(uint16 slave_no, bool relais_1, bool relais_2);
void        set_EL2622(uint16 slave_no, bool relais_1, bool relais_2);

fourBits_t  get_EL1024(uint16 slave_no);
fourBits_t  get_EL1104(uint16 slave_no);
fourBits_t  get_EL1084(uint16 slave_no);
eightBits_t get_EL1018(uint16 slave_no);

void        set_ECM701_TrigMode(uint16 slave_no, bool state_Trigger);
int         get_ECM701_EncoderCounts(uint16 slave_no, int channel, uint32_t *counts);
uint16_t    get_ECM701_TrigCount(uint16 slave_no);
uint16_t    get_ECM701_Revision(uint16 slave_no);

void        set_ECM702_ControlByte(uint16 slave_no, int controlByte);
uint32_t    get_ECM702_EncoderCounts(uint16 slave_no, int channel);
uint32_t    get_ECM702_otfCaptureCounts(uint16 slave_no, int channel);
int         get_ECM702_Status(uint16 slave_no, int channel);
uint8_t     get_ECM702_Revision(uint16 slave_no);

int32_t     get_ECM712_StepsIst(uint16 slave_no, int channel);
void        get_ECM712_Limits(uint16 slave_no, int channel, bool *lowerLimit, bool *upperLimit);
void        set_ECM712_StepsSoll(uint16 slave_no, int channel, int32_t absSteps);

int32_t     get_ECM713_StepsIst(uint16 slave_no);
void        get_ECM713_Limits(uint16 slave_no, bool *lowerLimit, bool *upperLimit, bool *aux1, bool *aux2);
uint8       get_ECM713_MotorStatus(uint16 slave_no);
void        set_ECM713_StepsSoll(uint16 slave_no, int32_t absSteps);
uint16_t    get_ECM713_Revision(uint16 slave_no);
int         get_ECM713_EncoderCounts(uint16 slave_no, uint32_t *counts);
void        set_ECM713_microSteps(uint16 slave_no, uint8 microSteps);
void        set_ECM713_current(uint16 slave_no, uint8 current);
void        set_ECM713_TimingAndCtrl(uint16 slave_no);

void        set_A760_01_B_Shutter(uint16 slave_no, bool state_Trigger);
void        set_A760_01_B_Delay(uint16 slave_no, uint16 delay);
void        get_A760_01_B_DetectorCounts(uint16 slave_no, int *Q1, int *Q2,  int *Q3,  int *Q4);
uint8_t     get_A760_01_B_Revision(uint16 slave_no);
uint8_t     get_A760_01_B_Status(uint16 slave_no);



uint8_t     bissCRC(uint64_t checkData);    // Berechnung der CRC für die Resolute

//
//          Definition der Motion Funktionen
//

void Startup_Ecat(char *ifname)
{
    int i, j, chk;
    needlf = FALSE;
    ecatState.allSlavesOperational = FALSE;

    foundSlaves.ECM701count = 0;  // werden schon in der Typendeklaration initialisiert, aber wird hier für Restart benötigt
    foundSlaves.ECM702count = 0;
    foundSlaves.ECM712count = 0;
    foundSlaves.ECM713count = 0;
    foundSlaves.A760_01_B_count = 0;
    foundSlaves.EL2612count = 0;
    foundSlaves.EL2622count = 0;
    foundSlaves.EL2202count = 0;
    foundSlaves.EL1104count = 0;
    foundSlaves.EL1018count = 0;
    foundSlaves.EL1024count = 0;
    foundSlaves.EL1084count = 0;
    foundSlaves.OTHERcount = 0;
    ec_slavecount = 0;

    //ifname = ecatName.toLatin1().data();

    if (ec_init(ifname))     // initialise SOEM, bind socket to ifname
    {
        fprintf(stderr, "Ok: ec_init on %s succeeded.\n\n",ifname);

        if ( ec_config_init(FALSE) > 0 )    // FALSE = find and auto-config all slaves
        {
            fprintf(stderr, "%d slaves found and configured:\n", ec_slavecount);
            foundSlaves.mySlavescount = ec_slavecount;

            for(i = 1; i<=ec_slavecount ; i++)  // das soem array fängt mit 1 an !
            {
                fprintf(stderr, "%d %s\n", i, ec_slave[i].name);
                // Tabelle der bekannten Slaves aufbauen:
                if(ec_slave[i].eep_man == 0x0179B && ec_slave[i].eep_id == 0x0701)  // Vendor ID Physimetron, Produkt ECM-701 Biss Encoder
                    foundSlaves.ECM701index[foundSlaves.ECM701count++] = i;         // Position des gefundenen Slaves in die Tabelle eintragen
                else
                    if(ec_slave[i].eep_man == 0x0179B && ec_slave[i].eep_id == 0x0702)  // Vendor ID Physimetron, Produkt ECM-702 Incremental Encoder Interface
                        foundSlaves.ECM702index[foundSlaves.ECM702count++] = i;
                    else
                        if(ec_slave[i].eep_man == 0x0179B && ec_slave[i].eep_id == 0x0712)  // Vendor ID Physimetron, Produkt ECM-712 Stepper Indexer
                            foundSlaves.ECM712index[foundSlaves.ECM712count++] = i;
                    else
                            if(ec_slave[i].eep_man == 0x0179B && ec_slave[i].eep_id == 0x0713)  // Vendor ID Physimetron, Produkt ECM-713 Stepper Indexer mit integrierter Endstufe
                                foundSlaves.ECM713index[foundSlaves.ECM713count++] = i;
                            else
                                if(ec_slave[i].eep_man == 0x0179B && ec_slave[i].eep_id == 0x0762)  // Vendor ID Physimetron, Produkt A760-01-B für 4Q Diode Projekt 483
                                    foundSlaves.A760_01_B_index[foundSlaves.A760_01_B_count++] = i;
                                else
                                    if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0xa343052)  // Vendor ID Beckhoff, Produkt EL2612 2CH Relais mit Wechsler 30 VDC
                            foundSlaves.EL2612index[foundSlaves.EL2612count++] = i;
                        else
                            if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0xa3e3052)  // Vendor ID Beckhoff, Produkt EL2622 2CH Relais
                                foundSlaves.EL2622index[foundSlaves.EL2622count++] = i;
                            else
                            if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0x89a3052)  // Vendor ID Beckhoff, Produkt EL2202 2CH Digital Out
                                foundSlaves.EL2202index[foundSlaves.EL2202count++] = i;
                            else
                                if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0x4503052)  // Vendor ID Beckhoff, Produkt EL1104 4CH Digital In
                                    foundSlaves.EL1104index[foundSlaves.EL1104count++] = i;
                                else
                                    if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0x03fa3052)  // Vendor ID Beckhoff, Produkt EL1018 8CH Digital In
                                        foundSlaves.EL1018index[foundSlaves.EL1018count++] = i;
                                    else
                                        if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0x4003052)  // Vendor ID Beckhoff, Produkt EL1024 4CH Digital In
                                            foundSlaves.EL1024index[foundSlaves.EL1024count++] = i;
                                        else
                                            if(ec_slave[i].eep_man == 0x02 && ec_slave[i].eep_id == 0x43c3052)  // Vendor ID Beckhoff, Produkt EL1084 4CH Digital In
                                                foundSlaves.EL1084index[foundSlaves.EL1084count++] = i;
                                            else
                                                foundSlaves.OTHERcount++;
            }
            ec_config_map(&IOmap);  // Mapping des Process Image erstellen

            myAxes.numAxis = 4 * foundSlaves.ECM712count;                                       // da vier Axen je Indexer          
            myAxes.numAxis += foundSlaves.ECM713count;                                          // hat nur einen Motor

            myAxes.numAxis += myAxes.numSimulatedAxes;                                          // simulierte Achsen ohne Hardware dazupacken

            for(int i=0; i<foundSlaves.ECM713count; ++i)
            {
                myAxes.Axis[4 * foundSlaves.ECM712count + i].axisDriverType = 1;                // die Achsen mit ECM-713 markieren
            }

            myEncoderChannels.numEncoder = 4 * foundSlaves.ECM701count;                         // da vier Axen je Encoder Modul
            myEncoderChannels.numEncoder += foundSlaves.ECM713count;                            // ein Encoder pro ECM-713
            myEncoderChannels.numEncoder += 2 * foundSlaves.ECM702count;                        // der 702 für inkrementelle Encoder hat nur zwei Kanäle

            myRelais.numRelaisBits = 2*foundSlaves.EL2622count + 2*foundSlaves.EL2612count;     // da zwei Kontakte je Relais Modul
            myInputs.numInputBits = 4*foundSlaves.EL1024count + 4*foundSlaves.EL1104count + 4*foundSlaves.EL1084count + 8*foundSlaves.EL1018count;

            for(i=0; i < foundSlaves.ECM701count; i++)
            {
                for(j=0; j<4; j++)  // durch alle 4 Kanäle jedes Moduls iterieren
                {
                    int index_enc = 4*i+j;
                    myEncoderChannels.Encoder[index_enc].encoderType = ENCTYPE_ECM_701;
                }
            }

            for(i=0; i < foundSlaves.ECM713count; i++)  // die ECM-713 Encoder vor die Tonics einordnen, damit alle absoluten zusammen stehen
            {
                    int index_enc = 4* foundSlaves.ECM701count + i;
                    myEncoderChannels.Encoder[index_enc].encoderType = ENCTYPE_ECM_713;         // auch Resolute
            }

            for(i=0; i < foundSlaves.ECM702count; i++)
            {
                for(j=0; j<2; j++)  // durch alle 2 Kanäle jedes Moduls iterieren
                {
                    int index_enc = 4* foundSlaves.ECM701count + foundSlaves.ECM713count + 2*i+j;
                    myEncoderChannels.Encoder[index_enc].encoderType = ENCTYPE_ECM_702;         // die Tonics
                }
            }


            //
            //  das Prozessdatenabbild für alle Indexer-Kanäle auf Null Steps initialisieren,
            //  damit beim Restart keine unerwünschten Sprünge auftreten.
            //  Die internen Zähler der ECM712 und ECM713 werden beim Reset implizit auf Null initialisiert,
            //  so ist auch die Differenz zum Prozessdatenabbild gleich Null.
            //  Muss vor dem ersten ec_send_processdata() und nach ec_config_map() geschehen
            //
            for(i=0; i < foundSlaves.ECM712count; i++)
            {
                for(j=0; j<4; j++)  // durch alle 4 Kanäle jedes Indexer Moduls iterieren
                {
                    set_ECM712_StepsSoll(foundSlaves.ECM712index[i], j +1, 0);
                }
            }

            for(i=0; i < foundSlaves.ECM713count; i++)
            {
                set_ECM713_StepsSoll(foundSlaves.ECM713index[i], 0);
                // die Mikroschritte werden nur nach einem eCat Restart geändert
                int index = i + 4*foundSlaves.ECM712count;  // Zeiger auf den laufenden Achsen Eintrag
                set_ECM713_microSteps(foundSlaves.ECM713index[i], myAxes.Axis[index].microSteps);
                set_ECM713_TimingAndCtrl(foundSlaves.ECM713index[i]);

                // hier wird der Motorstrom ein erstes Mal initialisiert, damit es beim Einschalten des DRV keinen unkontrollierten Stromstoß zum Motor gibt
                // der Stop-Wert ist hier schon aus der config gelesen
                set_ECM713_current(foundSlaves.ECM713index[i], myAxes.Axis[index].motorCurrentStop);
            }

            for(i=0; i < myAxes.numAxis; i++)
            {
                // muss so sein, damit die Initialisierung encoderloser Achsen beim Restart nicht verloren geht:
                myAxes.Axis[i].positionStepsOffset  = myAxes.Axis[i].positionSteps;     // Stand der Achse in Steps mit Vorzeichen, läuft im move live mit
                myAxes.Axis[i].positionStepsStart   = myAxes.Axis[i].positionSteps;     // merkt sich Position zu Beginn eines Moves
                myAxes.Axis[i].positionStepsTarget  = myAxes.Axis[i].positionSteps;     // die Offsets für den Init Befehl rücksetzen ???
            }

            // auch das Prozessdatenbild für alle Relais auf Aus initialisieren:
            for(i=0; i < myRelais.numRelaisBits; i++)               // auf numRelaisBits ändern ???
            {
                myRelais.state[i] = false;                          // damit sie nicht gleich wieder mit den letzten Werten an gehen
            }
            for(i=0; i < foundSlaves.EL2622count; i++)
            {
                set_EL2622(foundSlaves.EL2622index[i], 0, 0);       // Alle Relais im Prozessdatenbild rücksetzen
            }
            for(i=0; i < foundSlaves.EL2612count; i++)
            {
                set_EL2612(foundSlaves.EL2612index[i], 0, 0);       // Alle Wechsler im Prozessdatenbild rücksetzen
            }

            // Auch den Digital Out für den Laser initialisieren:
            for(i=0; i < foundSlaves.EL2202count; i++)
            {
                set_EL2202_CH1(foundSlaves.EL2202index[i], false, false);   // Shutter schließen
                set_EL2202_CH2(foundSlaves.EL2202index[i], false, false);   // Laser Trigger rücksetzen
            }

            // den Trigger Delay initialisieren
            if(foundSlaves.A760_01_B_count > 0)
            {
                set_A760_01_B_Delay(foundSlaves.A760_01_B_index[0], sourceControl.A760_TriggerDelay);
            }

            //ec_configdc();    // es werden keine Distributed Clocks verwendet

            fprintf(stderr, "Slaves mapped, setting state to SAFE_OP.\n");

            ec_statecheck(0, EC_STATE_SAFE_OP,  EC_TIMEOUTSTATE * 4);   // wait for all slaves to reach SAFE_OP state

            oloop = ec_slave[0].Obytes;
            if ((oloop == 0) && (ec_slave[0].Obits > 0)) oloop = 1;
            if (oloop > 8) oloop = 8;
            iloop = ec_slave[0].Ibytes;
            if ((iloop == 0) && (ec_slave[0].Ibits > 0)) iloop = 1;
            if (iloop > 8) iloop = 8;

            fprintf(stderr, "segments : %d : %d %d %d %d\n",ec_group[0].nsegments ,ec_group[0].IOsegment[0],ec_group[0].IOsegment[1],ec_group[0].IOsegment[2],ec_group[0].IOsegment[3]);

            fprintf(stderr, "Requesting OP state for all slaves\n");
            expectedWKC = (ec_group[0].outputsWKC * 2) + ec_group[0].inputsWKC;
            fprintf(stderr, "Calculated workcounter = %d\n", expectedWKC);
            ec_slave[0].state = EC_STATE_OPERATIONAL;

            ec_send_processdata();       // send one valid process data telegram to make outputs in slaves happy
            ec_receive_processdata(EC_TIMEOUTRET);

            ec_writestate(0);           // request OP state for all slaves
            chk = 40;
            /* wait for all slaves to reach OP state */
            do
            {
                ec_send_processdata();
                ec_receive_processdata(EC_TIMEOUTRET);
                ec_statecheck(0, EC_STATE_OPERATIONAL, 50000);
            }
            while (chk-- && (ec_slave[0].state != EC_STATE_OPERATIONAL));

            if (ec_slave[0].state == EC_STATE_OPERATIONAL )
            {
                fprintf(stderr, "OP state reached for all slaves.\n");
                //usleep(500000);         // 100 Millisekunden warten, ganz wichtig sonst startet ecTool nicht wenn als Release kompiliert!! ???
                ecatState.allSlavesOperational = TRUE;
            }
            else
            {
                fprintf(stderr, "Error - Not all slaves reached operational state:\n");
                ec_readstate();
                for(i = 1; i<=ec_slavecount ; i++)
                {
                    if(ec_slave[i].state != EC_STATE_OPERATIONAL)
                    {
                        /*
                        fprintf(stderr, "Slave %d State=0x%2.2x StatusCode=0x%4.4x : %s\n",
                               i, ec_slave[i].state, ec_slave[i].ALstatuscode, ec_ALstatuscode2string(ec_slave[i].ALstatuscode));
                               */
                    }
                }
            }
        }
        else
        {
            fprintf(stderr, "No slaves found!\n");
        }
    }
    else
    {
        fprintf(stderr, "Error: No socket connection on %s, use correct name and capabilities.\n", ifname);
    }
}

void Restart_Ecat(char *ifname)
{
    ecatState.allSlavesOperational = FALSE;     // RT Zyklus stoppen
    ecatState.workingCounterState = WKC_INIT;   // Restart State
    ecatState.numErrorCycles = 0;
    ecatState.eCycleCounter = 0;
    ecatState.deltaLoopMax = 0;
    ecatState.deltaJitterMax = 0;
    ecatState.deltaSleepMax = 0;

    ec_slave[0].state = EC_STATE_INIT;
    ec_writestate(0);
    ec_close();                         // stop SOEM, close socket

    fprintf(stderr, "\n...restarting EtherCAT...\n");
    usleep(2000000);                    // 2 Sekunden warten und damit die ECM sicher in den Watchdog Timeout treiben und auf erfolgreichen Restart der Slaves warten
    Startup_Ecat(ifname);     // startet SOEM mit aktuellem Socket Namen
}

void Stop_Ecat()
{
    ecatState.allSlavesOperational = false;   // RT Zyklus stoppen
    fprintf(stderr, "\nRequesting INIT state for all slaves\n");
    ec_slave[0].state = EC_STATE_INIT;
    ec_writestate(0);
    fprintf(stderr, "closing EtherCAT socket\n");
    ec_close();             // stop SOEM, close EthetCAT socket
}


//
//  die Bewegungsphasen in ms ausrechnen und den Move aktivieren
//  axis von 0 bis myAxisList.numAxis-1,  targetSteps ist die Zielposition
//
//  Diese Routine wurde am 9.9.19 komplett überarbeitet,
//  die stetige Zusammenfügung der Bewegungssegmente beim Trapez Profil funktioniert nur mit gebrochenen Zeiten!
//
//  wird innerhalb der RT Schleife *nicht* aufgerufen
//
void MovePlan(int axis, int targetSteps, double relVel)
{
    int delta;

    if(myAxes.statusMotionGlobal != GSTATUS_FOLLOWING_ERROR && myAxes.statusMotionGlobal != GSTATUS_TOPOLOGY_ERROR)     // bei Following oder Topology Error keine weiteren Moves ausführen
    {
        myAxes.Axis[axis].statusMove = 3;                                           // Stab blockieren, Achse stoppt hier sofort (die RT Loop macht bei 3 nichts)
        // relative Reduzierung der Fahrgeschwindigkeit für einen Move, wird von der RT Loop am Ende wieder zurückgesetzt:
        myAxes.Axis[axis].velocity = relVel * myAxes.Axis[axis].velocity_backup;    // Geschwindigkeit mit Faktor skalieren, robust da von _backup gelesen wird
        myAxes.Axis[axis].lastMoveTailDowncount = myAxes.lastMoveTail;              // Zähler für Bewegungsprotokoll Nachlauf initialisieren, wartet auf status 0 und zählt dann runter
        myAxes.Axis[axis].indexPath = 0;                                            // Speicher für Bewegungsprotokoll initialisieren, Zeiger auf Null

        myAxes.Axis[axis].positionStepsStart = myAxes.Axis[axis].positionSteps;     // Position zu Beginn des Moves merken
        myAxes.Axis[axis].positionStepsTarget = targetSteps;
        delta = targetSteps - myAxes.Axis[axis].positionStepsStart;

        if(delta > 0)
            myAxes.Axis[axis].direction = 1;       // Laufrichtung merken
        else
            myAxes.Axis[axis].direction = -1;

        myAxes.Axis[axis].deltaSteps = delta;         // Länge des Fahrwegs in Steps mit Vorzeichen merken
        // Weg der in einer der beiden Rampen eines Dreiecksprofils mit velocity als Spitzenwert zurückgelegt würde, in Steps (Umweg über double zur Vermeidung von Rundungsfehlern oder numerischen Überlaufs des Quadrates):
        myAxes.Axis[axis].sRamp = 0.5 * (double)myAxes.Axis[axis].velocity * (double)myAxes.Axis[axis].velocity / (double)myAxes.Axis[axis].acceleration;

        if(2 * myAxes.Axis[axis].sRamp < abs(delta))       // Trapezprofil fahren, hat Anteil mit konstanter der Achse maximal möglicher Geschwindigkeit
        {
            myAxes.Axis[axis].T2_mplan = (double)myAxes.Axis[axis].velocity / (double)myAxes.Axis[axis].acceleration;       // Beschleunigungs-Phase
            myAxes.Axis[axis].T3_mplan = myAxes.Axis[axis].T2_mplan + ((double)abs(delta) - 2.0 * (double)myAxes.Axis[axis].sRamp) / (double)myAxes.Axis[axis].velocity;   // Cruise-Phase
            myAxes.Axis[axis].T4_mplan = myAxes.Axis[axis].T3_mplan + myAxes.Axis[axis].T2_mplan;                           // Anfahr- und Bremsrampe sind immer symmetrisch mit gleicher Beschleunigung
            // die zeitlichen Grenzen der Bewegungssegmente in EtherCAT-Zyklen = ms:
            myAxes.Axis[axis].T1 = 0;                                               // relative Zeiten in ms/Zyklen, der Startwert ist daher immer 0
            myAxes.Axis[axis].T2 = (int)(1000.0 * myAxes.Axis[axis].T2_mplan);      // 1000 = Umrechnung in ms
            myAxes.Axis[axis].T3 = (int)(1000.0 * myAxes.Axis[axis].T3_mplan);
            myAxes.Axis[axis].T4 = (int)(1000.0 * myAxes.Axis[axis].T4_mplan);
        }
        else    // Dreiecksprofil fahren
        {
            myAxes.Axis[axis].T2_mplan = sqrt((double)abs(delta) / (double)myAxes.Axis[axis].acceleration);         // Beschleunigungs-Phase
            myAxes.Axis[axis].T3_mplan = myAxes.Axis[axis].T2_mplan;        // Mittelsegment mit konstanter Geschwindigkeit entfällt
            myAxes.Axis[axis].T4_mplan = 2 * myAxes.Axis[axis].T2_mplan;    // Brems-Phase

            myAxes.Axis[axis].T1 = 0;
            myAxes.Axis[axis].T2 = (int)(1000.0 * myAxes.Axis[axis].T2_mplan);      // 1000 = Umrechnung in ms
            myAxes.Axis[axis].T3 = (int)(1000.0 * myAxes.Axis[axis].T3_mplan);
            myAxes.Axis[axis].T4 = (int)(1000.0 * myAxes.Axis[axis].T4_mplan);
        }

        if(myAxes.otfAxisNr == axis+1)      // wenn die OTF Lead Axis bewegt wird, einen Capture Lauf beginnen
        {
            for(int i=0; i < myEncoderChannels.numEncoder; ++i)
                myEncoderChannels.Encoder[i].otfIndex = 0;          // löscht die OTF Daten Puffer
            myAxes.otfCapturing = true;
        }

        myAxes.Axis[axis].statusMove = 2;       // Plan scharf machen, springt hier von 3 auf 2

        // Logfile für die Moves:
        /*
        if(mlogfile.open(QIODevice::Append | QIODevice::Text))
        {
            QTextStream out(&mlogfile); // sammelt den gesamten Text auf, erst bei close() wird in die Datei geschrieben
            QString line = QString("%L1 S %L2 %L3  %L4 %L5 %L6\n").arg(QTime::currentTime().toString())
                    .arg(axis+1).arg(myAxes.Axis[axis].name)
                    .arg( myAxes.Axis[axis].positionStepsStart).arg(myAxes.Axis[axis].deltaSteps).arg(myAxes.Axis[axis].positionStepsTarget);
            out << line;
            mlogfile.close();
        }
        */
    }
}

// Kopie nur für Aufruf innerhalb der RT Schleife, die nimmt die Geschwindigkeit wie sie grade ist (also auch reduziert), nur für closed loop und automatische Traget Korrektur
void MovePlanRT(int axis, int targetSteps)
{
    int delta;

    myAxes.Axis[axis].positionStepsStart = myAxes.Axis[axis].positionSteps;    // Position zu Beginn des Moves merken
    // myAxis.Axis[axis].positionStepsTarget = targetSteps;         Ist das so richtig rausgenommen ???
    delta = targetSteps - myAxes.Axis[axis].positionStepsStart;

    if(delta > 0)
        myAxes.Axis[axis].direction = 1;       // Laufrichtung merken
    else
        myAxes.Axis[axis].direction = -1;

    myAxes.Axis[axis].deltaSteps = delta;         // Länge des Fahrwegs mit Vorzeichen merken
    // Weg der in einer der beiden Rampen eines Dreiecksprofils mit velocity als Spitzenwert zurückgelegt würde, in Steps (Umweg über double zur Vermeidung numerischen Überlaufs des Quadrates):
    myAxes.Axis[axis].sRamp = 0.5 * (double)myAxes.Axis[axis].velocity * (double)myAxes.Axis[axis].velocity / (double)myAxes.Axis[axis].acceleration;

    if(2 * myAxes.Axis[axis].sRamp < abs(delta))       // Trapezprofil fahren, hat Anteil mit konstanter der Achse maximal möglicher Geschwindigkeit
    {
        myAxes.Axis[axis].T2_mplan = (double)myAxes.Axis[axis].velocity / (double)myAxes.Axis[axis].acceleration;       // Beschleunigungs-Phase
        myAxes.Axis[axis].T3_mplan = myAxes.Axis[axis].T2_mplan + ((double)abs(delta) - 2.0 * (double)myAxes.Axis[axis].sRamp) / (double)myAxes.Axis[axis].velocity;   // Cruise-Phase
        myAxes.Axis[axis].T4_mplan = myAxes.Axis[axis].T3_mplan + myAxes.Axis[axis].T2_mplan;                           // Anfahr- und Bremsrampe sind immer symmetrisch mit gleicher Beschleunigung
        // die zeitlichen Grenzen der Bewegungssegmente in EtherCAT-Zyklen = ms:
        myAxes.Axis[axis].T1 = 0;                                               // relative Zeiten in ms/Zyklen, der Startwert ist daher immer 0
        myAxes.Axis[axis].T2 = (int)(1000.0 * myAxes.Axis[axis].T2_mplan);      // 1000 = Umrechnung in ms
        myAxes.Axis[axis].T3 = (int)(1000.0 * myAxes.Axis[axis].T3_mplan);
        myAxes.Axis[axis].T4 = (int)(1000.0 * myAxes.Axis[axis].T4_mplan);
    }
    else    // Dreiecksprofil fahren
    {
        myAxes.Axis[axis].T2_mplan = sqrt((double)abs(delta) / (double)myAxes.Axis[axis].acceleration);         // Beschleunigungs-Phase
        myAxes.Axis[axis].T3_mplan = myAxes.Axis[axis].T2_mplan;        // Mittelsegment mit konstanter Geschwindigkeit entfällt
        myAxes.Axis[axis].T4_mplan = 2 * myAxes.Axis[axis].T2_mplan;    // Brems-Phase

        myAxes.Axis[axis].T1 = 0;
        myAxes.Axis[axis].T2 = (int)(1000.0 * myAxes.Axis[axis].T2_mplan);      // 1000 = Umrechnung in ms
        myAxes.Axis[axis].T3 = (int)(1000.0 * myAxes.Axis[axis].T3_mplan);
        myAxes.Axis[axis].T4 = (int)(1000.0 * myAxes.Axis[axis].T4_mplan);
    }
    myAxes.Axis[axis].statusMove = 2;    // Plan scharf machen, dies springt immer von 1 auf 2 bei closed loop
}

// die Real Time Schleife
void *rt_Loop(void *arg)
{
    int     i, j, waitforBreak, posInt;

    uint32_t    tempCounts_uint32;          // temporäre Variable für das dWord = 4 Bytes Count vom Encoder
    long long   diffCounts;

    struct  timespec deadline, nowtime;
    double  deltaT;
    double  deltaUnits;
    int     statusGlobalRT, statusArgRT;    // lokale Variable in der RT_Loop, sammelt hier Fehler auf für den globalen Bewegungsstatus
    bool    limitEncoder;                   // lokale Variable für die globale Encoder Überwachung
    bool    isMoving;

    clock_gettime(CLOCK_MONOTONIC, &deadline);  // einmalig Startwert holen, da mit absoluter Zeit gerechnet wird

    while(1)
    {
        deadline.tv_nsec += 1000000;            // Das Inkrement ist die Zykluszeit der Echtzeitschleife in ns, hier 1e6 = 1 ms
        if(deadline.tv_nsec >= 1000000000) {    // Normalize the time to account for the second boundary
            deadline.tv_nsec -= 1000000000;
            deadline.tv_sec++;
        }
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &deadline, NULL);   // schlafen bis 1 ms rum ist
        ecatState.msCounter++;          //  läuft immer, zählt die Schleifendurchläufe und damit faktisch die Millisekunden seit Start des Programms
        ecatState.eCycleCounter++;      //  läuft immer, zählt die Schleifendurchläufe und damit faktisch die Millisekunden seit Start oder Restart eCat

        clock_gettime(CLOCK_MONOTONIC, &nowtime);       // Moment nach Aufwachen
        ecatState.deltaSleep = nowtime.tv_nsec - deadline.tv_nsec + (nowtime.tv_sec - deadline.tv_sec)*1000000000;
        ecatState.deltaSleep /= 1000;                   // auf µs
        if(ecatState.deltaSleep > ecatState.deltaSleepMax) ecatState.deltaSleepMax = ecatState.deltaSleep;

        if(ecatState.allSlavesOperational)  // wird in Startup_Ecat eingeschaltet, in Restart_Ecat erneuert
        {
            ec_send_processdata();

            clock_gettime(CLOCK_MONOTONIC, &nowtime);       // Moment nach senden des eCat Telegramms
            ecatState.deltaJitter = nowtime.tv_nsec - deadline.tv_nsec + (nowtime.tv_sec - deadline.tv_sec)*1000000000;
            ecatState.deltaJitter /= 1000;              // auf µs
            if(ecatState.deltaJitter > ecatState.deltaJitterMax) ecatState.deltaJitterMax = ecatState.deltaJitter;

            wkc = ec_receive_processdata(EC_TIMEOUTRET);

            if(wkc >= expectedWKC)
            {
                ecatState.workingCounterState = WKC_OK;

                //if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
                {
                    // gleich zu Anfang die Shuttersteuerung
                    if(sourceControl.processRunning && myAxes.Axis[sourceControl.TR_AxisNo-1].statusMove == 1)  // Shutter nur bei laufender Target Rotation öffnen
                    {
                        set_A760_01_B_Shutter(foundSlaves.A760_01_B_index[0], true);     // Shutter öffnen
                        sourceControl.shutterState = true;

                        if(sourceControl.intervalCounter > 0)
                            sourceControl.intervalCounter -= 1;
                        else
                        {
                            sourceControl.intervalCounter = 1000 / sourceControl.laserPulseRate;   // dies ist bei 100 Hz = 10
                            sourceControl.shotsCounter += 1;
                        }
                    }
                    else
                    {
                        set_A760_01_B_Shutter(foundSlaves.A760_01_B_index[0], false);    // Shutter schließen
                        sourceControl.shutterState = false;
                    }

                    // die Counts auslesen, wenn neue Werte zur verfügung stehen
                    if(get_A760_01_B_Status(foundSlaves.A760_01_B_index[0]) == 1)
                    {
                        get_A760_01_B_DetectorCounts(foundSlaves.A760_01_B_index[0], &sourceControl.counts_Q1, &sourceControl.counts_Q2, &sourceControl.counts_Q3, &sourceControl.counts_Q4);

                        //int numAverage = 50;

                        // nach links schieben
                        for(int i=0; i<sourceControl.numAverage-1; i++)
                        {
                            sourceControl.counts_Q1A[i] = sourceControl.counts_Q1A[i+1];
                            sourceControl.counts_Q2A[i] = sourceControl.counts_Q2A[i+1];
                            sourceControl.counts_Q3A[i] = sourceControl.counts_Q3A[i+1];
                            sourceControl.counts_Q4A[i] = sourceControl.counts_Q4A[i+1];
                        }
                        // neue Werte am Ende des Array eintragen
                        sourceControl.counts_Q1A[sourceControl.numAverage-1] = sourceControl.counts_Q1;
                        sourceControl.counts_Q2A[sourceControl.numAverage-1] = sourceControl.counts_Q2;
                        sourceControl.counts_Q3A[sourceControl.numAverage-1] = sourceControl.counts_Q3;
                        sourceControl.counts_Q4A[sourceControl.numAverage-1] = sourceControl.counts_Q4;
                        // Variablen für Mittelwert recyclen
                        sourceControl.counts_Q1 = 0;
                        sourceControl.counts_Q2 = 0;
                        sourceControl.counts_Q3 = 0;
                        sourceControl.counts_Q4 = 0;
                        // Mittelwert bilden
                        for(int i=0; i<sourceControl.numAverage; i++)
                        {
                            sourceControl.counts_Q1 += sourceControl.counts_Q1A[i];
                            sourceControl.counts_Q2 += sourceControl.counts_Q2A[i];
                            sourceControl.counts_Q3 += sourceControl.counts_Q3A[i];
                            sourceControl.counts_Q4 += sourceControl.counts_Q4A[i];
                        }
                        sourceControl.counts_dQ1 = (double)sourceControl.counts_Q1 / (double)sourceControl.numAverage;
                        sourceControl.counts_dQ2 = (double)sourceControl.counts_Q2 / (double)sourceControl.numAverage;
                        sourceControl.counts_dQ3 = (double)sourceControl.counts_Q3 / (double)sourceControl.numAverage;
                        sourceControl.counts_dQ4 = (double)sourceControl.counts_Q4 / (double)sourceControl.numAverage;

                        sourceControl.counts_dQ1 -= sourceControl.offset_Q1;
                        sourceControl.counts_dQ2 -= sourceControl.offset_Q2;
                        sourceControl.counts_dQ3 -= sourceControl.offset_Q3;
                        sourceControl.counts_dQ4 -= sourceControl.offset_Q4;

                        double qSum = sourceControl.counts_dQ1 + sourceControl.counts_dQ2 + sourceControl.counts_dQ3 + sourceControl.counts_dQ4;
                        if(qSum == 0) qSum = 1;     // Division durch Null vermeiden
                        sourceControl.QX = (sourceControl.counts_dQ2 + sourceControl.counts_dQ3 - sourceControl.counts_dQ1 - sourceControl.counts_dQ4) / qSum;
                        sourceControl.QY = (sourceControl.counts_dQ1 + sourceControl.counts_dQ2 - sourceControl.counts_dQ3 - sourceControl.counts_dQ4) / qSum;

                        // nur bei geöffnetem Shutter sinnvolle Counts für die Korrektur, wenn TC schon fährt hier nichts machen
                        if(sourceControl.shutterState && !sourceControl.TC_Flag)
                        {
                            if(sourceControl.QX > sourceControl.TC_Limit)
                            {
                                sourceControl.TC_Flag = true;                                       // move im Gui signalisieren
                                sourceControl.TC_delta_steps_temp  = sourceControl.TC_delta_steps;  // der Move wird am Ende der Axis-Schleife geplant

                            }
                            if(sourceControl.QX < -sourceControl.TC_Limit)
                            {
                                sourceControl.TC_Flag = true;                                       // move im Gui signalisieren
                                sourceControl.TC_delta_steps_temp  = -sourceControl.TC_delta_steps; // der Move wird am Ende der Axis-Schleife geplant
                            }
                        }
                    }
                }


                // vorhandene 701 Encoder Module auslesen und auf Units umrechnen, Trigger Status setzen
                for(i=0; i < foundSlaves.ECM701count; i++)
                {
                    myEncoderChannels.Encoder[4*i].revision = get_ECM701_Revision(foundSlaves.ECM701index[i]);      // Versionsnummer des Moduls
                    set_ECM701_TrigMode(foundSlaves.ECM701index[i], myEncoderChannels.state_Trigger_Resolute);      // alle ECM mit gleicher Triggereinstellung
                    myEncoderChannels.Encoder[4*i].trigCount = get_ECM701_TrigCount(foundSlaves.ECM701index[i]);    // Zähler für alle Module auslesen, ist nur einer für je 4 Kanäle, Index beginnt bei Null
                    for(j=0; j<4; j++)  // durch alle 4 Kanäle jedes Moduls iterieren
                    {
                        int index = 4*i+j;
                        //  Die Counts werden immer unverändert übernommen:
                        myEncoderChannels.Encoder[index].encState  = get_ECM701_EncoderCounts(foundSlaves.ECM701index[i], j+1, &tempCounts_uint32);
                        myEncoderChannels.Encoder[index].counts = tempCounts_uint32;           //  auf qint64 erweitern, signed ist hier Ok!

                        //  nur hier werden die Counts in Units umgerechnet:
                        diffCounts = myEncoderChannels.Encoder[index].counts - myEncoderChannels.Encoder[index].offsetCounts;

                        //  Sonderbehandlung führ Drehachsen mit Encoder Ring:
                        if(myEncoderChannels.Encoder[index].encoderModus == ENCMODUS_R180 || myEncoderChannels.Encoder[index].encoderModus == ENCMODUS_R200)  // ist REXA Ring, wird auf +/- 180 Grad Bereich gezwungen
                        {
                            if(diffCounts > ZWEI_E31)       // größer +180 Grad ?
                                diffCounts -= ZWEI_E32;     // um exakt -360 Grad ins Negative drehen
                            if(diffCounts < -ZWEI_E31)
                                diffCounts += ZWEI_E32;
                        }
                        if( myEncoderChannels.Encoder[index].encoderModus == ENCMODUS_R200)         // hier sind sogar +/- 200 Grad möglich
                        {
                            // sicherer Auslösebereich ohne Mehrdeutigkeiten:
                            if(diffCounts > 140*ZWEI_E31/180 && diffCounts < 159*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState != 1)   // > 150 Grad ?
                            {
                                myEncoderChannels.Encoder[index].rotationState = +1;
                                flags.doWriteRotationState = true;    // keine Dateizugriffe in der RT Routine, nur Marker setzen !!
                            }
                            if(diffCounts < -140*ZWEI_E31/180 && diffCounts > -159*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState != -1)   // < -150 Grad ?
                            {
                                myEncoderChannels.Encoder[index].rotationState = -1;
                                flags.doWriteRotationState = true;
                            }
                            if(diffCounts > 160*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState == -1)
                                diffCounts -= ZWEI_E32;
                            if(diffCounts < -160*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState == +1)
                                diffCounts += ZWEI_E32;
                        }
                        myEncoderChannels.Encoder[index].positionUnits = myEncoderChannels.Encoder[index].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[index].offsetUnits; // zusätzlicher Offset!!
                    }
                }

                // vorhandene 713 Encoder Module auslesen und auf Units umrechnen, Trigger Status/Count wird hier nicht verwendet
                // Auch die Outputs versorgen:
                // Genau hier wird auch der Motorstrom laufend aktualisiert
                for(i=0; i < foundSlaves.ECM713count; i++)
                {
                    int index_enc = 4*foundSlaves.ECM701count + i;          // Zeiger auf den Encoder Eintrag
                    int index_axis = 4*foundSlaves.ECM712count + i;         // Zeiger auf den Achsen Eintrag

                    if(myAxes.Axis[index_axis].statusMove == 1)             // Achse läuft
                        set_ECM713_current(foundSlaves.ECM713index[i], myAxes.Axis[index_axis].motorCurrentRun);        // den Laufstrom ständig aktualisieren
                    else
                        set_ECM713_current(foundSlaves.ECM713index[i], myAxes.Axis[index_axis].motorCurrentStop);       // den Haltestrom ständig aktualisieren

                    myAxes.Axis[index_axis].motorStatus = get_ECM713_MotorStatus(foundSlaves.ECM713index[i]);           // den Status auch

                    myEncoderChannels.Encoder[index_enc].revision = get_ECM713_Revision(foundSlaves.ECM713index[i]);       // Versionsnummer des Moduls
                    //  Die Counts werden immer unverändert übernommen:
                    myEncoderChannels.Encoder[index_enc].encState = get_ECM713_EncoderCounts(foundSlaves.ECM713index[i], &tempCounts_uint32);
                    myEncoderChannels.Encoder[index_enc].counts = tempCounts_uint32;           //  auf qint64 erweitern, signed ist hier Ok!

                    //  nur hier werden die Counts in Units umgerechnet:
                    diffCounts = myEncoderChannels.Encoder[index_enc].counts - myEncoderChannels.Encoder[index_enc].offsetCounts;

                    //  Sonderbehandlung führ Drehachsen mit Encoder Ring:
                    if(myEncoderChannels.Encoder[index_enc].encoderModus == ENCMODUS_R180 || myEncoderChannels.Encoder[index_enc].encoderModus == ENCMODUS_R200)  // ist REXA Ring, wird auf +/- 180 Grad Bereich gezwungen
                    {
                        if(diffCounts > ZWEI_E31)       // größer +180 Grad ?
                            diffCounts -= ZWEI_E32;     // um exakt -360 Grad ins Negative drehen
                        if(diffCounts < -ZWEI_E31)
                            diffCounts += ZWEI_E32;
                    }
                    if( myEncoderChannels.Encoder[index_enc].encoderModus == ENCMODUS_R200)         // hier sind sogar +/- 200 Grad möglich
                    {
                        // sicherer Auslösebereich ohne Mehrdeutigkeiten:
                        if(diffCounts > 140*ZWEI_E31/180 && diffCounts < 159*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState != 1)   // > 150 Grad ?
                        {
                            myEncoderChannels.Encoder[index_enc].rotationState = +1;
                            flags.doWriteRotationState = true;    // keine Dateizugriffe in der RT Routine, nur Marker setzen !!
                        }
                        if(diffCounts < -140*ZWEI_E31/180 && diffCounts > -159*ZWEI_E31/180 && myEncoderChannels.Encoder[4*i+j].rotationState != -1)   // < -150 Grad ?
                        {
                            myEncoderChannels.Encoder[index_enc].rotationState = -1;
                            flags.doWriteRotationState = true;
                        }
                        if(diffCounts > 160*ZWEI_E31/180 && myEncoderChannels.Encoder[index_enc].rotationState == -1)
                            diffCounts -= ZWEI_E32;
                        if(diffCounts < -160*ZWEI_E31/180 && myEncoderChannels.Encoder[index_enc].rotationState == +1)
                            diffCounts += ZWEI_E32;
                    }
                    myEncoderChannels.Encoder[index_enc].positionUnits = myEncoderChannels.Encoder[index_enc].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[index_enc].offsetUnits; // zusätzlicher Offset!!
                }


                for(i=0; i < foundSlaves.ECM702count; i++)      // vorhandene 702 Incremental Encoder Module auslesen und auf Units umrechnen, Control Byte setzen
                {
                    int index = 4* foundSlaves.ECM701count+ foundSlaves.ECM713count + 2*i;  // index auf den je ersten Kanal eines ECM-702-Moduls
                    int controlByte = myEncoderChannels.Encoder[index].referencing + 16*myEncoderChannels.Encoder[index + 1].referencing;     // Verhalten beim Überfahren der Referenzmarke
                    controlByte += 2*myEncoderChannels.Encoder[index].trigpolNegative + 32*myEncoderChannels.Encoder[index + 1].trigpolNegative;                // zweites Bit ist Dummy und macht nichts, Trigger für beide Kanäle gemeinsam
                    set_ECM702_ControlByte(foundSlaves.ECM702index[i], controlByte);
                    myEncoderChannels.Encoder[index].revision = get_ECM702_Revision(foundSlaves.ECM702index[i]);       // Versionsnummer des Moduls
                    for(j=0; j<2; j++)  // durch alle 2 Kanäle jedes Moduls iterieren
                    {
                        int index_enc = 4*foundSlaves.ECM701count + foundSlaves.ECM713count + 2*i+j;
                        //  Die Counts werden immer unverändert übernommen:
                        tempCounts_uint32 = get_ECM702_EncoderCounts(foundSlaves.ECM702index[i], j+1);
                        myEncoderChannels.Encoder[index_enc].counts = (int32_t)tempCounts_uint32;       //  Mit Vorzeichen auf qint64 erweitern, Werte vom Tonic Encoder sind immer mit Vorzeichen (das 32. Bit)
                        myEncoderChannels.Encoder[index_enc].encStateRaw = get_ECM702_Status(foundSlaves.ECM702index[i], j+1);  // beinhaltet auch Zero Marke und Trigger:Bit012 = Z A T
                        if(myEncoderChannels.Encoder[index_enc].encStateRaw & ALARM_BIT)
                            myEncoderChannels.Encoder[index_enc].encState = ENCSTATE_READ_ERROR;     // kein Signal = Error
                        else
                            if(myEncoderChannels.Encoder[index_enc].encStateRaw & ZERO_BIT)     // kein Alarm und Zero = wieder OK
                                myEncoderChannels.Encoder[index_enc].encState = ENCSTATE_OK;
                            else
                                if(myEncoderChannels.Encoder[index_enc].encState == ENCSTATE_READ_ERROR)
                                    myEncoderChannels.Encoder[index_enc].encState = ENCSTATE_ZEROLOST;  // kein Alarm, aber es war mal einer

                        //  nur hier werden die Counts in Units umgerechnet:
                        diffCounts = myEncoderChannels.Encoder[index_enc].counts - myEncoderChannels.Encoder[index_enc].offsetCounts;
                        myEncoderChannels.Encoder[index_enc].positionUnits = myEncoderChannels.Encoder[index_enc].units_per_count * (double)(diffCounts) + myEncoderChannels.Encoder[index_enc].offsetUnits; // zusätzlicher Offset!!

                        //  hier die otf capture Werte speichern
                        if(myAxes.otfCapturing)
                        {
                            if(myEncoderChannels.Encoder[index_enc].encStateRaw & TRIGGER_BIT)  // Das TRIGGER_BIT ist für beide Kanäle eines ECM-702 immer gleich
                            {
                                tempCounts_uint32 = get_ECM702_otfCaptureCounts(foundSlaves.ECM702index[i], j+1);
                                myEncoderChannels.Encoder[index_enc].otfCounts[myEncoderChannels.Encoder[index_enc].otfIndex] = (int32_t)tempCounts_uint32;
                                myEncoderChannels.Encoder[index_enc].otfIndex += 1;
                                if(myEncoderChannels.Encoder[index_enc].otfIndex >= MAXOTFSAMPLES)
                                {
                                    myAxes.otfCapturing = false;    // Notbremse, damit das Array nicht überlaufen kann
                                    myAxes.otfReady = true;         // den abgrebrochenen / unvollständigen Scan trotzdem bereit stellen
                                }
                            }
                        }
                    }
                }


                // Alle Arten von Encodern wurden ausgelesen, hier nun der gleitende Mittelwert fürs Logging:
                for(int enc = 0; enc < myEncoderChannels.numEncoder; enc++)
                {
                   for(i = 0; i < AVERAGEENCODER - 1; ++i)
                    {
                        myEncoderChannels.Encoder[enc].countsLast[i] = myEncoderChannels.Encoder[enc].countsLast[i+1];      // nach links schieben
                    }
                    myEncoderChannels.Encoder[enc].countsLast[AVERAGEENCODER - 1] = myEncoderChannels.Encoder[enc].counts;   // letzten Messwert eintragen

                    long long temp = 0;
                    for(i = 0; i < AVERAGEENCODER; ++i)
                    {
                        temp +=  myEncoderChannels.Encoder[enc].countsLast[i];  // nach links schieben
                    }
                    temp /= AVERAGEENCODER;
                    myEncoderChannels.Encoder[enc].counts_mean = temp;
                }


                statusGlobalRT = GSTATUS_IDLE;         // Reset der Variable vor der Iteration über alle Achsen, sammelt im Folgenden den Status nach steigender Priorität ein
                statusArgRT = 0;

                // alle Encoder auf Fehler abchecken, wenn auch nur einer nicht stimmt globaler Stop, auch bei einer Deichsel schief
                limitEncoder = false;   // Flag für Global Stop
                for(i=0; i < myEncoderChannels.numEncoder; i++)
                {
                    //if(myEncoderChannels.Encoder[i].name == "NU")
                    if(myEncoderChannels.Encoder[i].isUsed) // isUsed = Fehlerbehandlung aktiv
                    {
                        if(myEncoderChannels.Encoder[i].encState < ENCSTATE_WARN)   // alles unter Warning zählt als Fehler
                        {
                            limitEncoder = true;
                            if(myEncoderChannels.Encoder[i].encState == ENCSTATE_ZEROLOST)
                                statusGlobalRT = GSTATUS_ENCODER_ZEROLOST;
                            else
                                statusGlobalRT = GSTATUS_ENCODER_ERROR;   // genutzter Encoder nicht angeschlossen oder gestört
                            statusArgRT = i+1;
                        }
                        else
                        {
                            if(myEncoderChannels.Encoder[i].encoderModus == ENCMODUS_GANTRY) // Überschreiten der Grenzen nur bei Deichseln (in beide Richtungen)als Error werten
                            {
                                if((myEncoderChannels.Encoder[i].limitHigh != 0) && (myEncoderChannels.Encoder[i].counts > myEncoderChannels.Encoder[i].limitHigh))
                                {
                                    limitEncoder = true;
                                    statusGlobalRT = GSTATUS_ELH;
                                    statusArgRT = i+1;
                                }
                                if((myEncoderChannels.Encoder[i].limitLow != 0) && (myEncoderChannels.Encoder[i].counts < myEncoderChannels.Encoder[i].limitLow))
                                {
                                    limitEncoder = true;
                                    statusGlobalRT = GSTATUS_ELL;
                                    statusArgRT = i+1;
                                }
                            }
                        }
                    }
                }
                myAxes.errorEncoderGlobal = limitEncoder;

                //if(!myAxes.ignoreLimits && myAxes.errorEncoderGlobal)
                //    StopAllAxis();   // alle Achsen sofort stoppen

                // bei Fehlern in einem oder mehreren Encodern (zerolost zählt als Fehler) dürfen alle Closed-Loop Moves (to position) auslaufen,
                // nur die logarithmische Annäherungskette wird abgebrochen.
                // Open-Loop Moves auf Steps sind weiterhin möglich.
                if(myAxes.errorEncoderGlobal)
                {
                    myAxes.isStabilizedGlobal = false;                  // Stabilisierung deaktivieren
                    for(int i = 0; i < myAxes.numAxis; ++i)
                    {
                        myAxes.Axis[i].isMovingClosedLoop = false;   // closed-loop Achsen kontrolliert auslaufen lassen
                    }
                }

                for(i=0; i < foundSlaves.ECM712count; i++)  // die Endlagenschalter auslesen
                {
                    for(j=0; j<4; j++)  // durch alle 4 Kanäle jedes Indexer Moduls iterieren
                    {
                        int index_axis = 4*i+j;
                        get_ECM712_Limits(foundSlaves.ECM712index[i], j+1, &myAxes.Axis[index_axis].limitReverse, &myAxes.Axis[index_axis].limitForward);
                    }
                }

                for(i=0; i < foundSlaves.ECM713count; i++)  // auch die Endlagenschalter der integrierten Endstufen auslesen
                {
                    int index_axis = 4*foundSlaves.ECM712count + i;
                    get_ECM713_Limits(foundSlaves.ECM713index[i], &myAxes.Axis[index_axis].limitReverse, &myAxes.Axis[index_axis].limitForward, &myAxes.Axis[index_axis].limitAux1, &myAxes.Axis[index_axis].limitAux2);
                }

                // Collision Control Regeln abarbeiten:
                for(i=0; i < collisionControl.numRules; i++)
                {
                    if(collisionControl.sensAxisIndex[i] > 0 && collisionControl.sensAxisIndex[i] <= myAxes.numAxis)
                    {
                        if( (myAxes.Axis[collisionControl.sensAxisIndex[i] - 1].limitAux1 && (collisionControl.auxSelect[i] == 1))
                                || (myAxes.Axis[collisionControl.sensAxisIndex[i] - 1].limitAux2 && (collisionControl.auxSelect[i] == 2)) )
                        {
                            if(collisionControl.targetAxisRoF[i] == -1)
                                myAxes.Axis[collisionControl.targetAxisIndex[i] - 1].limitReverse = true;
                            if(collisionControl.targetAxisRoF[i] == 1)
                                myAxes.Axis[collisionControl.targetAxisIndex[i] - 1].limitForward = true;
                        }
                    }
                }

                //  hier wird die Bewegung ausgeführt, es können mehrere oder alle Achsen gleichzeitig laufen
                isMoving = false;
                for(int axis = 0; axis < myAxes.numAxis; axis++)    // alle Achsen abarbeiten
                {
                    //double aUnits, bUnits;
                    //long long aCounts, bCounts;

                    // Initialisierte Achse läuft aus dem Schalter wieder raus, bei Verlassen des Limit anhalten und Steps auf Null setzen:
                    if(myAxes.Axis[axis].isIniting == 2)
                    {
                        if((myAxes.Axis[axis].initDirection == -1 && !myAxes.Axis[axis].limitReverse) || (myAxes.Axis[axis].initDirection == 1 && !myAxes.Axis[axis].limitForward))
                        {
                            // Schalter wurde wieder verlassen
                            myAxes.Axis[axis].isIniting = 1;                                            // Limit wurde wieder verlassen
                            myAxes.Axis[axis].statusMove = 0;
                            myAxes.Axis[axis].positionStepsOffset  -= myAxes.Axis[axis].positionSteps;  // Internen Zähler des Indexer merken, das -= ist der Trick, die Offsets müssen verkettet werden
                            myAxes.Axis[axis].positionSteps = 0;                                        // Reset der Steps auf Null, dies ist der neue Bezug für die Achse
                            myAxes.Axis[axis].positionStepsTarget = 0;                                  // damit deltajog auf der letzten Position aufbaut
                            flags.refreshMoveFinished = true;                                           // Anzeige aktualisieren
                            if(myAxes.Axis[axis].initSteps > 0)
                            {
                                // jetzt noch ein Stück fahren, wenn bestellt, zB um die Zero Marken der Tonics zu finden
                                MovePlanRT(axis, myAxes.Axis[axis].positionSteps - myAxes.Axis[axis].initSteps * myAxes.Axis[axis].initDirection);
                            }
                            else
                            {
                                myAxes.Axis[axis].isIniting = 0;    // Init Prozess beenden
                                if(myAxes.Axis[axis].indexEncoder_A > 0)
                                    myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_A-1].referencing = 0;    // Zero on Mark deaktivieren
                                if(myAxes.Axis[axis].indexEncoder_B > 0)
                                    myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_B-1].referencing = 0;    // Zero on Mark deaktivieren
                            }
                        }
                    }

                    // sofortiger Stop der betreffenden Achse, wenn diese in den Limit Switch läuft, außer bei ignore, dies stopt auch eine Achse die gerade initialisiert:
                    if(myAxes.Axis[axis].limitForward)
                    {
                        //if(myAxes.Axis[axis].name != "NU" && !myAxes.Axis[axis].isGantry)   // Gantry-Achsen nicht als Summenfehler Limit anzeigen
                        if(myAxes.Axis[axis].isUsed && !myAxes.Axis[axis].isGantry)     // Gantry-Achsen nicht als Summenfehler Limit anzeigen
                        {
                            statusGlobalRT = GSTATUS_FLIMIT;
                            statusArgRT = axis+1;
                        }
                        if(!myAxes.ignoreLimits && myAxes.Axis[axis].direction == 1)
                        {
                            if(myAxes.Axis[axis].statusMove != 0)  // Abfrage wichtig, sonst wird der Protokoll Nachlauf zu einer Endlosschleife
                            {
                                myAxes.Axis[axis].statusMove = 0;
                                myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps;    // damit deltajog nach NotStop auf der letzten Position aufbaut
                                myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnits;
                                myAxes.Axis[axis].isMovingClosedLoop = false;
                                flags.refreshMoveFinished = true;                                           // Anzeige aktualisieren, wirklich??
                            }
                        }
                    }
                    if(myAxes.Axis[axis].limitReverse)
                    {
                        if(myAxes.Axis[axis].isUsed && !myAxes.Axis[axis].isGantry)
                        {
                            statusGlobalRT = GSTATUS_RLIMIT;
                            statusArgRT = axis+1;
                        }
                        if(!myAxes.ignoreLimits && myAxes.Axis[axis].direction == -1)
                        {
                            if(myAxes.Axis[axis].statusMove != 0)  // Abfrage wichtig, sonst wird der Protokoll Nachlauf zu einer Endlosschleife
                            {
                                myAxes.Axis[axis].statusMove = 0;
                                myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps;    // damit deltajog nach NotStop auf der letzten Position aufbaut
                                myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnits;
                                myAxes.Axis[axis].isMovingClosedLoop = false;
                                flags.refreshMoveFinished = true;                                           // Anzeige aktualisieren
                             }
                        }
                    }

                    // Nur Encoder A genutzt
                    if((myAxes.Axis[axis].indexEncoder_A > 0) && (myAxes.Axis[axis].indexEncoder_B == 0))
                    {
                        myAxes.Axis[axis].positionUnits = myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_A -1 ].positionUnits;
                        myAxes.Axis[axis].positionCounts = myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_A -1 ].counts;
                    }

                    // Nur Encoder B genutzt
                    if((myAxes.Axis[axis].indexEncoder_B > 0) && (myAxes.Axis[axis].indexEncoder_A == 0))
                    {
                        myAxes.Axis[axis].positionUnits = myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_B -1 ].positionUnits;
                        myAxes.Axis[axis].positionCounts = myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_B -1 ].counts;
                    }

                    // Beide Encoder A und B genutzt, dies aktiviert implizit Mittelwertbildung, feedbackType wird hier nicht mehr genutzt
                    if((myAxes.Axis[axis].indexEncoder_A > 0) && (myAxes.Axis[axis].indexEncoder_B > 0))
                    {
                        myAxes.Axis[axis].positionUnits = 0.5 * (myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_A -1 ].positionUnits
                                + myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_B -1 ].positionUnits);

                        myAxes.Axis[axis].positionCounts = (myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_A -1 ].counts
                                + myEncoderChannels.Encoder[ myAxes.Axis[axis].indexEncoder_B -1 ].counts)/2;
                    }

                    // Kein Encoder genutzt
                    if( (myAxes.Axis[axis].indexEncoder_A == 0) && (myAxes.Axis[axis].indexEncoder_B == 0) )    // keine Encoder vorhanden
                    {
                        // Synthetische Units aus den Steps berechnen, fürs GUI und Borys
                        myAxes.Axis[axis].positionUnits = (double)myAxes.Axis[axis].positionSteps / myAxes.Axis[axis].steps_per_unit;
                    }

                    /*
                    switch(myAxes.Axis[axis].feedbackType)
                    {
                    case 0: // A
                        myAxes.Axis[axis].positionUnits = aUnits;
                        myAxes.Axis[axis].positionCounts = aCounts;
                        break;
                    case 1: // B
                        myAxes.Axis[axis].positionUnits = bUnits;
                        myAxes.Axis[axis].positionCounts = bCounts;
                        break;
                    case 2: // Mittelwert von A und B
                        myAxes.Axis[axis].positionUnits = 0.5 * (aUnits + bUnits);
                        //aCounts = aCounts/2;
                        //bCounts = bCounts/2;
                        myAxes.Axis[axis].positionCounts = aCounts/2 + bCounts/2;   // mit qint64 wird overflow bei der Addition vermieden
                        break;
                    }
                    */

                    // gleitenden Mittelwert der Achs-Positionen bilden, dies ist *nicht* atomar und damit auch nicht thread sicher !!
                    // die positionUnistsLast dürfen nicht
                    for(i = 0; i < myAxes.Axis[axis].CL_Average-1; ++i)
                    {
                        myAxes.Axis[axis].positionUnitsLast[i] = myAxes.Axis[axis].positionUnitsLast[i+1];  // nach links schieben
                    }
                    myAxes.Axis[axis].positionUnitsLast[myAxes.Axis[axis].CL_Average-1] = myAxes.Axis[axis].positionUnits;   // letzten Messwert eintragen

                    double positionUnitsAverageLocal = 0;
                    for(i = 0; i < myAxes.Axis[axis].CL_Average; ++i)
                    {
                        positionUnitsAverageLocal += myAxes.Axis[axis].positionUnitsLast[i];  // nach links schieben
                    }
                    positionUnitsAverageLocal /= myAxes.Axis[axis].CL_Average;

                    // wenn gerade in MoveToUnits unterbrochen wurde, wird auf ein Update verzichtet:
                    if(myAxes.puaBlock)
                        myAxes.puaSplit = true;  // Diagnostik Flag setzen
                    else
                        myAxes.Axis[axis].positionUnitsAverage = positionUnitsAverageLocal;

                    if(myAxes.Axis[axis].statusMove == 0 && myAxes.Axis[axis].lastMoveTailDowncount > 0)   // Nachlauf des Protokolls beim Übergang von 1 auf 0
                    {
                        //myAxis.Axis[axis].lastMoveStepsDouble[myAxis.Axis[axis].indexPath] = myAxis.Axis[axis].posDouble; // nur für Diagnostik
                        myAxes.Axis[axis].lastMoveSteps[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].positionSteps;  // Bewegung aller Achsen immer protokollieren

                        if(axis < 4*foundSlaves.ECM712count)
                            myAxes.Axis[axis].lastMoveStepsIst[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].motor_direction * get_ECM712_StepsIst(foundSlaves.ECM712index[axis/4], axis%4 +1);    // rücklesen der PID des Indexers
                        else
                            myAxes.Axis[axis].lastMoveStepsIst[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].motor_direction * get_ECM713_StepsIst(foundSlaves.ECM713index[axis - 4*foundSlaves.ECM712count]);    // rücklesen der PID des Indexers

                        myAxes.Axis[axis].lastMovePosition[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].positionUnits;
                        myAxes.Axis[axis].lastMovePositionAverage[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].positionUnitsAverage;
                        if(flagsDebug.enc > 0)  // für Diagnostik einen beliebigen anderen Encoder-Kanal mit aufzeichnen
                            myEncoderChannels.lastMoveDebugEnc[myAxes.Axis[axis].indexPath] = myEncoderChannels.Encoder[flagsDebug.enc-1].positionUnits;
                        if(flagsDebug.encStatus > 0)  // für Diagnostik einen beliebigen anderen Encoder-Status mit aufzeichnen
                            myEncoderChannels.lastMoveDebugEncStatus[myAxes.Axis[axis].indexPath] = myEncoderChannels.Encoder[flagsDebug.encStatus-1].encState;

                        myAxes.Axis[axis].indexPath += 1;
                        if(myAxes.Axis[axis].indexPath < MAXPATHPOINTS - 1)
                            myAxes.Axis[axis].lastMoveTailDowncount -= 1;
                        else
                            myAxes.Axis[axis].lastMoveTailDowncount = 0;    // Ende des Arrays erreicht, Protokollierung beenden
                        //diag_StepsIst = get_ECM712_StepsIst(foundSlaves.ECM712index[axis/4], axis%4 +1);    // mitlaufende Kontrolle im GUI, nur Debug
                        isMoving = true;    // Nachlauf als Bestandteil des Moves behandeln
                    }

                    if(myAxes.Axis[axis].statusMove == 3)   // Bewegung vorbereiten, Stab abbrechen
                    {
                        // nichts zu tun, soll nur Stab blockieren
                    }

                    if(myAxes.Axis[axis].statusMove == 2)   // Bewegung initialisieren
                    {                     
                        waitforBreak = 0;               // sofort loslegen, ist Rest des automatischen Bremsens (nicht mehr implementiert)

                        myAxes.Axis[axis].T1 += ecatState.eCycleCounter + waitforBreak;   // von relativen zu absoluten Zeiten, in Einheiten von 1 ms (EtherCAT Zyklus)
                        myAxes.Axis[axis].T2 += ecatState.eCycleCounter + waitforBreak;
                        myAxes.Axis[axis].T3 += ecatState.eCycleCounter + waitforBreak;
                        myAxes.Axis[axis].T4 += ecatState.eCycleCounter + waitforBreak;

                        myAxes.Axis[axis].T2_mplan += 0.001 * (double)(ecatState.eCycleCounter + waitforBreak);
                        myAxes.Axis[axis].T3_mplan += 0.001 * (double)(ecatState.eCycleCounter + waitforBreak);
                        myAxes.Axis[axis].T4_mplan += 0.001 * (double)(ecatState.eCycleCounter + waitforBreak);

                        myAxes.Axis[axis].positionUnitsStart = myAxes.Axis[axis].positionUnits; // Position in Units (vom Encoder) zu Beginn des Moves merken, dies wird für den Schleppfehler benötigt

                        myAxes.Axis[axis].statusMove = 1;   // Bewegungszyklus beginnen
                    }

                    if(myAxes.Axis[axis].statusMove == 1)   // Achse läuft
                    {
                        isMoving = true;    // mindestens eine Achse läuft
                        if(ecatState.eCycleCounter > myAxes.Axis[axis].T4)          // Echt größer, Bewegung eines Submove ist abgeschlossen, Achse ruht
                        {
                            if(axis == sourceControl.TC_AxisNo-1)
                            {
                                sourceControl.TC_Flag = false;       // für Achse Target Korrektur Ende move im Gui signalisieren, auch bei versehentlichem closed loop
                            }

                            if(myAxes.Axis[axis].isMovingClosedLoop)    // Closed Loop für diese eine Move Kette aktiv
                            {
                                if(myAxes.Axis[axis].CL_WaitMoveDowncount > 0)   // Wartezeit zwischen Submoves abwarten
                                {
                                    myAxes.Axis[axis].CL_WaitMoveDowncount -= 1;
                                }
                                else    // Wartezeit abgelaufen
                                {
                                    myAxes.Axis[axis].CL_WaitMoveDowncount = myAxes.Axis[axis].CL_WaitMove;     // Countdown wieder scharf machen
                                    deltaUnits = myAxes.Axis[axis].positionUnitsTarget - myAxes.Axis[axis].positionUnitsAverage;
                                    if(fabs(deltaUnits) > myAxes.Axis[axis].CL_DeltaMove && myAxes.Axis[axis].CL_downcountIterations > 0)  // Toleranzintervall noch nicht erreicht, nächster Submove:
                                    {
                                        myAxes.Axis[axis].CL_downcountIterations -= 1;
                                        MovePlanRT(axis, myAxes.Axis[axis].positionSteps + (int)(deltaUnits * myAxes.Axis[axis].CL_Approach * myAxes.Axis[axis].steps_per_unit)); // setzt status wieder auf 2 hoch
                                    }
                                    else    // closed loop Move Kette ist abgeschlossen, Toleranzintervall erreicht:
                                    {
                                        StopAxisRT(axis);
                                    }
                                }
                            }
                            else    // kein Closed Loop, nach einem Move ist dann immer Schluß
                            {
                                StopAxisRT(axis);
                            }
                        }
                        else if(ecatState.eCycleCounter >= myAxes.Axis[axis].T3)     // Bremsphase
                        {
                            deltaT = myAxes.Axis[axis].T4_mplan - 0.001 * (double)(ecatState.eCycleCounter);    // in Sekunden
                            double posDouble = myAxes.Axis[axis].positionStepsStart + myAxes.Axis[axis].deltaSteps - 0.5 * myAxes.Axis[axis].direction * myAxes.Axis[axis].acceleration * deltaT * deltaT;
                            if(posDouble > 0)
                                myAxes.Axis[axis].posDouble = posDouble  + 0.5;   //aufrunden, damit Zielwert auch erreicht wird
                            if(posDouble < 0)
                                myAxes.Axis[axis].posDouble = posDouble  - 0.5;   //abrunden, damit Zielwert auch erreicht wird
                        }
                        else if(ecatState.eCycleCounter >= myAxes.Axis[axis].T2)    // Cruise mit konstanter Geschwindigkeit
                        {
                            deltaT = 0.001 * (double)(ecatState.eCycleCounter) - myAxes.Axis[axis].T2_mplan;    // in Sekunden
                            myAxes.Axis[axis].posDouble = myAxes.Axis[axis].positionStepsStart + myAxes.Axis[axis].direction * myAxes.Axis[axis].sRamp + myAxes.Axis[axis].direction * myAxes.Axis[axis].velocity * deltaT;
                        }
                        else if(ecatState.eCycleCounter >= myAxes.Axis[axis].T1)    // Beschleunigungsphase, muss bei exakt = T1 starten!
                        {
                            deltaT = 0.001 * (double)(ecatState.eCycleCounter - myAxes.Axis[axis].T1);    // in Sekunden
                            myAxes.Axis[axis].posDouble = myAxes.Axis[axis].positionStepsStart + 0.5 * myAxes.Axis[axis].direction * myAxes.Axis[axis].acceleration * deltaT * deltaT;
                        }

                        if(myAxes.Axis[axis].statusMove == 1)   // nur wenn Status immer noch 1 (T4 nicht erreicht) die StepsSoll aktualisieren, in allen anderen Zuständen fährt die Achse nicht!
                        {
                            posInt = (int)myAxes.Axis[axis].posDouble;    // war die böse Stelle !!
                            myAxes.Axis[axis].positionSteps = posInt;

                            //myAxis.Axis[axis].lastMoveStepsDouble[myAxis.Axis[axis].indexPath] = myAxis.Axis[axis].posDouble;     // nur für Diagnostik
                            myAxes.Axis[axis].lastMoveSteps[myAxes.Axis[axis].indexPath] = posInt;              // Bewegung aller Achsen immer protokollieren

                            if(axis < 4*foundSlaves.ECM712count)
                                myAxes.Axis[axis].lastMoveStepsIst[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].motor_direction * get_ECM712_StepsIst(foundSlaves.ECM712index[axis/4], axis%4 +1);    // rücklesen der PID des Indexers
                            else
                                myAxes.Axis[axis].lastMoveStepsIst[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].motor_direction * get_ECM713_StepsIst(foundSlaves.ECM713index[axis - 4*foundSlaves.ECM712count]);    // rücklesen der PID des Indexers

                            myAxes.Axis[axis].lastMovePosition[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].positionUnits;
                            myAxes.Axis[axis].lastMovePositionAverage[myAxes.Axis[axis].indexPath] = myAxes.Axis[axis].positionUnitsAverage;
                            if(flagsDebug.enc > 0)  // für Diagnostik einen beliebigen anderen Encoder-Kanal mit aufzeichnen
                                myEncoderChannels.lastMoveDebugEnc[myAxes.Axis[axis].indexPath] = myEncoderChannels.Encoder[flagsDebug.enc-1].positionUnits;
                            if(flagsDebug.encStatus > 0)  // für Diagnostik einen beliebigen anderen Encoder-Status mit aufzeichnen
                                myEncoderChannels.lastMoveDebugEncStatus[myAxes.Axis[axis].indexPath] = myEncoderChannels.Encoder[flagsDebug.encStatus-1].encState;

                            if(myAxes.Axis[axis].indexPath < MAXPATHPOINTS - 1) // Ende des Arrays erreicht, Protokollierung einfrieren
                                myAxes.Axis[axis].indexPath += 1;

                            //diag_StepsIst = get_ECM712_StepsIst(foundSlaves.ECM712index[axis/4], axis%4 +1);

                            //
                            //      nur hier werden die ECM-712 und -713 Indexer angesteuert !!
                            //      und nur hier werden die Steps nach Init um den Offset verschoben
                            //      und die Steps mit der Vorzugsdrehrichtung = Vorzeichen steps_per_unit multipliziert an die Indexer gegeben
                            //
                            {
                                long long stepsIndexer = myAxes.Axis[axis].motor_direction * (myAxes.Axis[axis].positionSteps - myAxes.Axis[axis].positionStepsOffset);

                                if(axis < 4*foundSlaves.ECM712count)
                                {
                                    set_ECM712_StepsSoll(foundSlaves.ECM712index[axis/4], axis%4 +1, stepsIndexer);
                                }
                                else if(axis < 4*foundSlaves.ECM712count + foundSlaves.ECM713count)
                                {
                                    set_ECM713_StepsSoll(foundSlaves.ECM713index[axis - 4*foundSlaves.ECM712count], stepsIndexer);
                                }
                                else    // jetzt kommen die simulierten Achsen
                                {

                                }

                                for(i = 0; i < myAxes.numAxis; i++)     // zugeordnete Gantry Achsen suchen
                                {
                                    if(myAxes.Axis[i].isGantry == axis+1)
                                    {
                                        // Gantries fahren als Slaves passiv mit, dürfen aber selber keineswegs den status 1=moving haben!
                                        if(i < 4*foundSlaves.ECM712count)
                                            set_ECM712_StepsSoll(foundSlaves.ECM712index[i/4], i%4 +1, stepsIndexer);
                                        else if(axis < 4*foundSlaves.ECM712count + foundSlaves.ECM713count)
                                            set_ECM713_StepsSoll(foundSlaves.ECM713index[i - 4*foundSlaves.ECM712count], stepsIndexer);
                                        else    // jetzt kommen die simulierten Achsen
                                        {

                                        }
                                    }
                                }
                            }
                            //  Überwachung Schleppfehler für laufende Achsen, wenn CL_DeltaFollow == 0 ist die Überwachnung deaktiviert:
                            if( (myAxes.Axis[axis].indexEncoder_A != 0) || (myAxes.Axis[axis].indexEncoder_B != 0) )     // Überwachung nur möglich, wenn Encoder vorhanden
                            {
                                // Der Schleppfehler in Units:
                                double deltaUnits = (double)(myAxes.Axis[axis].positionSteps - myAxes.Axis[axis].positionStepsStart) / myAxes.Axis[axis].steps_per_unit
                                        - (myAxes.Axis[axis].positionUnits- myAxes.Axis[axis].positionUnitsStart);
                                myAxes.Axis[axis].lastMoveDeltaFollow[myAxes.Axis[axis].indexPath] = deltaUnits;

                                if( (fabs(deltaUnits) > myAxes.Axis[axis].CL_DeltaFollow) && (myAxes.Axis[axis].CL_DeltaFollow > 0) )
                                {
                                    statusGlobalRT = GSTATUS_FOLLOWING_ERROR;     // Fehlermeldung
                                    statusArgRT = axis+1;                         // die betroffene Achse
                                    StopAllAxis();                      // sofortiger Stop aller Achsen bei Schleppfehler
                                }
                            }

                        }


                    }   // Ende StatusMove = 1

                    //
                    //      Die automatische Stabilisierung:
                    //
                    if(myAxes.Axis[axis].CL_StabWaitDowncount > 0)
                        myAxes.Axis[axis].CL_StabWaitDowncount -= 1;    // Wartezeit nach letztem Move runterzählen

                    // nur wenn alle Bedingungen erfüllt sind und die Achse ruht (statusMove == 0):
                    if(myAxes.Axis[axis].CL_StabWaitDowncount == 0 && myAxes.Axis[axis].isStabilized && myAxes.Axis[axis].statusMove == 0 && myAxes.isStabilizedGlobal)
                    {
                        deltaUnits = myAxes.Axis[axis].positionUnitsTarget - myAxes.Axis[axis].positionUnitsAverage;
                        if(fabs(deltaUnits) > myAxes.Axis[axis].CL_DeltaStab )  // Toleranzintervall für Stabilisierung verlassen, automatische Korrektur
                        {
                            myAxes.Axis[axis].CL_WaitMoveDowncount = myAxes.Axis[axis].CL_WaitMove;     // Countdown WaitMove scharf machen
                            myAxes.Axis[axis].CL_downcountIterations = 1;           // für Stabilisierung max zweischrittigen Move machen ??
                            myAxes.Axis[axis].indexPath = 0;                        // Speicher der Achse für Bewegungsprotokoll initialisieren, Zeiger auf Null
                            myAxes.Axis[axis].isMovingClosedLoop = true;            // wirklich ???
                            MovePlanRT(axis, myAxes.Axis[axis].positionSteps + (int)(deltaUnits * myAxes.Axis[axis].CL_Approach * myAxes.Axis[axis].steps_per_unit)); // setzt status wieder auf 2 hoch
                        }
                    }
                    // Init auf Forward oder Reverse Limit für Achsen mit Tonics oder ohne Encoder:
                    if(myAxes.Axis[axis].statusMove == 0 && myAxes.Axis[axis].isIniting == 3 && (myAxes.Axis[axis].limitReverse || myAxes.Axis[axis].limitForward) ) // aktuelle Achse fährt nicht mehr und steht im Limit
                    {
                        myAxes.Axis[axis].positionStepsTarget -= 500000 * myAxes.Axis[axis].initDirection;    //  500000 Steps sollten immer reichen um aus dem Reverse Limt wieder raus zu fahren
                        myAxes.Axis[axis].isIniting = 2;                    //  Flag für automatisches wieder Rausfahren aus dem Limit setzen
                        //MoveToSteps(axis);
                        //myAxis.Axis[axis].indexPath = 0;    // Speicher der Achse für Bewegungsprotokoll initialisieren, Zeiger auf Null
                        //targetSteps = myAxis.Axis[axis].positionStepsTarget;
                        MovePlanRT(axis, myAxes.Axis[axis].positionStepsTarget);
                    }

                    if(myAxes.Axis[axis].statusMove == 0 && (myAxes.otfAxisNr == axis+1) && myAxes.otfCapturing)   // wenn die Lead-Achse anhält, aus welchem Grund auch immer, das Capturing stoppen
                    {
                        myAxes.otfCapturing = false;
                        myAxes.otfReady = true;     // Scan fertig, Array steht zum Abspeichern bereit

                    }

                }   // Ende der for axis Schleife

                // die automatische Target-Korrektur, wird durch TC_delta_steps_temp != 0 aktiviert
                if(sourceControl.TC_delta_steps_temp != 0)
                {
                    int axis = sourceControl.TC_AxisNo -1;
                    myAxes.Axis[axis].indexPath = 0;
                    MovePlanRT(axis, myAxes.Axis[axis].positionSteps + sourceControl.TC_delta_steps_temp);
                    sourceControl.TC_delta_steps_temp = 0;          // ist zugleich Flag, also rücksetzen auf Null
                }

                // automatisches Inkrement der Target Translation nach einer Umdrehung
                // Die Translation findet bei 180 Grad statt
                {
                    long long pcTR = myAxes.Axis[sourceControl.TR_AxisNo-1].positionCounts;
                    if(pcTR > (ZWEI_E31 - 2*EIN_GRAD) && pcTR < (ZWEI_E31 - EIN_GRAD))
                    {
                        sourceControl.TT_Flag = true;   // Bedingung für den nächsten TT Move scharf machen
                    }
                    if(pcTR > ZWEI_E31 && sourceControl.TT_Flag)
                    {
                        int axis = sourceControl.TT_AxisNo -1;
                        myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps + (long long)(sourceControl.increment_trans * myAxes.Axis[axis].steps_per_unit);
                        myAxes.Axis[axis].indexPath = 0;                        // Speicher der Achse für Bewegungsprotokoll initialisieren, Zeiger auf Null
                        // flags.lastMoveType = flags.STEPS;
                        MovePlanRT(axis, myAxes.Axis[axis].positionStepsTarget);
                        sourceControl.TT_Flag = false;
                    }
                }

                myAxes.initFlag = true;                     // Alle Achsen mindestens einmal behandelt
                myAxes.isMovingGlobal = isMoving;           // für flackerfreie Anzeige im GUI erst hier aktualisieren

                if(myAxes.statusMotionGlobal != GSTATUS_FOLLOWING_ERROR && myAxes.statusMotionGlobal != GSTATUS_TOPOLOGY_ERROR)    // diese beiden Fehler müssen manuell quittiert werden
                {
                    if(isMoving & (statusGlobalRT==0) )                 // Mindestens eine Achse fährt und sonst kein Fehler aufgetreten
                        statusGlobalRT = GSTATUS_ISMOVING;              // mindestens eine Achse fährt, sonst alles Ok
                    myAxes.statusMotionGlobal = statusGlobalRT;         // Summen-Status nachdem alle Achsen abgearbeitet sind
                    myAxes.statusMotionArg = statusArgRT;
                }

                //
                //  Alle Digital IO Bausteine behandeln
                //

                // Alle Relais setzen:
                for(i=0; i < foundSlaves.EL2622count; i++)
                {
                    set_EL2622(foundSlaves.EL2622index[i], myRelais.state[2*i], myRelais.state[2*i+1]);
                }
                for(i=0; i < foundSlaves.EL2612count; i++)  // die EL2612 Wechsler werden in der Zählung einfach hinten angehängt
                {
                    set_EL2612(foundSlaves.EL2612index[i], myRelais.state[2*(i+foundSlaves.EL2622count)], myRelais.state[2*(i+foundSlaves.EL2622count)+1]);
                }

                //  die Eingänge einlesen:
                int inpIndex = 0;

                // EL1024 4CH Digital In, Typ 2 auslesen:
                for(i=0; i < foundSlaves.EL1024count; i++)
                {
                    fourBits_t fourBits;

                    fourBits = get_EL1024(foundSlaves.EL1024index[i]);
                    myInputs.state[inpIndex++] = fourBits.ch1;
                    myInputs.state[inpIndex++] = fourBits.ch2;
                    myInputs.state[inpIndex++] = fourBits.ch3;
                    myInputs.state[inpIndex++] = fourBits.ch4;
                }

                // EL1104 4CH Digital In, Typ 3 auslesen:
                for(i=0; i < foundSlaves.EL1104count; i++)
                {
                    fourBits_t fourBits;

                    fourBits = get_EL1104(foundSlaves.EL1104index[i]);
                    myInputs.state[inpIndex++] = fourBits.ch1;
                    myInputs.state[inpIndex++] = fourBits.ch2;
                    myInputs.state[inpIndex++] = fourBits.ch3;
                    myInputs.state[inpIndex++] = fourBits.ch4;
                }

                // EL1084 4CH Digital In, masseschaltend auslesen:
                for(i=0; i < foundSlaves.EL1084count; i++)
                {
                    fourBits_t fourBits;

                    fourBits = get_EL1084(foundSlaves.EL1084index[i]);
                    myInputs.state[inpIndex++] = fourBits.ch1;
                    myInputs.state[inpIndex++] = fourBits.ch2;
                    myInputs.state[inpIndex++] = fourBits.ch3;
                    myInputs.state[inpIndex++] = fourBits.ch4;
                }

                // EL1018 8CH Digital In, 24V, 10µs auslesen:
                for(i=0; i < foundSlaves.EL1018count; i++)
                {
                    eightBits_t Bits;

                    Bits = get_EL1018(foundSlaves.EL1018index[i]);
                    myInputs.state[inpIndex++] = Bits.ch1;
                    myInputs.state[inpIndex++] = Bits.ch2;
                    myInputs.state[inpIndex++] = Bits.ch3;
                    myInputs.state[inpIndex++] = Bits.ch4;
                    myInputs.state[inpIndex++] = Bits.ch5;
                    myInputs.state[inpIndex++] = Bits.ch6;
                    myInputs.state[inpIndex++] = Bits.ch7;
                    myInputs.state[inpIndex++] = Bits.ch8;
                }

            }
            else
            {
                if(ecatState.workingCounterState != WKC_INIT)   // nicht direkt von INIT zu Error springen
                {
                    // hier Datagramm Verlust, nachdem das System voll operational war, z.B. Global Stop am ECM-712 ausgelöst
                    ecatState.workingCounterState = WKC_ERR;
                    ecatState.numErrorCycles += 1;
                    myAxes.statusMotionGlobal = GSTATUS_ECAT_ERROR;
                    StopAllAxis();   // alle Achsen sofort stoppen, hier wirklich nötig??
                    myAxes.isMovingGlobal = false;
                }
            }
            clock_gettime(CLOCK_MONOTONIC, &nowtime);            // Ende der rt_Loop
            ecatState.deltaLoop = nowtime.tv_nsec - deadline.tv_nsec + (nowtime.tv_sec - deadline.tv_sec)*1000000000;
            ecatState.deltaLoop /= 1000;                        // die Laufzeit der gesamten RT-Schleife in µs
            if(ecatState.deltaLoop > ecatState.deltaLoopMax) ecatState.deltaLoopMax = ecatState.deltaLoop;
        }
    }   // Ende while(1)
}



//
//      Initialisierungen für die Real Time Fähigkeit
//

static int latency_target_fd = -1;
static int32_t latency_target_value = 0;

void set_cpu_dma_latency(void)
{
    // power mangement des Systems abschalten, setzt CPU Takt auf maximum oder konstant ???
    // Auf jeden Fall ist dies der Schlüssel unter dem OnLogic CL210G
    struct stat s;
    int ret;

    if (stat("/dev/cpu_dma_latency", &s) == 0) {
        latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);   // dies benötigt "setcap CAP_DAC_OVERRIDE"
        if (latency_target_fd == -1)
        {
            fprintf(stderr, "Error: Cannot open \"/dev/cpu_dma_latency\"\n");
            return;
        }
        ret = write(latency_target_fd, &latency_target_value, 4);
        if (ret == 0) {
            //fprintf(stderr, "Error: Cannot set cpu_dma_latency to %d!: %s\n", latency_target_value, strerror(errno));
            fprintf(stderr, "Error: Cannot set cpu_dma_latency");
            close(latency_target_fd);
            return;
        }
        fprintf(stderr, "Ok: \"/dev/cpu_dma_latency\" set to %dus\n", latency_target_value);
    }
}

void lock_AllMemory(void)
{
    /* Lock memory */
    //if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1)
    if(mlockall(MCL_CURRENT) == -1)     // Juhu, das ist es: MCL_FUTURE muss weg!
        fprintf(stderr, "Warning: mlockall failed: %m\n");
    else
        fprintf(stderr, "Ok: Memory successfully locked\n");
}

void create_RT(void)
{
    // create the RT thread, der läuft gleich los, aber wartet auf allSlavesOperational==true
    int iret1 = pthread_create( &thread1, NULL, rt_Loop, NULL);
    if(iret1==0)
        fprintf(stderr, "Ok: Real Time Thread successfully created.\n");
    else
        fprintf(stderr, "Error: Real Time Thread could not be created.\n");
}

void elevate_RT(void)
{
    // give the RT thread higher priority
    struct sched_param param;
    memset(&param, 0, sizeof(param));
    param.sched_priority = 90;
    int policy = SCHED_FIFO;
    int iret1 = pthread_setschedparam(thread1, policy, &param);
    if(iret1==0)
        fprintf(stderr, "Ok: Priority of Real Time Thread successfully elevated.\n");
    else
        fprintf(stderr, "Error: Priority of Real Time Thread could not be elevated.\n");
}


// sofortiger Notstop aller Achsen, wird bei Encoder Error auch aus der RT Lopp aufgerufen
void StopAllAxis()
{
    myAxes.isStabilizedGlobal = false;      // automatisches Wiederanlaufen stabilisierter Achsen verhindern
    sourceControl.processRunning = false;   // dies schließt den Shutter, evenmtuell laufenden Prozess abbrechen

    for(int axis = 0; axis < myAxes.numAxis; ++axis)        // alle Achsen anhalten (also auch das Target!)
    {
        if(myAxes.Axis[axis].statusMove != 0)  // Abfrage wichtig, sonst wird der Protokoll Nachlauf zu einer Endlosschleife
        {
            myAxes.Axis[axis].statusMove = 0;
            myAxes.Axis[axis].isMovingClosedLoop = false;                               // Moving Steps ist der Default
            myAxes.Axis[axis].velocity = myAxes.Axis[axis].velocity_backup;             // Geschwindigkeit zurücksetzen
            myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps;    // damit deltajog nach NotStop auf der letzten Position aufbaut
            myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnits;
            // die Beiden folgenden müsse im if stehen, sonst führte ein Encoderfehler zu einem Dauerfeuer:
            myAxes.ignoreLimits = false;                                                // nur einen Move mit ignore zulassen
            flags.refreshMoveFinished = true;                                           // GUI aktualisieren, aber nur wenn mindestens eine laufende Achse gestoppt wird
        }
    }
}

// sofortiger Stop einer einzelnen Achse per GUI oder TCP-IP
void StopAxis(int axis)
{
    if(myAxes.Axis[axis].statusMove != 0)  // Abfrage wichtig, sonst wird der Protokoll Nachlauf zu einer Endlosschleife
    {
        myAxes.Axis[axis].statusMove = 0;
        myAxes.Axis[axis].isMovingClosedLoop = false;                                   // Moving Steps ist der Default
        myAxes.Axis[axis].velocity = myAxes.Axis[axis].velocity_backup;                 // Geschwindigkeit zurücksetzen, noch nötig ???
        myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps;        // damit deltajog nach NotStop auf der letzten Position aufbaut
        myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnits;
    }
    myAxes.ignoreLimits = false;                                                        // nur einen Move mit ignore zulassen
    flags.refreshMoveFinished = true;                                                   // GUI aktualisieren
}

// regulärer Stop einer einzelnen Achse am Ende des Moves oder der Move Kette (bei closed loop), wird *nur* in der RT Routine aufgerufen
void StopAxisRT(int axis)
{
    myAxes.Axis[axis].statusMove = 0;                                                   // Bewegungszyklus beenden
    myAxes.Axis[axis].isMovingClosedLoop = false;                                       // Moving Steps ist der Default
    myAxes.Axis[axis].velocity = myAxes.Axis[axis].velocity_backup;                     // Geschwindigkeit zurücksetzen
    myAxes.Axis[axis].CL_StabWaitDowncount = myAxes.Axis[axis].CL_WaitStab;      // mindest Wartezeit in Milli-Sekunden bis Stab neu starten
    myAxes.Axis[axis].positionStepsTarget = myAxes.Axis[axis].positionSteps;            // damit deltajog nach NotStop auf der letzten Position aufbaut
    // Bei regulärem Stop am Ende eines Moves die Zielposition nicht ändern, bleibt hier auskommentiert:
    //myAxes.Axis[axis].positionUnitsTarget = myAxes.Axis[axis].positionUnits;            // ohne Average, trifft die Endposition tatsächlich besser
    myAxes.ignoreLimits = false;                                                        // nur einen Move mit ignore zulassen
    flags.refreshMoveFinished = true;                                                   // GUI aktualisieren

    if(myAxes.Axis[axis].isIniting == 1)   // wenn der move der Abschluß eines inits mit initSteps > 0 war
    {
        myAxes.Axis[axis].isIniting = 0;    // Init Prozess beenden
        if(myAxes.Axis[axis].indexEncoder_A > 0)
            myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_A-1].referencing = 0;    // Zero on Mark deaktivieren
        if(myAxes.Axis[axis].indexEncoder_B > 0)
            myEncoderChannels.Encoder[myAxes.Axis[axis].indexEncoder_B-1].referencing = 0;    // Zero on Mark deaktivieren
    }
}


//
//              Hier sind alle Funktionen versammelt, die Daten aus dem PDO lesen oder schreiben
//              Also alle Zugriffe auf die EtherCAT Slaves
//


void set_EL2202_CH1(uint16 slave_no, bool output, bool tristate)
{
    uint8 *data_ptr, bit_index;

    if(foundSlaves.EL2202count > 0) // Trigger Modul für Laser vorhanden
    {

        data_ptr = ec_slave[slave_no].outputs;
        bit_index = ec_slave[slave_no].Ostartbit;
        if(output)
            *data_ptr |= 1 << bit_index;
        else
            *data_ptr &= ~(1 << bit_index);
        bit_index++;
        if(tristate)
            *data_ptr |= 1 << bit_index;
        else
            *data_ptr &= ~(1 << bit_index);
    }
}

void set_EL2202_CH2(uint16 slave_no, bool output, bool tristate)
{
    uint8 *data_ptr, bit_index;

    data_ptr = ec_slave[slave_no].outputs;
    bit_index = ec_slave[slave_no].Ostartbit;
    bit_index++;
    bit_index++; // Pointer auf zweiten Kanal setzen
    if(output)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);
    bit_index++;
    if(tristate)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);

}

void set_EL2612(uint16 slave_no, bool relais_1, bool relais_2)
{
    uint8 *data_ptr, bit_index;

    data_ptr = ec_slave[slave_no].outputs;
    bit_index = ec_slave[slave_no].Ostartbit;
    if(relais_1)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);
    bit_index++;
    if(relais_2)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);
}

void set_EL2622(uint16 slave_no, bool relais_1, bool relais_2)
{
    uint8 *data_ptr, bit_index;

    data_ptr = ec_slave[slave_no].outputs;
    bit_index = ec_slave[slave_no].Ostartbit;
    /* Move pointer to correct module index*/
    //data_ptr += module_index * 2;
    /* Read value byte by byte since all targets can't handle misaligned
 addresses
    */
    if(relais_1)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);

    bit_index++;
    if(relais_2)
        *data_ptr |= 1 << bit_index;
    else
        *data_ptr &= ~(1 << bit_index);
}

fourBits_t get_EL1024(uint16 slave_no)
{
    fourBits_t myBits;
    uint8_t *data_ptr, bit_index, byte;

    data_ptr = ec_slave[slave_no].inputs;
    bit_index = ec_slave[slave_no].Istartbit;
    byte = *data_ptr;

    myBits.ch1 = byte & 1<< bit_index++;
    myBits.ch2 = byte & 1<< bit_index++;
    myBits.ch3 = byte & 1<< bit_index++;
    myBits.ch4 = byte & 1<< bit_index;

    return myBits;
}

fourBits_t get_EL1104(uint16 slave_no)
{
    fourBits_t myBits;
    uint8_t *data_ptr, bit_index, byte;

    data_ptr = ec_slave[slave_no].inputs;
    bit_index = ec_slave[slave_no].Istartbit;
    byte = *data_ptr;

    myBits.ch1 = byte & 1<< bit_index++;
    myBits.ch2 = byte & 1<< bit_index++;
    myBits.ch3 = byte & 1<< bit_index++;
    myBits.ch4 = byte & 1<< bit_index;

    return myBits;
}

fourBits_t get_EL1084(uint16 slave_no)
{
    fourBits_t myBits;
    uint8_t *data_ptr, bit_index, byte;

    data_ptr = ec_slave[slave_no].inputs;
    bit_index = ec_slave[slave_no].Istartbit;
    byte = *data_ptr;

    myBits.ch1 = byte & 1<< bit_index++;
    myBits.ch2 = byte & 1<< bit_index++;
    myBits.ch3 = byte & 1<< bit_index++;
    myBits.ch4 = byte & 1<< bit_index;

    return myBits;
}

eightBits_t get_EL1018(uint16 slave_no)
{
    eightBits_t myBits;
    uint8_t *data_ptr, bit_index, byte;

    data_ptr = ec_slave[slave_no].inputs;
    bit_index = ec_slave[slave_no].Istartbit;
    byte = *data_ptr;

    myBits.ch1 = byte & 1<< bit_index++;
    myBits.ch2 = byte & 1<< bit_index++;
    myBits.ch3 = byte & 1<< bit_index++;
    myBits.ch4 = byte & 1<< bit_index++;
    myBits.ch5 = byte & 1<< bit_index++;
    myBits.ch6 = byte & 1<< bit_index++;
    myBits.ch7 = byte & 1<< bit_index++;
    myBits.ch8 = byte & 1<< bit_index;

    return myBits;
}


// den Trigger auf intern oder extern setzen
void set_ECM701_TrigMode(uint16 slave_no, bool state_Trigger)
{
    uint8 *data_ptr;

    data_ptr = ec_slave[slave_no].outputs;

    if(state_Trigger)
    {
        //*data_ptr |= 1 << bit_index;
        *data_ptr = 0xff;
    }
    else
    {
        //*data_ptr &= ~(1 << bit_index);
        *data_ptr = 0x00;
    }
}

//
//      Die IO-Funktionen für den A760-01-B Controller für Projekt 483 BliX
//

// den Shutter Auf machen = true
void set_A760_01_B_Shutter(uint16 slave_no, bool state_Shutter)
{
    uint8 *data_ptr;

    if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
    {
        data_ptr = ec_slave[slave_no].outputs;

        if(state_Shutter)
        {
            //*data_ptr |= 1 << bit_index;
            *data_ptr = 0xff;
        }
        else
        {
            //*data_ptr &= ~(1 << bit_index);
            *data_ptr = 0x00;
        }
    }
}

// die Verzögerung vom Laser Trigger zum Sample Zeitpunkt in µs
void set_A760_01_B_Delay(uint16 slave_no, uint16 delay)
{
    uint8 *data_ptr;
    UWORD word;

    if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
    {
        word.Word = delay;      // 16 Bit, also maximal 65,5 Millisekunden
        data_ptr = ec_slave[slave_no].outputs;
        data_ptr += 1;
        *data_ptr = word.Byte[0];
        data_ptr += 1;
        *data_ptr = word.Byte[1];
    }
}

// die Revisions-Nummer der Firmware aus der IOmap lesen
uint8_t get_A760_01_B_Revision(uint16_t slave_no)
{
    uint8_t *data_ptr;
    uint8_t revision = 0;

    if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
    {
        data_ptr = ec_slave[slave_no].inputs;
        revision = *data_ptr;
    }
    return revision;
}

// den Zustand lesen, ist zur Zeit nur ein Bit, ob neue Counts anliegen
uint8_t get_A760_01_B_Status(uint16_t slave_no)
{
    uint8 *data_ptr;
    uint8 status = 1;

    if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
    {
        data_ptr = ec_slave[slave_no].inputs;
        data_ptr += 1;
        status = *data_ptr;
    }
    return status;
}

// den ADC-Wert der vier Quadranten in Counts (also 16 Bit unsigned) vom Ladungsverstärker lesen
// slave_no 1..N, channel 1..4
void get_A760_01_B_DetectorCounts(uint16_t slave_no, int *Q1, int *Q2,  int *Q3,  int *Q4)
{
    uint8_t *data_ptr;
    UWORD dCounts;

    if(foundSlaves.A760_01_B_count > 0)     // sonst Absturz bei nicht vorhandenem Modul
    {
        data_ptr = ec_slave[slave_no].inputs;
        data_ptr += 2;                      // REVISION und status überspringen
        dCounts.Byte[0] = *data_ptr++;
        dCounts.Byte[1] = *data_ptr++;
        *Q1 = dCounts.Word;                 // Vertauschung richtig? -Nein, rückgängig gemacht!
        dCounts.Byte[0] = *data_ptr++;
        dCounts.Byte[1] = *data_ptr++;
        *Q2 = dCounts.Word;
        dCounts.Byte[0] = *data_ptr++;
        dCounts.Byte[1] = *data_ptr++;
        *Q3 = dCounts.Word;
        dCounts.Byte[0] = *data_ptr++;
        dCounts.Byte[1] = *data_ptr++;
        *Q4 = dCounts.Word;
    }
    else
    {
        *Q1 = 10000;
        *Q2 = 20000;
        *Q3 = 30000;
        *Q4 = 40000;
    }
}


// Indexpuls aktivieren und Triggerflanke setzen
void set_ECM702_ControlByte(uint16 slave_no, int controlByte)
{
    uint8 *data_ptr;

    uint8_t byte = controlByte;
    data_ptr = ec_slave[slave_no].outputs;
    *data_ptr = byte;
}

// einen Encoderwert in Counts (also die 32 Bit unsigned) vom Renishaw Tonic aus der IOmap lesen
// slave_no 1..N, channel 1..2
uint32_t get_ECM702_EncoderCounts(uint16 slave_no, int channel)
{
    uint8_t *data_ptr;
    ULONG encoderCounts;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 4*(channel-1);      // einen der Eingänge 1 bis 2 wählen
    encoderCounts.Byte[0] = *data_ptr++;
    encoderCounts.Byte[1] = *data_ptr++;
    encoderCounts.Byte[2] = *data_ptr++;
    encoderCounts.Byte[3] = *data_ptr++;
    return encoderCounts.Long;       // Rückgabewert
}

// einen Capture (das ist ein Encoderwert in Counts im Moment des Hardwaretriggers) aus der IOmap lesen
// slave_no 1..N, channel 1..2
// dieser Wert ist nur gültig, wenn im letzten ECat Frame (1 ms) tatsächlich ein Triggerpuls aufgetreten ist.
// Der Triggerpuls wird durch Bit 3 im Status Byte signalisiert
uint32_t get_ECM702_otfCaptureCounts(uint16 slave_no, int channel)
{
    uint8_t *data_ptr;
    ULONG captureCounts;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 4*(channel-1) + 8;      // einen der Eingänge 1 bis 2 wählen
    captureCounts.Byte[0] = *data_ptr++;
    captureCounts.Byte[1] = *data_ptr++;
    captureCounts.Byte[2] = *data_ptr++;
    captureCounts.Byte[3] = *data_ptr++;
    return captureCounts.Long;       // Rückgabewert
}

// den Status des Encoder-Kanals lesen
// slave_no 1..N, channel 1..2
int get_ECM702_Status(uint16 slave_no, int channel)
{
    uint8 *data_ptr;
    int status;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 16;         // das vorletzte Byte im TxPdo, ist der Status für beide Kanäle
    status = *data_ptr;
    if(channel == 2)
        status >>= 4;       // für Kanal 2 die oberen vier Bits nehmen, da das Trigger-Bit vom ECM-702 auf Position 3 und 7 gleichlautend gesetzt wird, geht dies auch für T
    status &= 15;           // die Bits Z A T und Reserve

    return status;
}

// die Revisions-Nummer der Firmware aus der IOmap lesen
uint8_t get_ECM702_Revision(uint16 slave_no)
{
    uint8 *data_ptr, revision;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 17;         // das letzte Byte im TxPdo, ist die Versionsnummer (10 = 1.0)
    revision = *data_ptr;
    return revision;
}


//  Diese Tabelle bestimmt das Generator-Polynom für den Biss-C CRC ( P(x) = x 6 + x 1 + 1 = 0x43 )
//  enthält alle 64 möglichen Werte von 6 Bit
uint8_t tableCRC6[64] = {
    0x00, 0x03, 0x06, 0x05, 0x0C, 0x0F, 0x0A, 0x09,
    0x18, 0x1B, 0x1E, 0x1D, 0x14, 0x17, 0x12, 0x11,
    0x30, 0x33, 0x36, 0x35, 0x3C, 0x3F, 0x3A, 0x39,
    0x28, 0x2B, 0x2E, 0x2D, 0x24, 0x27, 0x22, 0x21,
    0x23, 0x20, 0x25, 0x26, 0x2F, 0x2C, 0x29, 0x2A,
    0x3B, 0x38, 0x3D, 0x3E, 0x37, 0x34, 0x31, 0x32,
    0x13, 0x10, 0x15, 0x16, 0x1F, 0x1C, 0x19, 0x1A,
    0x0B, 0x08, 0x0D, 0x0E, 0x07, 0x04, 0x01, 0x02
};

//  Hier wird mit Hilfe obiger Tabelle die CRC über 34 Bit berechnet
//  32 Bit MSB bis LSB Position + err + warn
uint8_t bissCRC(uint64_t checkData)
{
    uint8_t crc;
    uint64_t tmp;

    // macht 6 sechser-Gruppen = 36 Bit, die obersten zwei sind immer 0:

    tmp = (checkData >> 30) & 0x0F;             // die obersten vier Bit
    crc = ((checkData >> 24) & 0x3F);           // jeweils sechs Bit
    tmp = crc ^ tableCRC6[tmp];
    crc = ((checkData >> 18) & 0x3F);
    tmp = crc ^ tableCRC6[tmp];
    crc = ((checkData >> 12) & 0x3F);
    tmp = crc ^ tableCRC6[tmp];
    crc = ((checkData >> 6) & 0x3F);
    tmp = crc ^ tableCRC6[tmp];
    crc = (checkData & 0x3F);
    tmp = crc ^ tableCRC6[tmp];
    crc = tableCRC6[tmp];

    return crc;         // 6-Bit CRC als Ergebnis, die obersten beiden Bit sind immer Null
}

// einen Encoderwert in Counts (also die 32 Bit unsigned) vom Renishaw Resolute aus der IOmap lesen
// slave_no 1..N, channel 1..4
int get_ECM701_EncoderCounts(uint16 slave_no, int channel, uint32_t *counts)
{
    uint8_t *data_ptr, crc_calc, crc_enc, Byte_5;
    uint64_t data;
    ULONG encoderCounts;
    bool error, warn;
    int encState;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 2 + 5*(channel-1);          // einen der Eingänge 1 bis 4 wählen
    encoderCounts.Byte[0] = *data_ptr++;    // die ersten vier Byte sind die counts
    encoderCounts.Byte[1] = *data_ptr++;
    encoderCounts.Byte[2] = *data_ptr++;
    encoderCounts.Byte[3] = *data_ptr++;
    Byte_5 = *data_ptr;                     // das fünfte Byte enthält die 6-bit CRC plus 2 Bit Status des Encoders

    *counts = encoderCounts.Long;       // Rückgabewert
    error = !(Byte_5 & 0x80);           // H = false = Ok, L = true = Position ungültig
    warn  = !(Byte_5 & 0x40);           // H = false = Ok, L = true = schlechtes Signal
    crc_enc = Byte_5 & 0x3F;            // die 6-bit CRC vom Encoder-Kopf

    //  CRC Prüfung, Position, Error und Warning Bits müssen mit eingeschlossen werden = 34 bits, die 6 bit CRC nicht!
    //  Position length: 32 bits, Status length: 2 bits (active low)
    data = encoderCounts.Long;
    data <<= 2;                         // zwei nach links
    data |= Byte_5 >> 6;                // ERR und WARN Bit hinzu
    crc_calc = bissCRC(data);           // die 6-bit CRC aus den Daten berechnet, über 34 Bit
    crc_calc = ~crc_calc & 0x3F;        // invertieren für direkten Vergleich, nur die unteren 6 Bit

    encState = ENCSTATE_OK;
    if(warn)
        encState = ENCSTATE_WARN;       // Lesekopf meldet schwaches Signal
    if(error)
        encState = ENCSTATE_READ_ERROR; // Lesekopf meldet Fehler, kann Masstab nicht lesen, counts sind ungültig
    if(crc_calc != crc_enc)
        encState = ENCSTATE_CRC_ERROR;  // Datentelegramm auf Kabel elektrisch gestört, ein oder mehrere Bits sind gekippt.
                                        // Dieser Fehler ist besonders heimtückisch und hat daher die zweithöchste Priorität
    if(encoderCounts.Long == 0 && Byte_5 == 0)
        encState = ENCSTATE_NO_CONNECT; // Kein Resolute an diesem Kanal angeschlossen, der ECM-701 findet kein ACK und signalisiert dies, indem er alles auf Null setzt.

    return encState;
}

// den Trigger Counter aus der IOmap lesen
uint16_t get_ECM701_TrigCount(uint16 slave_no)
{
    uint8 *data_ptr;
    UWORD tCount;

    data_ptr = ec_slave[slave_no].inputs;
    tCount.Byte[0] = *data_ptr++;
    tCount.Byte[1] = *data_ptr;
    return tCount.Word;
}

// die Revisions-Nummer der Firmware aus der IOmap lesen
uint16_t get_ECM701_Revision(uint16 slave_no)
{
    uint8 *data_ptr;
    UWORD revision;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 22;     // letzte beiden Einträge im TxPdo, ist die Versionsnummer als Word
    revision.Byte[0] = *data_ptr++;
    revision.Byte[1] = *data_ptr;
    return revision.Word;
}


// Ist-Schritte zur Kontrolle lesen
int32_t get_ECM712_StepsIst(uint16 slave_no, int channel)
{
    uint8 *data_ptr;
    ULONG steps;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 4*(channel-1);  // einen der Eingänge 1 bis 4 wählen
    steps.Byte[0] = *data_ptr++;
    steps.Byte[1] = *data_ptr++;
    steps.Byte[2] = *data_ptr++;
    steps.Byte[3] = *data_ptr++;
    return steps.Long;
}

// slave_no 1..N, channel 1..4
void get_ECM712_Limits(uint16 slave_no, int channel, bool *lowerLimit, bool *upperLimit)
{
    uint8 *data_ptr;
    uint8 byte;

    data_ptr = ec_slave[slave_no].inputs + 16;
    byte = *data_ptr;

    if(foundSlaves.simulateECM)
    {
        *lowerLimit = false;
        *upperLimit = false;
    }
    else
    {
        *lowerLimit = byte & (1 << (2*channel-1));
        *upperLimit = byte & (1 << (2*channel-2));
    }
}

// absolute Soll-Schritt-Position 32 Bit mit Vorzeichen, beim Einschalten implizit auf Null
void set_ECM712_StepsSoll(uint16 slave_no, int channel, int32_t absSteps)
{
    uint8 *data_ptr;
    ULONG steps;

    steps.Long = absSteps;
    data_ptr = ec_slave[slave_no].outputs + 4*(channel-1);  // einen der Eingänge 1 bis 4 wählen
    if(!foundSlaves.simulateECM)
    {
        *data_ptr++ = steps.Byte[0];
        *data_ptr++ = steps.Byte[1];
        *data_ptr++ = steps.Byte[2];
        *data_ptr++ = steps.Byte[3];
    }
}

// Ist-Schritte der Endstufe zur Kontrolle lesen
int32_t get_ECM713_StepsIst(uint16 slave_no)
{
    uint8 *data_ptr;
    ULONG steps;

    data_ptr = ec_slave[slave_no].inputs;
    steps.Byte[0] = *data_ptr++;
    steps.Byte[1] = *data_ptr++;
    steps.Byte[2] = *data_ptr++;
    steps.Byte[3] = *data_ptr++;
    return steps.Long;
}

// slave_no 1..N
void get_ECM713_Limits(uint16 slave_no, bool *lowerLimit, bool *upperLimit, bool *aux1, bool *aux2)
{
    uint8 *data_ptr;
    uint8 byte;

    data_ptr = ec_slave[slave_no].inputs + 4;
    byte = *data_ptr;

    if(foundSlaves.simulateECM)
    {
        *lowerLimit = false;
        *upperLimit = false;
    }
    else
    {
        *lowerLimit = byte & 2;
        *upperLimit = byte & 1;
        *aux1       = byte & 4;
        *aux2       = byte & 8;
    }
}

// den Status des Motor-Treibers DRV8711 lesen
uint8 get_ECM713_MotorStatus(uint16 slave_no)
{
    uint8 *data_ptr;

    data_ptr = ec_slave[slave_no].inputs + 5;
    return *data_ptr;
}

// absolute Soll-Schritt-Position 32 Bit mit Vorzeichen, beim Einschalten implizit auf Null
void set_ECM713_StepsSoll(uint16 slave_no, int32_t absSteps)
{
    uint8 *data_ptr;
    ULONG steps;

    steps.Long = absSteps;
    data_ptr = ec_slave[slave_no].outputs;
    if(!foundSlaves.simulateECM)
    {
        *data_ptr++ = steps.Byte[0];
        *data_ptr++ = steps.Byte[1];
        *data_ptr++ = steps.Byte[2];
        *data_ptr++ = steps.Byte[3];
    }
}

// die Revisions-Nummer der Firmware aus der IOmap lesen
uint16_t get_ECM713_Revision(uint16 slave_no)
{
    uint8 *data_ptr;
    UWORD revision;

    data_ptr = ec_slave[slave_no].inputs;
    data_ptr += 11;     // letzter Eintrag im TxPdo, ist die Versionsnummer als Byte
    revision.Byte[0] = *data_ptr;
    revision.Byte[1] = 0;
    return revision.Word;
}


// einen Encoderwert in Counts (also die 32 Bit unsigned) vom Renishaw Resolute aus der IOmap lesen
// slave_no 1..N
int get_ECM713_EncoderCounts(uint16 slave_no, uint32_t *counts)
{
    uint8_t *data_ptr, crc_calc, crc_enc, Byte_5;
    uint64_t data;
    ULONG encoderCounts;
    bool error, warn;
    int encState;

    data_ptr = ec_slave[slave_no].inputs + 6;
    encoderCounts.Byte[0] = *data_ptr++;
    encoderCounts.Byte[1] = *data_ptr++;
    encoderCounts.Byte[2] = *data_ptr++;
    encoderCounts.Byte[3] = *data_ptr++;
    Byte_5 = *data_ptr;                 // das fünfte Byte enthält die 6-bit CRC plus 2 Bit Status des Encoders

    *counts = encoderCounts.Long;       // Rückgabewert
    error = !(Byte_5 & 0x80);           // H = false = Ok, L = true = Position ungültig
    warn  = !(Byte_5 & 0x40);           // H = false = Ok, L = true = schlechtes Signal
    crc_enc = Byte_5 & 0x3F;            // die 6-bit CRC vom Encoder-Kopf

    //  CRC Prüfung, Position, Error und Warning Bits müssen mit eingeschlossen werden = 34 bits, die 6 bit CRC nicht!
    //  Position length: 32 bits, Status length: 2 bits (active low)
    data = encoderCounts.Long;
    data <<= 2;                         // zwei nach links
    data |= Byte_5 >> 6;                // ERR und WARN Bit hinzu
    crc_calc = bissCRC(data);           // die 6-bit CRC aus den Daten berechnet, über 34 Bit
    crc_calc = ~crc_calc & 0x3F;        // invertieren für direkten Vergleich, nur die unteren 6 Bit

    encState = ENCSTATE_OK;
    if(warn)
        encState = ENCSTATE_WARN;       // Lesekopf meldet schwaches Signal
    if(error)
        encState = ENCSTATE_READ_ERROR; // Lesekopf meldet Fehler, kann Masstab nicht lesen, counts sind ungültig
    if(crc_calc != crc_enc)
        encState = ENCSTATE_CRC_ERROR;  // Datentelegramm auf Kabel elektrisch gestört, ein oder mehrere Bits sind gekippt.
                                        // Dieser Fehler ist besonders heimtückisch und hat daher die zweithöchste Priorität
    if(encoderCounts.Long == 0 && Byte_5 == 0)
        encState = ENCSTATE_NO_CONNECT; // Kein Resolute angeschlossen, der ECM-713 findet kein ACK und signalisiert dies, indem er alles auf Null setzt.

    return encState;
}


// Der Strom wird laufend aktualisiert
void set_ECM713_current(uint16 slave_no, uint8 current)
{
    uint8 *data_ptr;

    data_ptr = ec_slave[slave_no].outputs + 6;  // TORQUE Register des DRV, nur die acht TORQUE Bits werden gesetzt, den Rest bestimmt die Firmware
    if(!foundSlaves.simulateECM)
    {
        *data_ptr = current;    // Strom von 0..255
    }
}

// Die folgenden Einstellungen werden nur einmalig initialisiert, Update nur mit "Restart EtherCAT"
void set_ECM713_microSteps(uint16 slave_no, uint8 microSteps)
{
    uint8 *data_ptr;

    data_ptr = ec_slave[slave_no].outputs + 5;              // CTRL Register des DRV, nur die vier MODE Bits für das µStepping werden gesetzt, den Rest bestimmt die Firmware
    if(!foundSlaves.simulateECM)
    {
        *data_ptr = microSteps;
    }
}

void set_ECM713_TimingAndCtrl(uint16 slave_no)
{
    uint8 *data_ptr;

    data_ptr = ec_slave[slave_no].outputs + 7;              // TOFF Register des DRV
    if(!foundSlaves.simulateECM)
    {
        // Die drei Zeiten werden direkt wie sie sind als 8-Bit-Werte von der Firmware in die entsprechenden Register eingetragen:
        *data_ptr++ = motorDriverConfig.motor_tOff;         // TOFF Zeit, in 500 ns Schritten
        *data_ptr++ = motorDriverConfig.motor_tBlank;       // TBLANK Zeit, in 20 ns Schritten
        *data_ptr++ = motorDriverConfig.motor_tDecay;       // TDECAY  Zeit, in 500 ns Schritten
    }
}

