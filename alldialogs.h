//
//      hier ist der ganze Kram für die Qt-Dialoge
//

#include <QDialog>
#include <QInputDialog>
#include <QListWidget>
#include <QKeyEvent>
#include <QPalette>     // für farbigen Text
#include <QTimer>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QDateTime>
//#include <QQAbstractFileEngineHandler>
#include <QNetworkInterface>
#include <QTcpServer>
#include <QTcpSocket>           // ganz wichtig, wird nur gefunden wenn in ecTool.pro "QT += core gui network" steht !
#include <QUdpSocket>
#include <QShortcut>
#include <QCryptographicHash>   // für die Passwort-Verarbeitung


namespace Ui {
    class ecMainDialog;
    class ecAxisDialog;
    class ecEncoderDialog;
    class ecBeckhoffDialog;
    class ecConfigurationDialog;
    class ecMoveSequenceDialog;
    class ecHexapodDialog;
    class ecChooseList;
    class ecOtfTonicDialog;
    class ecSourceControlDialog;
}

class QTcpServer;
class QTcpSocket;
class QUdpSocket;

// die nichtmodalen Dialoge müssen vor ecMainDialog deklariert sein
class ecAxisDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecAxisDialog(QWidget *parent = 0);
    void InitContent();     // initialisiert meine Tabelle aus den globalen Variablen
    void UpdateContent();   // für Status des Motortreibers

signals:
    void axisParameterChanged();    // wird nach Doppelklick gesendet

private:
    Ui::ecAxisDialog *ui;
    void reject();
    //QListWidget listWidget_Unit;

private slots:
    //void on_pushButton_Close_clicked();   // kein Close Button um Platz zu sparen
    void on_tableWidget_Axis_cellDoubleClicked(int row, int column);
};

class ecEncoderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecEncoderDialog(QWidget *parent = 0);
    void InitContent();
    void UpdateContent();

signals:
    //void axisValuesChanged();

private:
    Ui::ecEncoderDialog *ui;
    void reject();

private slots:
    //void on_pushButton_Close_clicked();   // kein Close Button um Platz zu sparen
    void on_tableWidget_Encoder_cellDoubleClicked(int row, int column);
};


class ecBeckhoffDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecBeckhoffDialog(QWidget *parent = 0);
    void UpdateContent();


signals:
    //void axisValuesChanged();

private:
    Ui::ecBeckhoffDialog *ui;
    void reject();

private slots:
    void on_checkBox_R1_clicked();
    void on_checkBox_R2_clicked();
    void on_checkBox_R3_clicked();
    void on_checkBox_R4_clicked();
    void on_checkBox_R5_clicked();
    void on_checkBox_R6_clicked();

};


class ecConfigurationDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecConfigurationDialog(QWidget *parent = 0);
    void InitContent();

signals:
    //void axisValuesChanged();

private:
    Ui::ecConfigurationDialog *ui;
    void reject();

private slots:

};


class ecMoveSequenceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecMoveSequenceDialog(QWidget *parent = 0);
    void InitContent(bool useHexa);
    void UpdateContent();
    int currentRow;

signals:
    //void axisValuesChanged();

private:
    Ui::ecMoveSequenceDialog *ui;
    void reject();

private slots:

    void on_pushButton_moveSequenceStart_clicked();
    void on_lineEdit_StartPoint_editingFinished();
    void on_lineEdit_EndPoint_editingFinished();
    void on_lineEdit_SubDivisions_editingFinished();
    void on_lineEdit_Iterations_editingFinished();
    void on_lineEdit_RestTime_editingFinished();
    //void on_lineEdit_MoveSequenceAxis_editingFinished();
    void on_lineEdit_WaitTime_editingFinished();
    void on_listWidgetAoD_itemClicked(QListWidgetItem *item);
};



class ecHexapodDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecHexapodDialog(QWidget *parent = 0);
    void SetContent();
    void UpdateContent();

signals:
    void hexaMoveToUnits(int axis, double units, double relVel);

private slots:
    void on_pushButton_moveHexaToPose_clicked();
    void on_lineEdit_dof_X_editingFinished();
    void on_lineEdit_dof_Y_editingFinished();
    void on_lineEdit_dof_Z_editingFinished();
    void on_lineEdit_dof_Yaw_editingFinished();
    void on_lineEdit_dof_Pitch_editingFinished();
    void on_lineEdit_dof_Roll_editingFinished();
    void on_lineEdit_dof_Z0_editingFinished();
    void on_lineEdit_dof_ZPP_editingFinished();
    void on_pushButton_calculateHexaPose_clicked();

private:
    Ui::ecHexapodDialog *ui;
    void reject();
};


class ecOtfTonicDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecOtfTonicDialog(QWidget *parent = 0);
    void InitContent();


signals:
    //void axisValuesChanged();

private:
    Ui::ecOtfTonicDialog *ui;
    void reject();

private slots:

    void on_listWidget_OTF_Axis_clicked(const QModelIndex &index);
};


class ecSourceControlDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecSourceControlDialog(QWidget *parent = 0);

    void InitContent();
    void UpdateContent();

signals:
    //void axisValuesChanged();

private:
    Ui::ecSourceControlDialog *ui;
    void reject();

private slots:
    //void on_pushButton_Shot_clicked();
    void on_pushButton_Start_clicked();
    void on_pushButton_Stop_clicked();
};



class ecMainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ecMainDialog(QWidget *parent = 0);
    // Pointer auf die untergeordneten nichtmodalen Dialoge:
    ecAxisDialog *axisDialog;
    ecEncoderDialog *encoderDialog;
    ecBeckhoffDialog *beckhoffDialog;
    ecConfigurationDialog *configurationDialog;
    ecHexapodDialog *hexapodDialog;
    ecMoveSequenceDialog *movesequenceDialog;
    ecOtfTonicDialog *otfTonicDialog;
    ecSourceControlDialog *sourceControlDialog;

    QString check;

    void MoveDeltaSteps(int axis, qint64 deltaSteps, double relVel);
    void start_Process();       // Den Plasmaprozess und Protokollierung starten
    void stop_Process();        // Den Plasmaprozess anhalten


public slots:
    //void update();
    void InitAxisListMainDialog();                         // dieser slot empfängt das Signal für geänderte Achsenparameter

protected:  // nötig??
    void keyPressEvent(QKeyEvent*);

private:
    Ui::ecMainDialog *ui;
    void InitConfigMainDialog();
    void MainDialogUpdateContent();
    void setUnitText(int unitType);
    void setFeedbackText(int axis);
    void reject();                  // diese Methode wird überschrieben, um versehentliches Schließen von ecControl zu verhindern
    QTcpServer *myServer = NULL;    // das Server-Objekt für die Kommunikation mit dem Bestec-Client
    QTcpSocket *clientConnection = NULL;    // die TCP/IP Netzwerkverbindung dazu
    QUdpSocket *moxaSocket = NULL;
    void MoveToSteps(int axis, qint64 absSteps, double relVel);
    void MoveToUnits(int axis, double units, double relVel);        // wird im Konstruktor connected, damit von hexapod aus zugänglich
    void MoveDeltaUnits(int axis, double deltaUnits, double relVel);
    void MoveToCounts(int axis, qint64 counts, double relVel);
    void MoveDeltaCounts(int axis, qint64 deltaCounts, double relVel);
    void Initialize_TCPIP_Interface();  // einmal bei Programmstart mit Default.cfg, danach bei Load Configuration

    void MoveAxisOrDof(int axis, double position);

    void setUserLevel(int userLevel);
    int userLevel;


private slots:
    void myqTimerTask();
    //void on_pushButton_Close_clicked();
    void on_pushButton_MoveTo_Steps_clicked();
    void on_pushButton_MoveTo_Position_clicked();
    void on_listWidget_Axis_itemClicked(QListWidgetItem *item);
    void on_pushButton_JogPlus_Steps_clicked();
    void on_pushButton_JogMinus_Position_clicked();
    void on_pushButton_JogPlus_Position_clicked();
    void on_pushButton_JogMinus_Steps_clicked();
    void on_pushButton_SaveConfiguration_clicked();
    void on_pushButton_LoadConfiguration_clicked();
    void on_pushButton_ecRestart_clicked();
    void on_checkBox_IgnoreLimits_clicked();
    void on_lineEdit_Tail_editingFinished();
    void on_lineEdit_DeltaSteps_editingFinished();
    void on_lineEdit_DeltaPosition_editingFinished();
    void on_lineEdit_Steps_Aim_editingFinished();
    void on_lineEdit_Position_Aim_editingFinished();
    void on_pushButton_Stop_clicked();
    //void on_key_s_clicked();

    void connectClient();
    void disconnectClient();
    void readClientData();
    void on_checkBox_Stabilize_clicked();
    void on_pushButton_OpenAxisConfiguration_clicked();
    void on_checkBox_GlobalStab_clicked();
    void on_pushButton_Init_clicked();
    void on_pushButton_viewConfiguration_clicked();
    void on_pushButton_FollowingError_Reset_clicked();
    void on_pushButton_OpenEncoderConfiguration_clicked();
    //void on_pushButton_Init_All_clicked();
    void on_pushButton_SaveLastMove_clicked();
    void on_pushButton_StartLogging_clicked();
    void on_pushButton_userLogin_clicked();
    void on_pushButton_userChangePWD_clicked();
    void on_pushButton_GetRecord_clicked();
    void on_pushButton_Extend_clicked();
};


class ecChooseList : public QDialog
{
    Q_OBJECT

public:
    explicit ecChooseList(int select, QWidget *parent = 0);
    int exec();     // Funktion überschreiben, damit ich den gewählten Item übergeben kann

private:
    Ui::ecChooseList *ui;
    int myrow;

private slots:
    void on_listWidget_Choose_itemClicked();
};


